<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResources(['auth' => 'API\ApiAuthController']);

/* MENUS */
Route::get('ocorrenciasPendentes', 'API\Admin\Menus\NavbarController@ocorrenciasPendentes');
Route::get('marcacoesPendentes', 'API\Admin\Menus\NavbarController@marcacoesPendentes');
Route::get('chatPendentes', 'API\Admin\Menus\NavbarController@chatPendentes');

Route::apiResources(['user' => 'API\UserController']);
Route::get('profile', 'API\UserController@profile');
Route::put('profile/{id}', 'API\UserController@updateProfile');
Route::put('userupdate/{id}', 'API\UserController@updateUser');
Route::get('findUser', 'API\UserController@search');

Route::get('empresas/findEmpresa', 'API\Admin\Empresas\EmpresasController@search');
Route::get('empresas/empresasbyempresasid', 'API\Admin\Empresas\EmpresasController@empresasByempresasId');
Route::apiResources(['empresas' => 'API\Admin\Empresas\EmpresasController']);


Route::get('filiais/findFilial', 'API\Admin\Filial\FilialController@search');
Route::apiResources(['filiais' => 'API\Admin\Filial\FilialController']);

Route::get('setores/findSetore', 'API\Admin\Setores\SetoresController@search');
Route::apiResources(['setores' => 'API\Admin\Setores\SetoresController']);

Route::get('logs/findLog', 'API\Admin\Logs\LogsController@search');
Route::apiResources(['logs' => 'API\Admin\Logs\LogsController']);

Route::get('cargos/findCargo', 'API\Admin\Cargos\CargosController@search');
Route::apiResources(['cargos' => 'API\Admin\Cargos\CargosController']);

Route::get('calendarios/findCalendario', 'API\Admin\Calendarios\CalendariosController@search');
Route::apiResources(['calendarios' => 'API\Admin\Calendarios\CalendariosController']);

Route::get('colaboradores/findColaboradore', 'API\Comum\Colaboradores\ColaboradoresController@search');
Route::get('colaboradores/ultimouserid', 'API\Comum\Colaboradores\ColaboradoresController@ultimoUserId');
Route::apiResources(['colaboradores' => 'API\Comum\Colaboradores\ColaboradoresController']);

Route::apiResources(['mapauxiliar' => 'API\Comum\Colaboradores\MapsAuxiliarController']);

Route::delete('removecercabyid/{id}', 'API\Comum\Colaboradores\UsersCercasVirtuaisController@removecercaById');
Route::apiResources(['userscercasvirtuais' => 'API\Comum\Colaboradores\UsersCercasVirtuaisController']);

Route::get('escalas/findEscala', 'API\Comum\Escalas\EscalasController@search');
Route::apiResources(['escalas' => 'API\Comum\Escalas\EscalasController']);

Route::get('intervalos/findIntervalo', 'API\Comum\Intervalos\IntervalosController@search');
Route::apiResources(['intervalos' => 'API\Comum\Intervalos\IntervalosController']);

Route::get('jornadas/findJornada', 'API\Comum\Jornadas\JornadasController@search');
Route::apiResources(['jornadas' => 'API\Comum\Jornadas\JornadasController']);

Route::get('perfis/findPerfil', 'API\Admin\Perfis\PerfisController@search');
Route::apiResources(['perfis' => 'API\Admin\Perfis\PerfisController']);

Route::get('permissoes/findPermissoes', 'API\Admin\Perfis\PermissoesController@search');
Route::apiResources(['permissoes' => 'API\Admin\Perfis\PermissoesController']);

Route::get('permissoes/findPermissoesUsuarios', 'API\Admin\Perfis\PermissoesUsuariosController@search');
Route::post('permissoesusuarios/verificapermissoes', 'API\Admin\Perfis\PermissoesUsuariosController@verificaPermissoes');
Route::apiResources(['permissoesusuarios' => 'API\Admin\Perfis\PermissoesUsuariosController']);

Route::get('cercas-virtuais/findCercasvirtuais', 'API\Admin\CercasVirtuais\CercasVirtuaisController@search');
Route::apiResources(['cercas-virtuais' => 'API\Admin\CercasVirtuais\CercasVirtuaisController']);

Route::get('registropontos/findRegistroPonto', 'API\Comum\RegistroPonto\RegistroPontoController@search');
Route::apiResources(['registropontos' => 'API\Comum\RegistroPonto\RegistroPontoController']);

Route::get('registropontosauditoria/findRegistroPontoAuditoria', 'API\Admin\RegistroPontoAuditoria\RegistroPontoAuditoriaController@search');
Route::apiResources(['registropontosauditoria' => 'API\Admin\RegistroPontoAuditoria\RegistroPontoAuditoriaController']);

Route::get('registromarcacaopontos/findRegistroPonto', 'API\Comum\RegistroPonto\RegistroMarcacaoPontosController@search');
Route::get('registromarcacaopontos/totalHorasMesAtual', 'API\Comum\RegistroPonto\RegistroMarcacaoPontosController@totalHorasMesAtual');
Route::get('registromarcacaopontos/totalHorasPrevistasMesAtual', 'API\Comum\RegistroPonto\RegistroMarcacaoPontosController@totalHorasPrevistasMesAtual');
Route::get('registromarcacaopontos/verificaSePodeBaterPontoRegistroWeb', 'API\Comum\RegistroPonto\RegistroMarcacaoPontosController@verificaSePodeBaterPontoRegistroWeb');
Route::get('registromarcacaopontos/verificaSePodeBaterPontoFerias', 'API\Comum\RegistroPonto\RegistroMarcacaoPontosController@verificaSePodeBaterPontoFerias');
Route::post('registromarcacaopontosmanuais', 'API\Comum\RegistroPonto\RegistroMarcacaoPontosController@pontoManual');
Route::apiResources(['registromarcacaopontos' => 'API\Comum\RegistroPonto\RegistroMarcacaoPontosController']);

Route::get('ocorrenciasjustificadas/findOcorrenciasJustificada', 'API\Admin\OcorrenciasJustificadas\OcorrenciasJustificadasController@search');
Route::apiResources(['ocorrenciasjustificadas' => 'API\Admin\OcorrenciasJustificadas\OcorrenciasJustificadasController']);

Route::get('ocorrenciaspendentes/findOcorrenciaspendentes', 'API\Admin\OcorrenciasPendentes\OcorrenciasPendentesController@search');
Route::get('showbyuserid/{id}', 'API\Admin\OcorrenciasPendentes\OcorrenciasPendentesController@showByUserId');
Route::post('aprovaroureprovar', 'API\Admin\OcorrenciasPendentes\OcorrenciasPendentesController@aprovarOUreprovar');
Route::apiResources(['ocorrenciaspendentes' => 'API\Admin\OcorrenciasPendentes\OcorrenciasPendentesController']);

Route::get('marcacoespendentes/findMarcacoespendente', 'API\Admin\MarcacoesPendentes\MarcacoesPendentesController@search');
Route::post('marcacoespendentes/modificapermissao', 'API\Admin\MarcacoesPendentes\MarcacoesPendentesController@modificaPermissao');
Route::apiResources(['marcacoespendentes' => 'API\Admin\MarcacoesPendentes\MarcacoesPendentesController']);

Route::get('historicomarcacoespendentes/find', 'API\Admin\Historico\HistoricoMarcacoesPendentesController@search');
Route::apiResources(['historicomarcacoespendentes' => 'API\Admin\Historico\HistoricoMarcacoesPendentesController']);

Route::get('historicoocorrenciaspendentes/find', 'API\Admin\Historico\HistoricoOcorrenciasPendentesController@search');
Route::apiResources(['historicoocorrenciaspendentes' => 'API\Admin\Historico\HistoricoOcorrenciasPendentesController']);

Route::get('chats/findChat', 'API\Admin\Chat\ChatController@search');
Route::apiResources(['chats' => 'API\Admin\Chat\ChatController']);

// Route::get('chatmessages/findChatmessage', 'API\Admin\Chatmessages\ChatmessagesController@search');
// Route::apiResources(['chatmessages' => 'API\Admin\Chatmessages\ChatmessagesController']);

Route::get('colaboradorchats/findChat', 'API\Comum\Colaboradorchat\ColaboradorChatController@search');
Route::apiResources(['colaboradorchats' => 'API\Comum\Colaboradorchat\ColaboradorChatController']);

Route::get('colaboradorhistoricopontos/findColaboradorhistoricopontos', 'API\Comum\Colaboradores\ColaboradorHistoricoDePontosController@search');
Route::apiResources(['colaboradorhistoricopontos' => 'API\Comum\Colaboradores\ColaboradorHistoricoDePontosController']);

// controle de ponto
Route::get('pontos/findRegistroPonto', 'API\Admin\Controledeponto\PontoController@search');
Route::post('pontos/exibeMarcacoesDePonto', 'API\Admin\Controledeponto\PontoController@exibeMarcacoesDePonto');
Route::post('pontos/aprovarPonto', 'API\Admin\Controledeponto\PontoController@aprovarPonto');
Route::post('pontos/reprovarPonto', 'API\Admin\Controledeponto\PontoController@reprovarPonto');
Route::post('pontos/corrigirPonto', 'API\Admin\Controledeponto\PontoController@corrigirPonto');
Route::post('pontos/lancarPontoManual', 'API\Admin\Controledeponto\PontoController@lancarPontoManual');
Route::post('pontos/lancarOcorrencia', 'API\Admin\Controledeponto\PontoController@lancarOcorrencia');
Route::apiResources(['pontos' => 'API\Admin\Controledeponto\PontoController']);

Route::post('bancodehoras/exibePorData', 'API\Admin\Controledeponto\BancodehorasController@exibePorData');
Route::get('bancodehoras/findRegistroPonto', 'API\Admin\Controledeponto\BancodehorasController@search');
Route::apiResources(['bancodehoras' => 'API\Admin\Controledeponto\BancodehorasController']);

Route::get('importafdt/findImport', 'API\Admin\Controledeponto\ImportafdController@search');
Route::apiResources(['importafdt' => 'API\Admin\Controledeponto\ImportafdController']);

Route::get('dispositivosautorizados/findDispositivosAutorizado', 'API\Admin\DispositivosAutorizado\DispositivosAutorizadoController@search');
Route::put('dispositivosautorizados/alterastatus/{id}', 'API\Admin\DispositivosAutorizado\DispositivosAutorizadoController@alteraStatus');
Route::apiResources(['dispositivosautorizados' => 'API\Admin\DispositivosAutorizado\DispositivosAutorizadoController']);

// aplicativo
Route::get('authenticacao/findColaborador', 'API\Aplicativo\Login\LoginController@search');
Route::apiResources(['authenticacao' => 'API\Aplicativo\Login\LoginController']);

$router->post('login/', 'API\Aplicativo\Authenticate\UsersController@authenticate');

$router->get('registro_marcacao_ponto/{id}', 'API\Aplicativo\Colaborador\RegistroMarcacaoPontosController@show');
$router->post('subirImagemColaborador', 'API\Aplicativo\Colaborador\RegistroMarcacaoPontosController@subirImagemColaborador');
$router->post('registro_marcacao_ponto', 'API\Aplicativo\Colaborador\RegistroMarcacaoPontosController@store');

$router->post('verificasepodebaterponto', 'API\Aplicativo\Colaborador\RegistroMarcacaoPontosController@primeiraVerificaSePodeBaterPonto');
$router->post('updatebaterponto', 'API\Aplicativo\Colaborador\RegistroMarcacaoPontosController@updateBaterPonto');

$router->get('verificapermissoes/{id}', 'API\Aplicativo\Colaborador\UsersColaboradoresController@verificaPermissoes');

$router->post('verificadistanciabaterponto', 'API\Aplicativo\Colaborador\UsersCercasvirtuaisController@verificaDistanciaBaterPonto');