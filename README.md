query crud:
php artisan crud:generate Posts --fields='title#string; content#text; category#select#options={"technology": "Technology", "tips": "Tips", "health": "Health"}' --view-path=comum --controller-namespace=Admin --route-group=admin --form-helper=html --soft-deletes=yes --model-namespace="Models"
w


php artisan crud:generate DispositivosAutorizado --fields='empresas_id#integer; users_id#integer; mensagem#text;ip#string' --view-path=admin --route-group=admin --controller-namespace=API/Admin/DispositivosAutorizado  --form-helper=html --soft-deletes=yes --model-namespace="Models/Admin"




php artisan migrate --path=/database/migrations/

titulo#password;quem_pertence#password;chave_ordem#password;ordem_exibicao#integer;avo#password;permissao_direta#boolean;descricao#password;pai#integer;status#boolean;


 inicio_intervalo#time;
 fim_intervalo#time;
 abonar#boolean;
 pre_assinalar#boolean;
 status#boolean;

 dias_jornada



 // calendario
nome_calendario#password;ano#date;descricao_feriado_ou_recesso#text;data_feriado#date;hora_ou_periodo#boolean;dia_inteiro_apuracao#boolean;apuracao#password;horario_apuracao#time;


empresas_id#integer;calendarios_id#integer;descricao_feriado_ou_recesso#text;data_feriado#date;data_periodo_inicio#timestamp;data_periodo_fim#timestamp;hora_ou_periodo#boolean;dia_inteiro_apuracao#boolean;apuracao#varchar;horario_apuracao#time;status#boolean;


top:

<IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews -Indexes
    </IfModule>

    RewriteEngine On

    # Handle Authorization Header
    RewriteCond %{HTTP:Authorization} .
    RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

    # Redirect Trailing Slashes If Not A Folder...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_URI} (.+)/$
    RewriteRule ^ %1 [L,R=301]

    # Handle Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]
</IfModule>


RewriteEngine On
    RewriteCond %{HTTPS} off
    # First rewrite to HTTPS:
    # Don't put www. here. If it is already there it will be included, if not
    # the subsequent rule will catch it.
    RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
    # Now, rewrite any request to the wrong domain to use www.
    # [NC] is a case-insensitive match
    RewriteCond %{HTTP_HOST} !^www\. [NC]
    RewriteRule .* https://www.%{HTTP_HOST}%{REQUEST_URI} [L,R=301]