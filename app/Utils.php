<?php

namespace App;

use App\Models\Comum\Intervalo;
use App\Models\Comum\RegistroMarcacaoPontos;
use App\Models\Comum\UsersEscala;
use DateTime;
use DateTimeZone;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Utils extends Model
{
    static function formatDateTimeToView($datetime, $format = "d/m/Y H:i:s")
    {
        date_default_timezone_set('America/Sao_Paulo');

        if ($datetime != null) {
            $tmp = strtotime($datetime);
            return date($format, $tmp);
        }
    }

    /**
     * Converte a o datetime formatada no padrao BR para formato do banco de dados d/m/Y H:i para Y-m-d H:i
     * @param $datetime
     * @return $data_saida
     */
    static function formatDateTimeToBD($datetime, $format = "Y-m-d H:i")
    {

        $data_entrada = $datetime;
        $formato_entrada = "d/m/Y H:i";
        $formato_saida = "Y-m-d H:i";

        try {
            $data = \DateTime::createFromFormat($formato_entrada, $data_entrada);

            $erros = \DateTime::getLastErrors();

            if ($erros['warning_count'] or $erros['error_count']) {
                throw new \Exception("Data inválida: '{$data_entrada}'; a data precisa estar no formato '{$formato_entrada}'."
                    . " Avisos ({$erros['warning_count']}): " . implode(". ", $erros['warnings'])
                    . " Erros ({$erros['error_count']}): " . implode(". ", $erros['errors'])
                );
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit;
        }

        $data_saida = $data->format($formato_saida);

        return $data_saida; // yyyy-mm-dd H:i
    }

    static function formatDateToBD($data)
    {
        $data_saida = implode("-",array_reverse(explode("/",$data)));
        return $data_saida; // yyyy-mm-dd 
    }

    static function explodeDateTime($datetime)
    {
        $d = self::formatDateTimeToView($datetime);

        $d = explode(' ', $d);

        $h = explode(":", $d[1]);

        $d[1] = $h[0] . ":" . $h[1];

        return $d;
    }

    static function convertFromDateRange($dateRange)
    {
        $datas = explode(' - ', $dateRange);

        $saida = [];

        foreach ($datas as $d) {
            $saida[] = implode('-', array_reverse(explode("/", $d)));
        }

        return $saida;
    }

    /**
     * Converte array de data do banco em um data range estilo DD/MM/YYYY - DD/MM/YYYY
     * @param $datas
     * @return string
     */
    static function convertToDateRange($datas)
    {
        $saida = [];
        foreach ($datas as $d) {
            $saida[] = implode('/', array_reverse(explode("-", $d)));
        }

        return implode(' - ', $saida);
    }

    /**
     * Converte a data no formato brasileiro para formato de banco de dados
     * @param $data - DD/MM/YYYY
     * @return string - YYYY-MM-DD
     * */
    static function convertFromDateBr($data)
    {
        return implode('-', array_reverse(explode('/', $data)));
    }

    /**
     * Converte a data no formato do bd para formato BR
     * @param $data - YYYY-MM-DD
     * @return string - DD/MM/YYYY
     * */
    static function convertToDateBr($data)
    {
        return implode('/', array_reverse(explode('-', $data)));
    }

    /**
     * @param $datetime
     * @param string $format
     * @return false|string
     * recebe uma data Y-m-d H:i converte para d/m/Y
     */
    static function formatDateTimeToDate($datetime, $format = "d/m/Y")
    {
        date_default_timezone_set('America/Sao_Paulo');

        if ($datetime != null) {
            $tmp = strtotime($datetime);
            return date($format, $tmp);
        }
    }

    /**
     * Recebe uma data no formato BR e concatena com um tempo (HH:II)
     * @param $data
     * @param $tempo
     * @return string
     */
    static function concatenarTempo($data, $tempo)
    {
        return self::convertFromDateBr($data) . " " . $tempo;
    }

    /**
     * Converte d-m-y h:i:s e retorna array de data [0] e hora [1]
     */
    static function converterDateTime($dateTime)
    {
        $d = explode(" ", $dateTime);

        return [self::convertToDateBr($d[0]), $d[1]];
    }
 
 

    /**
     * Count de Notificacoes nao lidas
     * @param $idnotificado - id do usuario que possui notificacoes nao lidas
     * @return bool
     */
    static function contagemMensagensSuporteNaoLidas($idnotificado, $idsuporte)
    {
        $results = \DB::select(
            \DB::raw("SELECT * FROM notificacoes_firebase 
        WHERE 
          SUBSTRING_INDEX(SUBSTRING_INDEX(notificacoes_firebase.data,'/','2'), '/', -1) = '$idsuporte'
          and idnotificado = '$idnotificado'
          and titulo like 'Novo Comentário'
          and read_at is null
          "));
        return count($results);
    }

    /**
     * Calculo de diferenca entre data de agora de que a notificacao foi enviada
     * @param $notificacao - dados da notificacao
     * @return bool
     */
    static function calcularDiferencaDateTime($notificacao)
    {
        date_default_timezone_set('America/Sao_Paulo');

        $dataAtual = date('Y-m-d H:i:s');
        $h2 = new \DateTime($dataAtual);

        $h1 = new \DateTime($notificacao->data_criacao);

        // O metodo diff retorna um novo DateInterval-object...
        $diff = $h1->diff($h2);

        $dia = $diff->format('%a');
        if ($dia >= 1) {
            return $diff->format('%r%a dia(s) e %H:%I:%S');
        } else {
            return $diff->format('%H:%I:%S');
        }
    }

    public function usuario_setor($idusuario)
    {
        return $setorUsuario = SetorUsuario::where('idusuario', $idusuario)->get();
    }

    static function separarDiaDaData($data)
    {
        date_default_timezone_set('America/Sao_Paulo');

        $dia = new \DateTime($data);
        return $dia->format('d');
    }

    static function separarDiaDaDataRange($data, $dia)
    {
        $d = explode('-', $data);

        $data1 = Utils::convertFromDateBr($d[0]);

        $data2 = Utils::convertFromDateBr($d[1]);


        if($data1 <= $data2){
            $data_inicio = new \DateTime($data1);

            $data_fim = new \DateTime($data2);


//            if($dia == $data_inicio->format('d') ||  $dia == $data_fim->format('d')){
//                return "X";
//            }


            if($dia == $data_inicio->format('d') || $dia <= $data_fim->format('d')){
//                if ($dia <= $data_fim->format('d')){
//                    dd($dia);
                    return"X";
//                }
            }

        }
    }

    /**
     * @param $rows Lista de objetos
     * @param $value Atributo que representa o valor
     * @param $desc  Atributo que servirá como label
     * @param string $selected Valor pré selecionado
     * @return string Options do HTML
     */
    static function options($rows, $value, $desc, $selected = '')
    {
//        dd($desc);
        $options = '';
        foreach ($rows as $r) {
            if ($r->$value == $selected) {
                $s = 'selected=""';
            } else {
                $s = '';
            }

            $options .= "<option value='" . $r->$value . "' " . $s . ">" . $r->$desc . "</option>";
        }

        return $options;
    }

    public static function inputValue($v1, $v2)
    {
        if ($v1) {
            return $v1;
        } else {
            return $v2;
        }
    }


    /*
     * Função recebe da um valor com "." e "," do tipo string e retorna um valor float formatado
     * entrada: 1.236,54
     * saida: 1236.54
     */
    static function formatarDecimalStringToFloat($str)
    {
        if (strstr($str, ",")) {
            $str = str_replace(".", "", $str); // replace dots (thousand seps) with blancs
            $str = str_replace(",", ".", $str); // replace ',' with '.'
        }

        if (preg_match("#([0-9\.]+)#", $str, $match)) { // search for number that may contain '.'
            return floatval($match[0]);
        } else {
            return floatval($str); // take some last chances with floatval
        }
    }

    /*
    * Função recebe da um valor com "." para separar casa decimal do tipo float e retorna um formatado com "," para decimal e "." para un. de milhar
    * entrada: 1236.54
    * saida: 1.236,54
    */
    static function formatarDecimalFloatToString($number, $dec_point = ",", $thousands_sep = ".")
    {
        $tmp = explode('.', $number);
        $out = number_format($tmp[0], 0, $dec_point, $thousands_sep);
        if (isset($tmp[1])) $out .= $dec_point . $tmp[1];

        return $out;
    }

    /**
     *   utilizar os 7 primeiros digitos do RF
     */
    static function formataRF($rf)
    {
        if (!empty($rf)) {
            $rf = str_replace(".", "", $rf);
            $rf = str_replace("/", "", $rf);
        }

        $rf = substr($rf, 0, 3) . "." . substr($rf, 3, 3) . "." . substr($rf, 6, 1);

        return $rf;
    }
    static function formataRg($rg)
    {
        if (!empty($rg)) {
            $rg = str_replace(".", "", $rg);
            $rg = str_replace("-", "", $rg);
        }

//        $rg = substr($rg, 0, 3) . "." . substr($rg, 3, 3) . "." . substr($rg, 6, 1);

        return $rg;
    }
    /*
     * Utilizada para busca com auto complete, onde pode buscar por RF ou Nome
     * Essa função recebe o termo digitado na busca
     * Se o termo digitado for número, ele é formatado com a mascara conforme a digitação, após o 3º e 6º dígito
     * Necessária para Busca de Usuários ao Abrir Chamado
     */
    static function formataTermoBuscaRfOuNome($termo)
    {
        $termo_rf = ctype_digit($termo);
        if ($termo_rf == true) {
            if (strlen($termo) > 3) {
                if (!empty($termo)) {
                    $termo = str_replace(".", "", $termo);
                    $termo = str_replace("/", "", $termo);
                }
                /* Formata Digito por digito o rf, adicionando a mascara conforme a digitacao*/
                if (strlen($termo) > 3 && strlen($termo) <= 6) {
                    return $termo = substr($termo, 0, 3) . "." . substr($termo, 3, 3);
                } else if (strlen($termo) > 6) {
                    return $termo = substr($termo, 0, 3) . "." . substr($termo, 3, 3) . "." . substr($termo, 6, 1);
                }
            } else {
                return $termo;
            }
        }else{
            return $termo;
        }


    }

    /**
     * @param $data
     * @return string
     * recebe a data Y-m-d e retorna a idade de acordo com o ano
     */
    static function calcularIdade($data)
    {
        date_default_timezone_set('America/Sao_Paulo');

        // Separa em dia, mês e ano
        list($dia, $mes, $ano) = explode('/', $data);

        // Descobre que dia é hoje e retorna a unix timestamp
        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        // Descobre a unix timestamp da data de nascimento do fulano
        $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);

        // Depois apenas fazemos o cálculo já citado :)
        $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
        return $idade;
    }

    /**
     * Recebe uma data inicial, um intervalo, e uma data final. Com base nessas informações devolve um array iniciando em $inicio, com intervalo de $intervalo e acaba em $fim
     * @param $inicio String 'Y-m-d H:i:s'
     * @param $intervalo String +1 day'
     * @param $fim String 'Y-m-d H:i:s'
     * @return array
     */
    static function timeRows($inicio, $intervalo, $fim)
    {
        $mixed = [];

        $inicio = strtotime($inicio);

        while ($inicio <= strtotime($fim)) {
            $mixed[] = date('Y-m-d', $inicio);

            $inicio = strtotime(date('Y-m-d', $inicio) . " " . $intervalo);
        }

        return $mixed;
    }

    
    static function converterDateTimeToTime($datetime)
    {
        if (!empty($datetime)) {
            $time = strtotime($datetime);
            return date("H:i:s", $time);
        } else {
            return "";
        }
    }

    /**
     * A funcao recebe um datetime do bd e retorna para a view a data com tempo
     * Usar a funcao em input type="datetime-local"
     * Entrada 2019-12-24 08:00:00
     * Saída 2019-12-24T08:00
     * @param $datetime
     * @return string
     */
    static function formatDateTimeToDateTimeLocalView($datetime){

        return \Carbon\Carbon::parse($datetime)->format('Y-m-d\TH:i');
    }
    static function formatDateTimeToDateTimeLocalSecondsView($datetime){

        return \Carbon\Carbon::parse($datetime)->format('Y-m-d\TH:i:s');
    }
    static function formatDateTimeToDateTimeLocalSecondsBD($datetime){

        return \Carbon\Carbon::parse($datetime)->format('Y-m-d H:i:s');
    }

    static function converterUtf8 ($texto) {
        // troca simbolos por palavras acentuadas
        $texto = str_replace("\'c3",'Ã',$texto);
        $texto = str_replace("\'e3",'ã',$texto);
        $texto = str_replace("\'c1",'Á',$texto);
        $texto = str_replace("\'e1",'á',$texto);
        $texto = str_replace("\'c0",'À',$texto);
        $texto = str_replace("'e0",'à',$texto);
        $texto = str_replace("\'cd",'Í',$texto);
        $texto = str_replace("'ed",'í',$texto);
        $texto = str_replace("\'c7",'Ç',$texto);
        $texto = str_replace("\'e7",'ç',$texto);
        $texto = str_replace("\'c2",'Â',$texto);
        $texto = str_replace("'e2",'â',$texto);
        $texto = str_replace("\'da",'Ú',$texto);
        $texto = str_replace("\'fa",'ú',$texto);
        $texto = str_replace("\'f3",'ó',$texto);
        $texto = str_replace("'f4",'ó',$texto);
        $texto = str_replace("'d3",'ó',$texto);
        $texto = str_replace("\'f5",'õ',$texto);
        $texto = str_replace("\'d4",'Ó',$texto);
        $texto = str_replace("\'aa",'ª',$texto);
        $texto = str_replace("'ba",'°',$texto);
        $texto = str_replace("'e9",'é',$texto);

        //remove simbolos desgraçados
        $texto = str_replace("'f4n ",'i',$texto);
        $texto = str_replace("d\\",'',$texto);
        $texto = str_replace("qj",'',$texto);
        $texto = str_replace("{",'',$texto);
        $texto = str_replace(";",'',$texto);
        $texto = str_replace("}",'',$texto);
        $texto = str_replace("\b",'',$texto);
        $texto = str_replace("d\b0",'',$texto);
        $texto = str_replace("d0",'',$texto);
        $texto = str_replace("",'',$texto);
        $texto = str_replace("\par",'',$texto);
        $texto = str_replace("\\f1",'',$texto);
        $texto = str_replace("\\f2",'',$texto);
        $texto = str_replace("\\f3",'',$texto);
        $texto = str_replace("\\f4",'',$texto);
        $texto = str_replace("\\f5",'',$texto);
        $texto = str_replace("\\f6",'',$texto);
        $texto = str_replace("\\f7",'',$texto);
        $texto = str_replace("\\f8",'',$texto);
        $texto = str_replace("\\f9",'',$texto);
        $texto = str_replace("\\rtf1",'',$texto);
        $texto = str_replace("\\fnil",'',$texto);
        $texto = str_replace("\\fswiss",'',$texto);
        $texto = str_replace("\\viewkind4",'',$texto);
        $texto = str_replace("\\uc1d",'',$texto);
        $texto = str_replace("\\sb100",'',$texto);
        $texto = str_replace("sb100",'',$texto);
        $texto = str_replace("uc1",'',$texto);
        $texto = str_replace("b0",'',$texto);
        $texto = str_replace("\\sa100",'',$texto);
        $texto = str_replace("\\ansi",'',$texto);
        $texto = str_replace("\\deff0",'',$texto);
        $texto = str_replace("\\fonttbl",'',$texto);
        $texto = str_replace("\\lang1046",'',$texto);
        $texto = str_replace("\\fs16",'',$texto);
        $texto = str_replace("\\fs20",'',$texto);
        $texto = str_replace("\\fs24",'',$texto);
        $texto = str_replace("\\f0",'',$texto);
        $texto = str_replace("\\froman",'',$texto);
        $texto = str_replace("\\fcharset0",'',$texto);
        $texto = str_replace("\\deftab720",'',$texto);
        $texto = str_replace("\\fcharset2",'',$texto);
        $texto = str_replace("\\fcharset1",'',$texto);
        $texto = str_replace("\\colortbl",'',$texto);
        $texto = str_replace("\\re",'',$texto);
        $texto = str_replace("\\green0lue0",'',$texto);
        $texto = str_replace("\\deflang1046d",'',$texto);
        $texto = str_replace("\\plain",'',$texto);
        $texto = str_replace(" plain ",'',$texto);
        
        // remove palavras
        $texto = str_replace("Symbol",'',$texto);
        $texto = str_replace("MS Sans ",'',$texto);
        $texto = str_replace("MT -Bold ",'',$texto);
        $texto = str_replace("Serif",'',$texto);
        $texto = str_replace("Arial",'',$texto);
        $texto = str_replace("Verdana",'',$texto);
        $texto = str_replace("Times New Roman",'',$texto);
        $texto = str_replace("uc1sb100",'',$texto);
        $texto = str_replace("deflang1046plain",'',$texto);
        
        $texto = str_replace("cpg1252",'',$texto);
        $texto = str_replace("\\",' ',$texto);
        return ($texto);
   }   

    static function mascaraRG($val, $mask)  // modo de usar: \App\Utils::mascaraRG($rg, '##.###.###-##')
    {
        $maskared = '';   
        $k = 0;
        for($i = 0; $i<=strlen($mask)-1; $i++){
        
        if($mask[$i] == '#'){
            if(isset($val[$k]))
                $maskared .= $val[$k++];
        }
    
        else
        {
            if(isset($mask[$i]))
                $maskared .= $mask[$i];
            }

        }
        return $maskared;

    }


    static function exibeNomeDoMes($numeroMes){
        $meses = array(
            1 => 'Janeiro',
            2 => 'Fevereiro',
            3 => 'Março',
            4 => 'Abril',
            5 => 'Maio',
            6 => 'Junho',
            7 => 'Julho',
            8 => 'Agosto',
            9 => 'Setembro',
            10 => 'Outubro',
            11 => 'Novembro',
            12 => 'Dezembro'
        );
        $numeroMes = strtolower(substr($numeroMes, 0, 3));
        return $meses[$numeroMes];
    }
    
    function contadiasdasemana($dia_semana, $mes, $ano) { // exemplo de uso: contadiasdasemana("4","05","2020");
        $Date = new DateTime();
        $dias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
          for ($dia = 0; $dia <= $dias; $dia++) {
          $Date->setDate( $ano, $mes, $dia );
             if ($Date->format( "w" ) == $dia_semana) {
                if ($dia != 0)
                    $datas[] = $dia."/".$mes."/".$ano;
             }
          }
        return count($datas);
    }

    public static function diaSemanaString() {
        $dias = array(
            1 => 'Segunda-feira',
            2 => 'Terça-feira',
            3 => 'Quarta-feira',
            4 => 'Quinta-feira',
            5 => 'Sexta-feira',
            6 => 'Sabado',
            7 => 'Domingo',
        );
        $numeroMes = date('N');
        return date('d/m/Y') . ' '. $dias[$numeroMes];
    }
    
    public static function diaSemanaStringAdminManual($dataFormatada) {
        // echo date('Y-m-d H:i', strtotime($da));
        // $data = implode('-', array_reverse(explode('/', $da)));
        // $data2 = implode('/', array_reverse(explode('-', $da)));

        $diasemana = array('Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sabado');

        $data = date($dataFormatada);

        $diasemana_numero = date('w', strtotime($data));

        return date('d/m/Y', strtotime($dataFormatada)) . ' ' . $diasemana[$diasemana_numero];
    }

    public static function subtraiTempo($horaInicio, $horaFinal){
        $contaLetrasHorasInicio = strlen($horaInicio);
        $contaLetrasHorasFinal = strlen($horaFinal);
        
        if ($contaLetrasHorasFinal == 5) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 2);
            $tempo_fim2 = (int)substr($horaFinal, 3, 2);
        } elseif($contaLetrasHorasFinal == 6) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 3);
            $tempo_fim2 = (int)substr($horaFinal, 4, 2);
        } elseif($contaLetrasHorasFinal == 7) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 4);
            $tempo_fim2 = (int)substr($horaFinal, 5, 2);
        } elseif($contaLetrasHorasFinal == 8) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 5);
            $tempo_fim2 = (int)substr($horaFinal, 6, 2);
        } elseif($contaLetrasHorasFinal == 9) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 6);
            $tempo_fim2 = (int)substr($horaFinal, 7, 2);
        } elseif($contaLetrasHorasFinal == 10) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 7);
            $tempo_fim2 = (int)substr($horaFinal, 8, 2);
        } elseif($contaLetrasHorasFinal == 11) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 8);
            $tempo_fim2 = (int)substr($horaFinal, 9, 2);
        } elseif($contaLetrasHorasFinal == 12) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 9);
            $tempo_fim2 = (int)substr($horaFinal, 10, 2);
        }

        if ($contaLetrasHorasInicio == 5) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 2);
            $tempo_inicio2 = (int)substr($horaInicio, 3, 2);
        } elseif($contaLetrasHorasInicio == 6) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 3);
            $tempo_inicio2 = (int)substr($horaInicio, 4, 2);
        } elseif($contaLetrasHorasInicio == 7) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 4);
            $tempo_inicio2 = (int)substr($horaInicio, 5, 2);
        } elseif($contaLetrasHorasInicio == 8) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 5);
            $tempo_inicio2 = (int)substr($horaInicio, 6, 2);
        } elseif($contaLetrasHorasInicio == 9) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 6);
            $tempo_inicio2 = (int)substr($horaInicio, 7, 2);
        } elseif($contaLetrasHorasInicio == 10) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 7);
            $tempo_inicio2 = (int)substr($horaInicio, 8, 2);
        } elseif($contaLetrasHorasInicio == 11) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 8);
            $tempo_inicio2 = (int)substr($horaInicio, 9, 2);
        } elseif($contaLetrasHorasInicio == 12) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 9);
            $tempo_inicio2 = (int)substr($horaInicio, 10, 2);
        }


        $tempo_inicio1 = $tempo_inicio1 ?? 0;
        $tempo_fim2 = $tempo_fim2 ?? 0;
        $tempo_fim1 = $tempo_fim1 ?? 0;
        $tempo_inicio2 = $tempo_inicio2 ?? 0;

        $min = $tempo_fim2 - $tempo_inicio2;
        $min = ($min + 60);
        $hora = ($tempo_inicio1 - $tempo_fim1) + 1;
        
        $minutosfinal = strlen($min) <  2 ? '0' . $min : $min;
        $horafinal = strlen($hora) < 2 ? '0' . $hora : $hora; 

        return $horafinal . ':' . $minutosfinal;
    }
    
    public static function somaTempo($horaInicio, $horaFinal){
        $contaLetrasHorasInicio = strlen($horaInicio);
        $contaLetrasHorasFinal = strlen($horaFinal);

        if ($contaLetrasHorasFinal == 5) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 2);
            $tempo_fim2 = (int)substr($horaFinal, 3, 2);
        } elseif($contaLetrasHorasFinal == 6) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 3);
            $tempo_fim2 = (int)substr($horaFinal, 4, 2);
        } elseif($contaLetrasHorasFinal == 7) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 4);
            $tempo_fim2 = (int)substr($horaFinal, 5, 2);
        } elseif($contaLetrasHorasFinal == 8) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 5);
            $tempo_fim2 = (int)substr($horaFinal, 6, 2);
        } elseif($contaLetrasHorasFinal == 9) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 6);
            $tempo_fim2 = (int)substr($horaFinal, 7, 2);
        } elseif($contaLetrasHorasFinal == 10) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 7);
            $tempo_fim2 = (int)substr($horaFinal, 8, 2);
        } elseif($contaLetrasHorasFinal == 11) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 8);
            $tempo_fim2 = (int)substr($horaFinal, 9, 2);
        } elseif($contaLetrasHorasFinal == 12) {
            $tempo_fim1 = (int)substr($horaFinal, 0, 9);
            $tempo_fim2 = (int)substr($horaFinal, 10, 2);
        }

        if ($contaLetrasHorasInicio == 5) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 2);
            $tempo_inicio2 = (int)substr($horaInicio, 3, 2);
        } elseif($contaLetrasHorasInicio == 6) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 3);
            $tempo_inicio2 = (int)substr($horaInicio, 4, 2);
        } elseif($contaLetrasHorasInicio == 7) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 4);
            $tempo_inicio2 = (int)substr($horaInicio, 5, 2);
        } elseif($contaLetrasHorasInicio == 8) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 5);
            $tempo_inicio2 = (int)substr($horaInicio, 6, 2);
        } elseif($contaLetrasHorasInicio == 9) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 6);
            $tempo_inicio2 = (int)substr($horaInicio, 7, 2);
        } elseif($contaLetrasHorasInicio == 10) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 7);
            $tempo_inicio2 = (int)substr($horaInicio, 8, 2);
        } elseif($contaLetrasHorasInicio == 11) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 8);
            $tempo_inicio2 = (int)substr($horaInicio, 9, 2);
        } elseif($contaLetrasHorasInicio == 12) {
            $tempo_inicio1 = (int)substr($horaInicio, 0, 9);
            $tempo_inicio2 = (int)substr($horaInicio, 10, 2);
        }

        $hora =  $tempo_fim1 + $tempo_inicio1;
        $min = $tempo_inicio2 + $tempo_fim2;

        $minutosfinal = strlen($min) <  2 ? '0' . $min : $min;
        $horafinal = strlen($hora) < 2 ? '0' . $hora : $hora; 

        return $horafinal . ':' . $minutosfinal;
    }

    public static function somaTotalHoras($horaInicio, $horaFinal)
    {
        $tempo_fim1 = (int)substr($horaFinal, 0, 2);
        $tempo_fim2 = (int)substr($horaFinal, 3, 2);
        
        $tempo_inicio1 = (int)substr($horaInicio, 0, 2);
        $tempo_inicio2 = (int)substr($horaInicio, 3, 2);

        

        $hora = $tempo_fim1 + $tempo_inicio1;

        if ($tempo_fim2 > $tempo_inicio2) {
            $min = $tempo_fim2 + $tempo_inicio2;
        } else {
            $min = $tempo_inicio2 + $tempo_fim2;
        }

        $minutosfinal = strlen($min) <  2 ? '0' . $min : $min;
        $horafinal = strlen($hora) < 2 ? '0' . $hora : $hora; 

        return $horafinal . ':' . $minutosfinal;
    }
    
    public static function diferencaHoras($horaInicio, $horaFinal)
    {
        $tempo_fim1 = (int)substr($horaFinal, 0, 2);
        $tempo_fim2 = (int)substr($horaFinal, 3, 2);

        $tempo_inicio1 = (int)substr($horaInicio, 0, 2);
        $tempo_inicio2 = (int)substr($horaInicio, 3, 2);


        $hora = $tempo_fim1 - $tempo_inicio1;

        if ($tempo_fim2 > $tempo_inicio2) {
            $min = $tempo_fim2 - $tempo_inicio2;
        } else {
            $min = $tempo_inicio2 - $tempo_fim2;
        }

        $minutosfinal = strlen($min) <  2 ? '0' . $min : $min;
        $horafinal = strlen($hora) < 2 ? '0' . $hora : $hora; 

        return $horafinal . ':' . $minutosfinal;
    }

    public static function calculaHorasTrabalhadasEintervalos($arrayHoras, $intervalo = null)
    {        
        $entrada1 = $arrayHoras[0]['hora_ponto'] ?? null;
        $saida1 = $arrayHoras[1]['hora_ponto'] ?? null;
        $entrada2 = $arrayHoras[2]['hora_ponto'] ?? null;
        $saida2 = $arrayHoras[3]['hora_ponto'] ?? null;
        
        $permissaoentrada1 = $arrayHoras[0]['permissao'] ?? null;
        $permissaosaida1 = $arrayHoras[1]['permissao'] ?? null;
        $permissaoentrada2 = $arrayHoras[2]['permissao'] ?? null;
        $permissaosaida2 = $arrayHoras[3]['permissao'] ?? null;
        
        if ($entrada1 == null 
            OR $saida1 == null 
            OR $permissaoentrada1 == null 
            OR $permissaoentrada1 != 1
            OR $permissaosaida1 == null
            OR $permissaosaida1 != 1
        ) {
            return 'inconsistencia';
        }

        if ($entrada2 == null 
            OR $saida2 == null
            OR $permissaoentrada2 == null 
            OR $permissaoentrada2 != 1
            OR $permissaosaida2 == null
            OR $permissaosaida2 != 1
        ) {
            return 'inconsistencia';
        }

        $primeiroTempo = self::diferencaHoras($arrayHoras[0]['hora_ponto'], $arrayHoras[1]['hora_ponto']);
        $segundoTempo = self::diferencaHoras($arrayHoras[2]['hora_ponto'], $arrayHoras[3]['hora_ponto']);

        if ($intervalo === null) { // retorna horas trabalhadas
            return self::somaHoras($primeiroTempo, $segundoTempo);
        } else { // retorna intervalo
            return self::diferencaHoras($arrayHoras[1]['hora_ponto'], $arrayHoras[2]['hora_ponto']);
        }
    }

    public static function somaHoras($horaInicio, $horaFinal)
    {
        $tempo_fim1 = (int)substr($horaFinal, 0, 2);
        $tempo_fim2 = (int)substr($horaFinal, 3, 2);

        $tempo_inicio1 = substr($horaInicio, 0, 2) . ':00';
        $tempo_inicio2 = '00:' . substr($horaInicio, 3, 2);

        $hora = date('H:i', strtotime("+$tempo_fim1 hour", strtotime($tempo_inicio1)));
        $minutos = date('H:i', strtotime("+$tempo_fim2 minutes", strtotime($tempo_inicio2)));

        $min1 = (int)substr($minutos, 0, 2);
        $min2 = (int)substr($minutos, 3, 2);

        if ($min1 > 0) {
            $addHoraMinutos = date('H:i', strtotime("+$min1 hour", strtotime($hora)));
        } else {
            $addHoraMinutos = $hora;
        }

        $horaFinal = date('H:i', strtotime("+$min2 minutes", strtotime($addHoraMinutos)));
            
        return $horaFinal;
    }

    public static function subtraiHoras($arrayHoras = []) {
        if (empty($arrayHoras))
            return '00:00';
            
        $sum = strtotime('00:00:00');
        $sum2=0;  
        foreach ($arrayHoras as $v){
            $sum1=strtotime($v)-$sum;
            $sum2 = $sum2-$sum1;
        }

            $sum3=$sum+$sum2;

        return date("H:i",$sum3);
    }

    public static function saldoHoras($horaInicio, $horaFinal) // subtrai
    {
        $tempo_fim1 = (int)substr($horaFinal, 0, 2);
        $tempo_fim2 = (int)substr($horaFinal, 3, 2);

        $tempo_inicio1 = (int)substr($horaInicio, 0, 2);
        $tempo_inicio2 = (int)substr($horaInicio, 3, 2);


        $hora = $tempo_fim1 - $tempo_inicio1;

        if ($tempo_fim2 > $tempo_inicio2) {
            $min = $tempo_fim2 - $tempo_inicio2;
        } else {
            $min = $tempo_inicio2 - $tempo_fim2;
        }

        $minutosfinal = strlen($min) <  2 ? '0' . $min : $min;
        $horafinal = strlen($hora) < 2 ? '0' . $hora : $hora; 

        return $horafinal . ':' . $minutosfinal;

    }

    public static function calculaDiferentecaEntreDias($dataInicio, $dataFim) {
        $data_inicio = new DateTime($dataInicio);
        $data_fim = new DateTime($dataFim);

        // Resgata diferença entre as datas
        $dateInterval = $data_inicio->diff($data_fim);
        return $dateInterval->days + 1;
    }

    public static function exibeDatasEmUmIntervalo($dataInicio, $dataFim) {
        $dateStart 		= $dataInicio;
        $dateStart 		= implode('-', array_reverse(explode('/', substr($dateStart, 0, 10)))).substr($dateStart, 10);
        $dateStart 		= new DateTime($dateStart);

        $dateEnd 		= $dataFim;
        $dateEnd 		= implode('-', array_reverse(explode('/', substr($dateEnd, 0, 10)))).substr($dateEnd, 10);
        $dateEnd 		= new DateTime($dateEnd);

        $dateRange = array();
        while($dateStart <= $dateEnd){
            $dateRange[] = $dateStart->format('Y-m-d');
            $dateStart = $dateStart->modify('+1day');
        }

        return $dateRange;
    }

    public static function calculaHorasTrabalhadas($arrayHoras) {
        $arrayHoras[0] = array_key_exists(0, $arrayHoras) ? $arrayHoras[0] : '00:00';
        $arrayHoras[1] = array_key_exists(1, $arrayHoras) ? $arrayHoras[1] : '00:00';
        $arrayHoras[2] = array_key_exists(2, $arrayHoras) ? $arrayHoras[2] : '00:00';
        $arrayHoras[3] = array_key_exists(3, $arrayHoras) ? $arrayHoras[3] : '00:00';

        $antesDoAlmoco = self::saldoHoras($arrayHoras[0], $arrayHoras[1]);
        $aposAlmoco = self::saldoHoras($arrayHoras[2], $arrayHoras[3]);
        
        return self::somaHoras($antesDoAlmoco, $aposAlmoco);
    }

    public static function somaHorasArray($arrayHoras) {
        if (empty($arrayHoras))
            return '00:00';
            
            $time = [];

            foreach ($arrayHoras as $key => $value) {
                $time[$key] = $value;
            }

            $sum = strtotime('00:00:00'); 
              
            $totaltime = 0; 
              
            foreach( $time as $element ) { 
                // Converting the time into seconds 
                $timeinsec = strtotime($element) - $sum; 
                  
                // Sum the time with previous value 
                $totaltime = $totaltime + $timeinsec; 
            } 
              
            // Totaltime is the summation of all 
            // time in seconds 
              
            // Hours is obtained by dividing 
            // totaltime with 3600 
            $h = intval($totaltime / 3600); 
              
            $totaltime = $totaltime - ($h * 3600); 
              
            // Minutes is obtained by dividing 
            // remaining total time with 60 
            $m = intval($totaltime / 60); 
              
            // Remaining value is seconds 
            $s = $totaltime - ($m * 60); 
              
            // Printing the result 
            return ("$h:$m"); 

    }

    public static function horasTrabalhadasTotal($users_id) {
        $horasTrabalhadas = array();

        $registrosPontos = RegistroMarcacaoPontos::selectRaw(
                "registro_pontos_id,
                 hora_ponto,
                 GROUP_CONCAT(hora_ponto ORDER BY registro_pontos_id, hora_ponto) as horaslist"
            )
            ->where('users_id', $users_id)
            #->where('permissao', 1)
            ->whereNull('ocorrencias_pendentes_id')
            ->groupBy('registro_pontos_id')
            ->get();
        
        foreach ($registrosPontos as $key => $value) {
            $exp = explode(',', $value->horaslist);
            $horasTrabalhadas[$key] = self::calculaHorasTrabalhadas($exp);
        }
        
        return self::somaHorasArray($horasTrabalhadas);
    }

    public static function horasAbonadasTotal($users_id) {
        $horasTrabalhadas = array();

        $registrosPontos = RegistroMarcacaoPontos::selectRaw(
                "registro_pontos_id,
                 hora_ponto,
                 GROUP_CONCAT(hora_ponto ORDER BY registro_pontos_id, hora_ponto) as horaslist"
            )
            ->where('users_id', $users_id)
            #->where('permissao', 1)
            ->whereNotNull('ocorrencias_pendentes_id')
            ->groupBy('registro_pontos_id')
            ->get();
        
        foreach ($registrosPontos as $key => $value) {
            $exp = explode(',', $value->horaslist);
            $horasTrabalhadas[$key] = self::calculaHorasTrabalhadas($exp);
        }
        
        return self::somaHorasArray($horasTrabalhadas);
    }

    public static function horasAbonadasMesAtualTotal($users_id) {
        $horasTrabalhadas = array();

        $registrosPontos = RegistroMarcacaoPontos::selectRaw(
                "registro_pontos_id,
                 hora_ponto,
                 GROUP_CONCAT(hora_ponto ORDER BY registro_pontos_id, hora_ponto) as horaslist"
            )
            ->where('users_id', $users_id)
            ->where('permissao', 1)
            ->whereNotNull('ocorrencias_pendentes_id')
            ->whereMonth('created_at', date('m'))
            ->whereYear('created_at', date('Y'))
            ->groupBy('registro_pontos_id')
            ->get();
        
        foreach ($registrosPontos as $key => $value) {
            $exp = explode(',', $value->horaslist);
            $horasTrabalhadas[$key] = self::calculaHorasTrabalhadas($exp);
        }
        
        return self::somaHorasArray($horasTrabalhadas);
    }
    public static function horasTrabalhadasMesAtualTotal($users_id) {
        $horasTrabalhadas = array();

        $registrosPontos = RegistroMarcacaoPontos::selectRaw(
                "registro_pontos_id,
                 hora_ponto,
                 GROUP_CONCAT(hora_ponto ORDER BY registro_pontos_id, hora_ponto) as horaslist"
            )
            ->where('users_id', $users_id)
            ->where('permissao', 1)
            ->whereNull('ocorrencias_pendentes_id')
            ->whereMonth('created_at', date('m'))
            ->whereYear('created_at', date('Y'))
            ->groupBy('registro_pontos_id')
            ->get();
        
        foreach ($registrosPontos as $key => $value) {
            $exp = explode(',', $value->horaslist);
            $horasTrabalhadas[$key] = self::calculaHorasTrabalhadas($exp);
        }
        
        return self::somaHorasArray($horasTrabalhadas);
    }

    public static function mesesAnoTrabalhados($users_id, $dataInicio = null, $dataFim = null) {
        $inicio = $dataInicio !== null ?  explode('-', $dataInicio)[1] : 1;
        $fim = $dataFim !== null ?  explode('-', $dataFim)[1] : date('m');

        $registrosPontos = RegistroMarcacaoPontos::selectRaw(
                'month(created_at ) as meses,
                year(created_at ) as anos'
            )   
            ->where('users_id', $users_id)
            ->whereMonth('created_at', '>=', $inicio)
            ->whereMonth('created_at', '<=', $fim)
            ->groupByRaw("month(created_at)")
            ->orderByDesc("created_at")
            ->get();
        $data = [];
        foreach ($registrosPontos as $key => $value) {
            if ($value->meses < 10){
                $data['inicio'][$key] = $value->anos . '-0' . $value->meses . '-01' . ' 00:00:00';
                $data['fim'][$key] = $value->anos . '-0' . $value->meses . '-31' . ' 00:00:00';    
            } else {
                $data['inicio'][$key] = $value->anos . '-' . $value->meses . '-01' . ' 00:00:00';
                $data['fim'][$key] = $value->anos . '-' . $value->meses . '-31' . ' 00:00:00';
            }
        }

        return $data;
    }
    
    public static function horasAbonadasTotalByDate($users_id, $dataInicio, $dataFim) {
        $horasTrabalhadas = array();

        $registrosPontos = RegistroMarcacaoPontos::selectRaw(
                "registro_pontos_id,
                 hora_ponto,
                 GROUP_CONCAT(hora_ponto ORDER BY registro_pontos_id, hora_ponto) as horaslist"
            )
            ->where('users_id', $users_id)
            ->where('permissao', 1)
            ->whereNotNull('ocorrencias_pendentes_id')
            ->whereBetween(DB::raw('DATE(created_at)'), array($dataInicio, $dataFim))
            ->groupBy('registro_pontos_id')
            ->get();
        
        foreach ($registrosPontos as $key => $value) {
            $exp = explode(',', $value->horaslist);
            $horasTrabalhadas[$key] = self::calculaHorasTrabalhadas($exp);
        }
        
        return self::somaHorasArray($horasTrabalhadas);
    }
    public static function horasTrabalhadasTotalByDate($users_id, $dataInicio, $dataFim) {
        $horasTrabalhadas = array();

        $registrosPontos = RegistroMarcacaoPontos::selectRaw(
                "registro_pontos_id,
                 hora_ponto,
                 GROUP_CONCAT(hora_ponto ORDER BY registro_pontos_id, hora_ponto) as horaslist"
            )
            ->where('users_id', $users_id)
            ->where('permissao', 1)
            ->whereNull('ocorrencias_pendentes_id')
            ->whereBetween(DB::raw('DATE(created_at)'), array($dataInicio, $dataFim))
            ->groupBy('registro_pontos_id')
            ->get();
        
        foreach ($registrosPontos as $key => $value) {
            $exp = explode(',', $value->horaslist);
            $horasTrabalhadas[$key] = self::calculaHorasTrabalhadas($exp);
        }
        
        return self::somaHorasArray($horasTrabalhadas);
    }

    public static function dias_uteis($mes,$ano){
        $uteis = 0;
        // Obtém o número de dias no mês 
        // (http://php.net/manual/en/function.cal-days-in-month.php)
        $dias_no_mes = cal_days_in_month(CAL_GREGORIAN, $mes, $ano); 
      
        for($dia = 1; $dia <= $dias_no_mes; $dia++){
      
          // Aqui você pode verifica se tem feriado
          // ----------------------------------------
          // Obtém o timestamp
          // (http://php.net/manual/pt_BR/function.mktime.php)
          $timestamp = mktime(0, 0, 0, $mes, $dia, $ano);
          $semana    = date("N", $timestamp);
      
          if($semana < 6) $uteis++;
      
        }
      
        return $uteis;
      
    }

    public static function adicionaTempoEmHoras($horasInicio, $dias = null, $horas = null, $minutos = null, $segundos = null) {
        $startTime = date("H:i", strtotime($horasInicio));
        
        if ($dias != null AND $horas != null AND $minutos != null AND $segundos != null)
            return date('H:i',strtotime("+$dias day +$horas hour +$minutos minutes +$segundos seconds",strtotime($startTime)));
        
        if ($horas != null AND $minutos != null AND $segundos != null)    
            return date('H:i',strtotime("+$horas hour +$minutos minutes +$segundos seconds",strtotime($startTime)));
        
        if ($horas != null AND $minutos != null)        
            return date('H:i',strtotime("+$horas hour +$minutos minutes",strtotime($startTime)));
        
        if ($horas != null)        
            return date('H:i',strtotime("+$horas hour",strtotime($startTime)));

    }

    public static function contaMesesTrabalhados($users_id) {
        return RegistroMarcacaoPontos::selectRaw(
            'month(created_at ) as meses,
            year(created_at ) as anos'
        )   
        ->where('users_id', $users_id)
        ->groupByRaw("month(created_at)")
        ->get();
    }

    public static function horasPrevistasTotal($users_id, $dia_jornada) {
        $quantidadeDeDiasAoMes = 0;
        $fim12Ou24 = 0;
        $qtdMeses = count(self::contaMesesTrabalhados($users_id));

        $userEscalas =UsersEscala::select(
            'users_escalas.escalas_id',
            'inicio_expediente',
            'fim_expediente'
        )
        ->join('escalas', 'escalas.id', 'users_escalas.escalas_id')
        ->where('users_escalas.users_id', $users_id)
        ->first();

        if ($dia_jornada <= 7) {
            $quantidadeDeDiasAoMes = self::contaDiaSemanaTemNoMes($dia_jornada);
        }

        if ($dia_jornada == 8) { // apenas sabado e domingo
            $quantidadeDeDiasAoMes = self::contaDiaSemanaTemNoMes(6) + self::contaDiaSemanaTemNoMes(7) ;
        }

        if ($dia_jornada == 9) { // 12x36
            $quantidadeDeDiasAoMes = 15;
            
            $fim12Ou24 = self::adicionaTempoEmHoras($userEscalas->inicio_expediente, null, 12);    
        }
        
        if ($dia_jornada == 10) { // 24x72
            $quantidadeDeDiasAoMes = 7;
            
            $fim12Ou24 = self::adicionaTempoEmHoras($userEscalas->inicio_expediente, null, 24);    
        }

        // if ($dia_jornada == 11) { // livre T-T
        //     $quantidadeDeDiasAoMes = self::contaDiaSemanaTemNoMes($dia_jornada);
        // }

        if ($dia_jornada == 12) {
            $quantidadeDeDiasAoMes = self::dias_uteis(date('m'), date('Y'));
        }
        
        $horasTrabalhoDiario = self::diferencaHoras($userEscalas->inicio_expediente, $userEscalas->fim_expediente ?? $fim12Ou24);
        
        $intervalo = self::horasIntervalosByEscalasId($userEscalas->escalas_id);
        
        $horaTrabalhoDia = self::saldoHoras($intervalo, $horasTrabalhoDiario );
        
        return self::multiplicaHoras($horaTrabalhoDia, $quantidadeDeDiasAoMes * $qtdMeses);
    }

    public static function horasPrevistasMesAtualTotal($users_id, $dia_jornada) {
        $quantidadeDeDiasAoMes = 0;
        $fim12Ou24 = 0;

        $userEscalas =UsersEscala::select(
            'users_escalas.escalas_id',
            'inicio_expediente',
            'fim_expediente'
        )
        ->join('escalas', 'escalas.id', 'users_escalas.escalas_id')
        ->where('users_escalas.users_id', $users_id)
        ->first();

        if ($dia_jornada <= 7) {
            $quantidadeDeDiasAoMes = self::contaDiaSemanaTemNoMes($dia_jornada);
        }

        if ($dia_jornada == 8) { // apenas sabado e domingo
            $quantidadeDeDiasAoMes = self::contaDiaSemanaTemNoMes(6) + self::contaDiaSemanaTemNoMes(7) ;
        }

        if ($dia_jornada == 9) { // 12x36
            $quantidadeDeDiasAoMes = 15;
            
            $fim12Ou24 = self::adicionaTempoEmHoras($userEscalas->inicio_expediente, null, 12);    
        }
        
        if ($dia_jornada == 10) { // 24x72
            $quantidadeDeDiasAoMes = 7;
            
            $fim12Ou24 = self::adicionaTempoEmHoras($userEscalas->inicio_expediente, null, 24);    
        }

        // if ($dia_jornada == 11) { // livre T-T
        //     $quantidadeDeDiasAoMes = self::contaDiaSemanaTemNoMes($dia_jornada);
        // }

        if ($dia_jornada == 12) {
            $quantidadeDeDiasAoMes = self::dias_uteis(date('m'), date('Y'));
        }
        
        $horasTrabalhoDiario = self::diferencaHoras($userEscalas->inicio_expediente, $userEscalas->fim_expediente ?? $fim12Ou24);
        
        $intervalo = self::horasIntervalosByEscalasId($userEscalas->escalas_id);
        
        $horaTrabalhoDia = self::saldoHoras($intervalo, $horasTrabalhoDiario );
        
        return self::multiplicaHoras($horaTrabalhoDia, $quantidadeDeDiasAoMes);
    }

    public static function multiplicaHoras($hora, $multiplicar) {
        $time = $hora;
        $parts=explode(':',$time);
        $seconds = ($parts[0]*60) + $parts[1];
        $total = $seconds * $multiplicar;

        return sprintf('%02d',floor($total/60)).':'. sprintf('%02d',$total%60);
    }

    public static function horasIntervalosByEscalasId($escalas_id) {
        $intervalos = Intervalo::where('escalas_id', $escalas_id)
            ->where('status', 1)
            ->get();

        foreach ($intervalos as $key => $value) {
            $horasIntervalos[$key] = Utils::diferencaHoras($value->inicio_intervalo, $value->fim_intervalo);
        }

        if (empty($horasIntervalos))
            return 0;
        
        return self::somaHorasArray($horasIntervalos);
    }

    public static function contaDiaSemanaTemNoMes($dia_jornada, $mes = null, $ano = null) { // exemplo de uso: contadiasdasemana("4","05","2020");
        $mes = $mes ?? date('m');
        $ano = $ano ?? date('Y');
        $dia_semana = $dia_jornada;
        $datas = [];

        if ($dia_jornada == 7) $dia_semana = 0; // DOMINGO = 0

        $Date = new DateTime();
        $dias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
          for ($dia = 0; $dia <= $dias; $dia++) {
          $Date->setDate( $ano, $mes, $dia );
             if ($Date->format( "w" ) == $dia_semana) {
                if ($dia != 0)
                    $datas[$dia] = $dia."/".$mes."/".$ano;
             }
          }
        return count($datas);
    }

    public static function horasPrevistasByDate($users_id, $dia_jornada) {
        $quantidadeDeDiasAoMes = 0;
        $fim12Ou24 = 0;

        $userEscalas =UsersEscala::select(
            'users_escalas.escalas_id',
            'inicio_expediente',
            'fim_expediente'
        )
        ->join('escalas', 'escalas.id', 'users_escalas.escalas_id')
        ->where('users_escalas.users_id', $users_id)
        ->first();

        if ($dia_jornada <= 7) {
            $quantidadeDeDiasAoMes = self::contaDiaSemanaTemNoMes($dia_jornada);
        }

        if ($dia_jornada == 8) { // apenas sabado e domingo
            $quantidadeDeDiasAoMes = self::contaDiaSemanaTemNoMes(6) + self::contaDiaSemanaTemNoMes(7) ;
        }

        if ($dia_jornada == 9) { // 12x36
            $quantidadeDeDiasAoMes = 15;
            
            $fim12Ou24 = self::adicionaTempoEmHoras($userEscalas->inicio_expediente, null, 12);    
        }
        
        if ($dia_jornada == 10) { // 24x72
            $quantidadeDeDiasAoMes = 7;
            
            $fim12Ou24 = self::adicionaTempoEmHoras($userEscalas->inicio_expediente, null, 24);    
        }

        // if ($dia_jornada == 11) { // livre T-T
        //     $quantidadeDeDiasAoMes = self::contaDiaSemanaTemNoMes($dia_jornada);
        // }

        if ($dia_jornada == 12) {
            $quantidadeDeDiasAoMes = self::dias_uteis(date('m'), date('Y'));
        }
        
        $horasTrabalhoDiario = self::diferencaHoras($userEscalas->inicio_expediente, $userEscalas->fim_expediente ?? $fim12Ou24);
        
        $intervalo = self::horasIntervalosByEscalasId($userEscalas->escalas_id);
        
        $horaTrabalhoDia = self::saldoHoras($intervalo, $horasTrabalhoDiario );
        
        return self::multiplicaHoras($horaTrabalhoDia, $quantidadeDeDiasAoMes);
    }

    function timeToSec($time) {
        $hours = substr($time, 0, -6);
        $minutes = substr($time, -5, 2);
        $seconds = substr($time, -2);
        
        return $hours * 3600 + $minutes * 60 + $seconds;
    }
    
    function secToTime($seconds) {
        $hours = floor($seconds / 3600);
        $minutes = floor($seconds % 3600 / 60);
        $seconds = $seconds % 60;
        
        return sprintf("%d:%02d:%02d", $hours, $minutes, $seconds);
    }
    
}
