<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isMaster', function($user){
            return $user->type === 'master';
        });

        Gate::define('isAdmin', function($user){
            return $user->type === 'admin';
        });

        Gate::define('isUser', function($user){
            return $user->type === 'user';
        });

        Gate::define('isEmpresa', function($user){
            return $user->type === 'empresa';
        });

        Gate::define('accessbyuser', 'App\Policies\AccessPolicy@accessbyuser');

        Passport::routes();
    }
}
