<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Models\Admin\PermissoesUsuarios;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'cpf', 'rg', 'photo', 'type', 'empresas_id', 'idtipo_usuario', 'matricula', 'pis', 'status', 'finger_print', 'sistema_operacional', 'sistema_operacional_versao', 'dispositivo_para_acesso', 'navegador'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function endereco(){
        return $this->hasOne('App\Models\Comum\UsersEndereco', 'users_id', 'id')->where('status', 1);
    }

    public function users_filiais()
    {
        return $this->hasMany('App\Models\Comum\UsersManyEmpresasSetores', 'users_id', 'id')
            ->where('status', 1);
    }

    public function users_escalas()
    {
        return $this->hasMany('App\Models\Comum\UsersEscala', 'users_id', 'id')
            ->where('empresas_id', auth('api')->user()->empresas_id)
            ->where('status', 1);;
    }
    
    public function users_calendarios()
    {
        return $this->hasMany('App\Models\Comum\UsersCalendario', 'users_id', 'id')
            ->where('empresas_id', auth('api')->user()->empresas_id)
            ->where('status', 1);
    }
    
    public function users_ferias()
    {
        return $this->hasMany('App\Models\Comum\UsersFeria', 'users_id', 'id')
            ->where('empresas_id', auth('api')->user()->empresas_id)
            ->where('status', 1);
    }
    
    public function users_cercasvirtuais()
    {
        return $this->hasMany('App\Models\Comum\UsersCercasVirtuais', 'users_id', 'id')
            ->where('empresas_id', auth('api')->user()->empresas_id)
            ->where('status', 1);
    }

    public static function permissoes()
    {
        $tipoPermissao = auth()->user()->idtipo_usuario;
        
        $permisso = null;

            $permisso = PermissoesUsuarios::select('idpermissao')
                ->where('idtipo_usuario', $tipoPermissao)
                ->get();

        return $permisso;
    }
}
