<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Admin\DispositivosAutorizado;
use App\Models\Admin\Empresa;
use App\Models\Comum\Colaborador;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {   
        $input = $request->all();
        $this->validate($request, [
            'matricula' => 'required',
            'password' => 'required',
        ]);

        $use = $input['matricula'];
          
        $user = User::select(
                'users.id',
                'users.name',
                'users.email',
                'users.password',
                'users.matricula',
                'users.empresas_id',
                'users.idtipo_usuario',
                'empresas.codigo'
            )
            ->join('empresas', 'empresas.id', 'users.empresas_id')
            ->where('empresas.codigo', $input['codigo'])
            ->where(function($query) use ($use){
                $query->where('users.matricula', $use);
                $query->orWhere('users.email', $use);
            })
            ->first();
        
        if (isset($user->status)) {
            if ($user->status == 0) 
            return redirect('login')->with('status', 'Usuário Bloqueado');    
        }

        if($user)
        {
            $dispositivo = $this->verificaDispositivo($user, $input);
            if ($dispositivo != 'OK') {
                return redirect('login')->with('status', $dispositivo); 
            }

            $eita = Hash::check($input['password'], $user->password);
            if ($eita) {
                Auth::login($user);
                return redirect()->route('home');
            } else {
                return redirect('login')->with('status', 'Senha incorreta');    
            }
        }else{
            return redirect('login')->with('status', 'Usuário incorreto');    
        }
    }

    public function verificaDispositivo($user, $input){
        $sessao = 'OK';

        if ($user->idtipo_usuario != 3000){
            session(['permissoes_dispositivos' => ['permite_dispositivo' => 'OK', 'user_dispositivo' => $input]]);
            return 'OK';
        }

        if ($user->idtipo_usuario == 3000 AND $input["dispositivo_para_acesso"] == "celular")
            return 'Sem permissão para acessar o sistema, por favor, utilize um computador ou baixe o aplicativo.';
        

        $colaborador = Colaborador::select(
                'dispositivos_autorizados_web'
            )
            ->where('users_id', $user->id)
            ->where('empresas_id', $user->empresas_id)
            ->first();

        if ($colaborador->dispositivos_autorizados_web == 0) {
            session(['permissoes_dispositivos' => ['permite_dispositivo' => 'OK', 'user_dispositivo' => $input]]);
            return 'OK';
        }

        $dispositivo = DispositivosAutorizado::where('users_id', $user->id)
                    ->where('empresas_id', $user->empresas_id)
                    ->where('finger_print', $input['finger_print'])
                    ->where('dispositivo_para_acesso', 'computador')
                    ->first();

        if (!isset($dispositivo))
            $sessao = 'SEM_DISPOSITIVO_CADASTRADO';

        if (isset($dispositivo) AND $dispositivo->status == 0)
            $sessao = 'AGUARDANDO';

        session(['permissoes_dispositivos' => ['permite_dispositivo' => $sessao, 'user_dispositivo' => $input]]);

        return 'OK';
    }

    
}
