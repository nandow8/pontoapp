<?php

namespace App\Http\Controllers\API\Comum\Colaboradorchat;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Comum\Chat;
use Illuminate\Http\Request;
use App\Models\Admin\Log;
use App\Models\Comum\Chatmessage;
use App\User;

class ColaboradorChatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $chat = Chat::where(function($query) use ($search){
                $query->where('status', 1)
                    ->orWhere('email','LIKE', '%' . $search. '%');
            })->paginate(20);
        }else{
            $chat = Chat::where('status', 1)->latest()->paginate(20);
        }

        return $chat;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $chat = Chat::where('status', 1)
            ->latest()
            ->paginate(25);

        return $chat;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;
        $data['users_id'] = auth('api')->user()->id;

        $chat = Chat::where('id', $data['id'])->where('status', 1)->count();

        if ($chat < 1) {
            $chat = Chat::create([
                'empresas_id' => $data['empresas_id'],
                'users_id' => $data['users_id'],
                'chamado' => 'aberto'
            ]);
        } else {
            $chat = Chat::where('users_id', $data['users_id'])->update(['chamado' => 'aberto']);
        }
        
        $idchat = $chat->id ?? $data['id'];

        $chatmessage = Chatmessage::create([
            'empresas_id' => $data['empresas_id'],
            'users_id' => $data['users_id'],
            'chats_id'=> $idchat,
            'mensagem' => $data['mensagem'],
            'colaborador_ou_suporte'=> 'colaborador',
        ]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Chat::class,
            'tabela_id' => $idchat,
            'atividade' => 'cadastrou'
        ]);

        
        // broadcast(new \App\Events\SendMessage($chatmessage, User::findOrFail($data['users_id'])));

        return response()->json($chatmessage, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chat = Chat::select(
                'chats.id',
                'chatmessages.mensagem',
                'chatmessages.colaborador_ou_suporte',
                'chatmessages.created_at',
                'users.photo'
            )
            ->join('chatmessages', 'chatmessages.chats_id', 'chats.id')
            ->join('users', 'users.id', 'chats.users_id')
            ->where('chats.users_id', $id)
            ->where('chats.status', 1)
            ->get();

        return $chat;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $chat = Chat::findOrFail($id);
        $chat->update($request->all());

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Chat::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($chat, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Chat::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Chat::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
