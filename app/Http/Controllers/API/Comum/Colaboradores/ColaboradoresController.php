<?php

namespace App\Http\Controllers\API\Comum\Colaboradores;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Admin\Empresa;
use App\Models\Comum\Colaborador;
use App\Models\Comum\UsersCalendario;
use App\Models\Comum\UsersEscala;
use App\Models\Comum\UsersFeria;
use App\Models\Comum\UsersCercasVirtuais;
use Illuminate\Http\Request;
use App\Models\Admin\Log;
use App\Models\Comum\UsersColaboradores;
use App\Models\Comum\UsersEndereco;
use App\User;
use Hash;
use App\Utils;

class ColaboradoresController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function search(){

        // if ($search = \Request::get('q')) {
        //     $colaboradore = Colaborador::where(function($query) use ($search){
        //         $query->where('status', 1)
        //             ->orWhere('email','LIKE', '%' . $search. '%');
        //     })->where('empresas_id', auth('api')->user()->empresas_id)->paginate(20);
        // }else{
        //     $colaboradore = Colaborador::where('status', 1)->latest()->paginate(20);
        // }
            
        $userAuth = auth('api')->user();

        if ($search = \Request::get('q')) {
            $colaboradore = Colaborador::selectRaw(
                'users.id,
                users.name,
                filiais.name AS filiais_name,
                empresas.name AS empresas_name'
            )
            ->join('users', 'users.id', 'users_colaboradores.users_id')
            ->join('filiais', 'filiais.id', 'users_colaboradores.filiais_id')
            ->join('empresas', 'empresas.id', 'users_colaboradores.empresas_id')
            ->where(function($query) use ($search){
                    $query->where('users.name','LIKE', '%' . $search. '%')
                        ->orWhere('filiais.name','LIKE', '%' . $search. '%')
                        ->orWhere('empresas.name','LIKE', '%' . $search. '%');
                })
            ->where('users_colaboradores.status', 1)
            ->orderBy('users_colaboradores.created_at')
            ->paginate(20);
        } else {
            return $this->index();
        }

        return [
            'colaboradores' => $colaboradore,
            'codigoEmpresa' => 'Recarregue a página.'
        ];

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colaboradores = Colaborador::selectRaw(
                'users.id,
                users.name,
                filiais.name AS filiais_name,
                empresas.name AS empresas_name'
            )
            ->join('users', 'users.id', 'users_colaboradores.users_id')
            ->join('filiais', 'filiais.id', 'users_colaboradores.filiais_id')
            ->join('empresas', 'empresas.id', 'users_colaboradores.empresas_id')
            ->where('users_colaboradores.status', 1)
            ->orderBy('users_colaboradores.created_at')
            ->paginate(25);

            return [
                'colaboradores' => $colaboradores,
                'codigoEmpresa' => Empresa::where('id', auth('api')->user()->empresas_id)->withoutGlobalScope('App\Scopes\VisibleScope')->first()['codigo']
            ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $this->validate($request, [
            'photo' => 'required',
            'url_qrcode' => 'required',
            'cargos_id' => 'required',
            'setores_id' => 'required',
            'filiais_id' => 'required',
            'name' => 'required',
            // 'matricula' => 'required|unique:users,matricula',
            // 'email' => 'required|unique:users,email',
            'data_admissao' => 'required',
            // 'cpf' => 'required|unique:users,cpf',
            // 'rg' => 'required|unique:users,rg,',
            // 'pis' => 'required|unique:users,pis,',
            'cep' => 'required',
            'numero' => 'required',
            'logradouro' => 'required',
            'uf' => 'required',
            'localidade' => 'required',
            'bairro' => 'required',
            'telefone' => 'required',
        ]);

        $userCredentials = auth('api')->user();

        $verificaCredenciais = $this->verificaColaboradorMesmaEmpresa($request->all(), $userCredentials);
        if($verificaCredenciais !== null) {
            return json_encode($verificaCredenciais);
        }

        $data = $request->all();
        $data['empresas_id'] = $userCredentials->empresas_id;

        if($request->photo){
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('img/colaboradores/').$name);
            $request->merge(['photo' => $name]);
            $userPhoto = public_path('img/colaboradores/').$name;

            // define('UPLOAD_DIR', 'img/colaboradores/');
            // $image_parts = explode(";base64,", $request->photo);
            // $image_type_aux = explode("image/", $image_parts[0]);
            // $image_base64 = base64_decode($image_parts[1]);
            // $pos = substr($image_parts[0], strpos($image_parts[0], '/') + 1, 5) ;
            // $name = uniqid() . '.' . $pos;
            // file_put_contents(UPLOAD_DIR . $name, $image_base64);
        }

        $senha = $data['acesso_ao_sistema_senha'] == null ? str_replace("-", "",str_replace(".", "", $data['cpf'])) : $data['acesso_ao_sistema_senha'];
        
        $user = User::create([
            'empresas_id' => $userCredentials->empresas_id,
            'name' => $data['name'],
            'email' => $data['email'],
            'type' => 'colaborador',
            'photo' => '/img/colaboradores/'.$name,
            'cpf' => $data['cpf'],
            'matricula' => $data['matricula'],
            'rg' => $data['rg'],
            'pis' => $data['pis'],
            'idtipo_usuario' => 3000, // colaborador 3000
            'password' => Hash::make($senha),
        ]);
        
        $demissao = null;
        if($data['data_demissao']) {
            $demissao = Utils::formatDateTimeToBD($data['data_demissao'] . date('H:i'));
        }

        $colaboradore = new Colaborador;
            $colaboradore->users_id = $user->id;
            $colaboradore->empresas_id = $userCredentials->empresas_id;
            $colaboradore->url_qrcode = $data['url_qrcode'];
            $colaboradore->filiais_id = $data['filiais_id'];
            $colaboradore->telefone = $data['telefone'];
            $colaboradore->cargos_id = $data['cargos_id'];
            $colaboradore->setores_id = $data['setores_id'];
            $colaboradore->matricula = $data['matricula'];
            $colaboradore->data_admissao = Utils::formatDateTimeToBD($data['data_admissao'] . date('H:i'));
            $colaboradore->data_demissao = $demissao;
            $colaboradore->auth_user_id = $userCredentials->id;
            $colaboradore->enviar_instrucoes_email = $data['enviar_instrucoes_email'];
            $colaboradore->enviar_comprovantes_email = $data['enviar_comprovantes_email'];
            $colaboradore->acesso_ao_sistema = $data['acesso_ao_sistema'];
            
            $colaboradore->permite_marcacao_ponto_app = $data['permite_marcacao_ponto_app'];
            $colaboradore->dispositivos_autorizados_app = $data['dispositivos_autorizados_app'];
            $colaboradore->permite_ponto_aplicativo = $data['permite_ponto_aplicativo'];
            $colaboradore->permite_marcacao_ponto_web = $data['permite_marcacao_ponto_web'];
            $colaboradore->dispositivos_autorizados_web = $data['dispositivos_autorizados_web'];
            $colaboradore->permite_ponto_web = $data['permite_ponto_web'];

            $colaboradore->consultas_ponto = $data['consultas_ponto'];
            $colaboradore->consultas_ponto_select_visualizar_editar = $data['consultas_ponto_select_visualizar_editar'];
            $colaboradore->consultas_ponto_dias = $data['consultas_ponto_dias'];
            $colaboradore->consultas_ponto_vizualizar_ultimas_horas = $data['consultas_ponto_vizualizar_ultimas_horas'];
            $colaboradore->consultas_ponto_vizualizar_ultimas_horas_qtd_dias = $data['consultas_ponto_vizualizar_ultimas_horas_qtd_dias'];

            $colaboradore->noturna_banco_extra = $data['noturna_banco_extra'];
            $colaboradore->contabilizar_normal_ou_acrescimo = $data['contabilizar_normal_ou_acrescimo'];
            if(
                $data['contabilizar_normal_ou_acrescimo'] == false 
                OR $data['excedente_um_dois'] != 'ate_hora_noturna2'
                OR $data['noturna_banco_extra'] == 'horas_extras'
                ) {
                $colaboradore->excedente_um_dois = $data['excedente_um_dois'];
                $colaboradore->excedente_um_hora = $data['excedente_um_hora'];
                $colaboradore->permite_hora_trabalho = $data['permite_hora_trabalho'];
                $colaboradore->porcentagem_hora_trabalho = $data['porcentagem_hora_trabalho'];
                $colaboradore->tempo_horas_noturnas_inicio = $data['tempo_horas_noturnas_inicio'];
                $colaboradore->tempo_horas_noturnas_fim = $data['tempo_horas_noturnas_fim'];
                $colaboradore->casojornada_mista = $data['casojornada_mista'];
                $colaboradore->dsr__folga_cemporcento = $data['dsr__folga_cemporcento'];
                $colaboradore->permite_considerar_porcentagem_trabalho = $data['permite_considerar_porcentagem_trabalho'];
                $colaboradore->porcentagem_considerar_porcentagem_trabalho = $data['porcentagem_considerar_porcentagem_trabalho'];
                $colaboradore->permite_hora_trabalho_dois = $data['permite_hora_trabalho_dois'];
                $colaboradore->porcentagem_hora_trabalho_dois = $data['porcentagem_hora_trabalho_dois'];
            }

            $colaboradore->status = 1;
        $colaboradore->save();

        UsersEndereco::create([
            'users_id' => $user->id,
            'empresas_id' => $userCredentials->empresas_id,
            'auth_user_id' => $userCredentials->id,
            'cep' => $data['cep'],
            'logradouro' => $data['logradouro'],
            'numero' => $data['numero'],
            'complemento' => $data['complemento'],
            'uf' => $data['uf'],
            'localidade' => $data['localidade'],
            'bairro' => $data['bairro'],
            'telefone' => $data['telefone'],
            'telefone_alternativo' => $data['telefone_alternativo'],
            'email' => $data['email'],
        ]);

        foreach ($data['rows_calendarios'] as $value) {
            UsersCalendario::create([
                'users_id' => $user->id,
                'empresas_id' => $userCredentials->empresas_id,
                'calendarios_id' => $value['id_calendario'],
                'calendario_data_vigente' => Utils::formatDateToBD($value['calendario_data_vigente']),
            ]);
        }

        foreach ($data['rows_escalas'] as $value) {
            UsersEscala::create([
                'users_id' => $user->id,
                'empresas_id' => $userCredentials->empresas_id,
                'escalas_id' => $value['id_escala'],
                'dia_jornada' => $value['tipos'],
                'data_vigente' => Utils::formatDateToBD($value['data_vigente']),

            ]);
        }

        foreach ($data['rows_ferias'] as $value) {
            UsersFeria::create([
                'users_id' => $user->id,
                'empresas_id' => $userCredentials->empresas_id,
                'ferias_inicio' => Utils::formatDateToBD($value['ferias_inicio']),
                'ferias_fim' => Utils::formatDateToBD($value['ferias_fim']),
                'impedir_registro_ferias' => $value['impedir_registro_ferias'],
            ]);
        }
        
        foreach ($data['rows_cercasvirtuais'] as $value) {
            UsersCercasVirtuais::create([
                'users_id' => $user->id,
                'empresas_id' => $userCredentials->empresas_id,
                'idunico' => $value['cerca_idunico'],
                'descricao' => $value['cerca_descricao'],
                'lat' => $value['cerca_lat'],
                'lng' => $value['cerca_lng'],
                'radius' => $value['cerca_radius'],
            ]);
        }
        

        Log::create([
            'empresas_id' => $userCredentials->empresas_id,
            'users_id' => $userCredentials->id,
            'tabela' => Colaborador::class,
            'tabela_id' => $colaboradore->id,
            'atividade' => 'cadastrou o colaborador: ' . User::where('id', $user->id)->first()['name']
        ]);

        return response()->json($colaboradore, 201);
    }

    public function verificaColaboradorMesmaEmpresa($request, $userCredentials, $update = false)
    {
        $matricula = User::join('empresas', 'empresas.id', 'users.empresas_id');
        $matricula = $matricula->where('users.matricula', $request['matricula']);
        $matricula = $matricula->where('empresas.id', $userCredentials->empresas_id);
        if ($update)
            $matricula = $matricula->where('users.id', $userCredentials->users_id);
        $matricula = $matricula->first();
            
        if ($matricula) {
            return 'matricula';
        }

        $email = User::join('empresas', 'empresas.id', 'users.empresas_id');
        $email = $email->where('users.email', $request['email']);
        $email = $email->where('empresas.id', $userCredentials->empresas_id);
        if ($update)
            $email = $email->where('users.id', $userCredentials->users_id);
        $email = $email->first();

        if ($email) {
            return 'email';
        }

        $cpf = User::join('empresas', 'empresas.id', 'users.empresas_id');
        $cpf = $cpf->where('users.cpf', $request['cpf']);
        $cpf = $cpf->where('empresas.id', $userCredentials->empresas_id);
        if ($update)
            $cpf = $cpf->where('users.id', $userCredentials->users_id);
        $cpf = $cpf->first();

        if ($cpf) {
            return 'cpf';
        }

        $rg = User::join('empresas', 'empresas.id', 'users.empresas_id');
        $rg = $rg->where('users.rg', $request['rg']);
        $rg = $rg->where('empresas.id', $userCredentials->empresas_id);
        if ($update)
            $rg = $rg->where('users.id', $userCredentials->users_id);
        $rg = $rg->first();

        if ($rg) {
            return 'rg';
        }
            
        $pis = User::join('empresas', 'empresas.id', 'users.empresas_id');
        $pis = $pis->where('users.pis', $request['pis']);
        $pis = $pis->where('empresas.id', $userCredentials->empresas_id);
        if ($update)
            $pis = $pis->where('users.id', $userCredentials->users_id);
        $pis = $pis->first();

        if ($pis) {
            return 'pis';
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $colaboradore = Colaborador::where('users_id', $id)->first();
        $colaboradore->push($colaboradore->users);
        $colaboradore->push($colaboradore->users->users_escalas);
        $colaboradore->push($colaboradore->users->users_calendarios);
        $colaboradore->push($colaboradore->users->users_ferias);
        $colaboradore->push($colaboradore->users->users_cercasvirtuais);
        $colaboradore->push($colaboradore->users_enderecos);
        $colaboradore->push($colaboradore->users_dispositivo);

        $escalas = array();
        foreach ($colaboradore->users->users_escalas as $key => $value) {
            $escalas[$key] = $value->escalas;
        }
        $colaboradore->push($escalas);

        $calendarios = array();
        foreach ($colaboradore->users->users_calendarios as $key => $value) {
            $calendarios[$key] = $value->calendarios;
        }
        $colaboradore->push($calendarios);

        return $colaboradore;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $currentUser = User::find( $data['id']);

        $this->validate($request, [
            //'photo' => 'required',
            'url_qrcode' => 'required',
            'cargos_id' => 'required',
            'setores_id' => 'required',
            'filiais_id' => 'required',
            'name' => 'required',
            // 'matricula' => 'required',
            // 'email' => 'required',
            'data_admissao' => 'required',
            // 'cpf' => 'required|unique:users,cpf,'. $currentUser->id,
            // 'rg' => 'required|unique:users,rg,'. $currentUser->id,
            // 'pis' => 'required|unique:users,pis,'. $currentUser->id,
            'cep' => 'required',
            'numero' => 'required',
            'logradouro' => 'required',
            'uf' => 'required',
            'localidade' => 'required',
            'bairro' => 'required',
            'telefone' => 'required',
        ]);

        $userAuth = auth('api')->user();

        $verificaCredenciais = $this->verificaColaboradorMesmaEmpresa($request->all(), $userAuth, true);
        if($verificaCredenciais !== null) {
            return json_encode($verificaCredenciais);
        }
             
        $name = '';
        // if($request->photo != $currentUser->photo){
        //     $name = time().'.' . $request->photo;
        //     \Image::make($request->photo)->save(public_path('img/colaboradores/').$name);
        //     $request->merge(['photo' => $name]);
        //     $currentPhoto = public_path('img/colaboradores/').$name;
        // } 

        $senha = $data['acesso_ao_sistema_senha'] == null ? str_replace("-", "",str_replace(".", "", $data['cpf'])) : $data['acesso_ao_sistema_senha'];
        
        $user = User::where('id', $data['id'])->first();        
            $user->empresas_id = $userAuth->empresas_id;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->type = 'colaborador';
            if($name != '')
                $user->photo = '/img/colaboradores/'. $name;
            $user->cpf = $data['cpf'];
            $user->matricula = $data['matricula'];
            $user->rg = $data['rg'];
            $user->pis = $data['pis'];
            $user->password = Hash::make($senha);
        $user->save();

        $demissao = null;
        if($data['data_demissao'] != 'Invalid date' AND $data['data_demissao'] != null) {
            $demissao = Utils::formatDateTimeToBD($data['data_demissao'] .  date('H:i'));
        }
        
        $colaboradore = Colaborador::where('users_id', $user->id)->first();
            $colaboradore->empresas_id = $userAuth->empresas_id;
            $colaboradore->url_qrcode = $data['url_qrcode'];
            $colaboradore->filiais_id = $data['filiais_id'];
            $colaboradore->telefone = $data['telefone'];
            $colaboradore->cargos_id = $data['cargos_id'];
            $colaboradore->setores_id = $data['setores_id'];
            $colaboradore->matricula = $data['matricula'];
            $colaboradore->data_admissao = Utils::formatDateTimeToBD($data['data_admissao'] . date('H:i'));
            $colaboradore->data_demissao = $demissao;
            $colaboradore->auth_user_id = $userAuth->id;
            $colaboradore->enviar_instrucoes_email = $data['enviar_instrucoes_email'];
            $colaboradore->enviar_comprovantes_email = $data['enviar_comprovantes_email'];
            $colaboradore->permite_marcacao_ponto_app = $data['permite_marcacao_ponto_app'];
            $colaboradore->dispositivos_autorizados_app = $data['dispositivos_autorizados_app'];
            $colaboradore->permite_ponto_aplicativo = $data['permite_ponto_aplicativo'];
            $colaboradore->permite_marcacao_ponto_web = $data['permite_marcacao_ponto_web'];
            $colaboradore->dispositivos_autorizados_web = $data['dispositivos_autorizados_web'];
            $colaboradore->permite_ponto_web = $data['permite_ponto_web'];
            $colaboradore->consultas_ponto = $data['consultas_ponto'];
            $colaboradore->consultas_ponto_select_visualizar_editar = $data['consultas_ponto_select_visualizar_editar'];
            $colaboradore->consultas_ponto_dias = $data['consultas_ponto_dias'];
            $colaboradore->consultas_ponto_vizualizar_ultimas_horas = $data['consultas_ponto_vizualizar_ultimas_horas'];
            $colaboradore->consultas_ponto_vizualizar_ultimas_horas_qtd_dias = $data['consultas_ponto_vizualizar_ultimas_horas_qtd_dias'];
            $colaboradore->status = 1;
            $colaboradore->acesso_ao_sistema = $data['acesso_ao_sistema'];
            
            $colaboradore->noturna_banco_extra = $data['noturna_banco_extra'];
            $colaboradore->contabilizar_normal_ou_acrescimo = $data['contabilizar_normal_ou_acrescimo'];
            if(
                $data['contabilizar_normal_ou_acrescimo'] == 1 
                OR $data['excedente_um_dois'] == 'ate_hora_noturna2'
                OR $data['noturna_banco_extra'] == 'horas_extras'
                ) {
                $colaboradore->excedente_um_dois = $data['excedente_um_dois'];
                $colaboradore->excedente_um_hora = $data['excedente_um_hora'];
                $colaboradore->permite_hora_trabalho = $data['permite_hora_trabalho'];
                $colaboradore->porcentagem_hora_trabalho = $data['porcentagem_hora_trabalho'];
                $colaboradore->tempo_horas_noturnas_inicio = $data['tempo_horas_noturnas_inicio'];
                $colaboradore->tempo_horas_noturnas_fim = $data['tempo_horas_noturnas_fim'];
                $colaboradore->casojornada_mista = $data['casojornada_mista'];
                $colaboradore->dsr__folga_cemporcento = $data['dsr__folga_cemporcento'];
                $colaboradore->permite_considerar_porcentagem_trabalho = $data['permite_considerar_porcentagem_trabalho'];
                $colaboradore->porcentagem_considerar_porcentagem_trabalho = $data['porcentagem_considerar_porcentagem_trabalho'];
                $colaboradore->permite_hora_trabalho_dois = $data['permite_hora_trabalho_dois'];
                $colaboradore->porcentagem_hora_trabalho_dois = $data['porcentagem_hora_trabalho_dois'];
            }
            


        $colaboradore->save();

        UsersEndereco::where('users_id', $user->id)->update([
            'users_id' => $user->id,
            'empresas_id' => $userAuth->empresas_id,
            'auth_user_id' => $userAuth->id,
            'cep' => $data['cep'],
            'logradouro' => $data['logradouro'],
            'numero' => $data['numero'],
            'complemento' => $data['complemento'],
            'uf' => $data['uf'],
            'localidade' => $data['localidade'],
            'bairro' => $data['bairro'],
            'telefone' => $data['telefone'],
            'telefone_alternativo' => $data['telefone_alternativo'],
            'email' => $data['email'],
        ]);

        UsersCalendario::where('users_id', $user->id)->update(['status' => 0]);
        foreach ($data['rows_calendarios'] as $value) {
            UsersCalendario::create([
                'users_id' => $user->id,
                'empresas_id' => $userAuth->empresas_id,
                'calendarios_id' => $value['id_calendario'],
                'calendario_data_vigente' => Utils::formatDateToBD($value['calendario_data_vigente']),
            ]);
        }

        UsersEscala::where('users_id', $user->id)->update(['status' => 0]);
        foreach ($data['rows_escalas'] as $value) {
            UsersEscala::create([
                'users_id' => $user->id,
                'empresas_id' => $userAuth->empresas_id,
                'escalas_id' => $value['id_escala'],
                'dia_jornada' => $value['tipos'],
                'data_vigente' => Utils::formatDateToBD($value['data_vigente']),

            ]);
        }

        UsersFeria::where('users_id', $user->id)->update(['status' => 0]);
        foreach ($data['rows_ferias'] as $value) {
            UsersFeria::create([
                'users_id' => $user->id,
                'empresas_id' => $userAuth->empresas_id,
                'ferias_inicio' => Utils::formatDateToBD($value['ferias_inicio']),
                'ferias_fim' => Utils::formatDateToBD($value['ferias_fim']),
                'impedir_registro_ferias' => $value['impedir_registro_ferias'],
            ]);
        }

        UsersCercasVirtuais::where('users_id', $user->id)->update(['status' => 0]);
        foreach ($data['rows_cercasvirtuais'] as $value) {
            UsersCercasVirtuais::create([
                'users_id' => $user->id,
                'empresas_id' => $userAuth->empresas_id,
                'idunico' => $value['cerca_idunico'],
                'descricao' => $value['cerca_descricao'],
                'lat' => $value['cerca_lat'],
                'lng' => $value['cerca_lng'],
                'radius' => $value['cerca_radius'],
            ]);
        }

        Log::create([
            'empresas_id' => $userAuth->empresas_id,
            'users_id' => $userAuth->id,
            'tabela' => Colaborador::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($colaboradore, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);
        UsersColaboradores::where('users_id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Colaborador::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }

    public function ultimoUserId()
    {
        $user['uid'] = date('dmY') . date('His');
        $user['empresa_id'] = auth('api')->user()->empresas_id;
        
        return $user;
    }
}
