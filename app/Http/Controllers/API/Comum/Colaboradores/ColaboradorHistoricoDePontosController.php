<?php

namespace App\Http\Controllers\API\Comum\Colaboradores;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Admin\Log;
use App\Models\Comum\RegistroMarcacaoPontos;
use App\Models\Comum\UsersColaboradores;
use App\Utils;

class ColaboradorHistoricoDePontosController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function index(){
        $configUsersColaboradores = UsersColaboradores::select(
            'consultas_ponto_select_visualizar_editar',
            'consultas_ponto_dias'
        )
        ->where('users_id', auth('api')->user()->id)
        ->first();

        $diasX4 = $configUsersColaboradores->consultas_ponto_dias * 4;
        $qtdDias = $diasX4 ?? 30;

        $registros = RegistroMarcacaoPontos::select(
            'id',
            'registro_pontos_id',
            'dia_semana_string',
            'permissao',
            'hora_ponto',
            'created_at'
        )
        ->where('users_id', auth('api')->user()->id)
        ->whereMonth('created_at', date('m'))
        ->whereYear('created_at', date('Y'))
        ->where('permissao', '!=', 4)
        ->orderBy('created_at', 'asc')
        ->limit($qtdDias)
        ->get();
        
        $array = [];
        $collection = collect($registros)->groupBy('dia_semana_string');
        foreach ($collection as $key => $value) {
             $array[$key] = $value;
        }
        
        $reverso = null;
        
        krsort($array);
        foreach ($array as $chave => $valor) {
            $reverso[$chave] = $valor;
        }

        $retorno = [
            'reverso' => $reverso,
            'consultas_ponto_select_visualizar_editar' => $configUsersColaboradores->consultas_ponto_select_visualizar_editar
        ];

        return $retorno;
    }

    public function show($id){
        $registro = RegistroMarcacaoPontos::where('id', $id)
        
            ->first();
        return $registro;
    }

    public function update(Request $request, $id) {
        $registroponto = RegistroMarcacaoPontos::findOrFail($id);
        $timestamp = strtotime($registroponto->created_at);

        $registroponto->update([
            'created_at' => strtotime(date('Y-m-d', $timestamp) . ' ' .  str_pad($request->hora_ponto, 5, '0',  STR_PAD_LEFT)),
            'justificativa_colaborador' => $request->justificativa_colaborador,
            'observacao_colaborador' => $request->observacao_colaborador,
            'hora_ponto' => str_pad($request->hora_ponto, 5, '0',  STR_PAD_LEFT),
            'permissao' => 0
        ]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => RegistroMarcacaoPontos::class,
            'tabela_id' => $id,
            'atividade' => 'Solicitou ajuste de ponto'
        ]);

        return response()->json($registroponto, 200);
    }
}