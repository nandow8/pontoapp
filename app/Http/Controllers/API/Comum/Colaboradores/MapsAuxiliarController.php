<?php

namespace App\Http\Controllers\API\Comum\Colaboradores;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Admin\Log;  
use App\Models\Comum\MapsAuxiliar;  
use App\Utils;

class MapsAuxiliarController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function show($id)
    {
        $map = MapsAuxiliar::select('lat', 'lng', 'radius')
            ->where('aux_tempo', $id)->get();
        
        $this->destroy($id);
        
        return $map;
    } 

    public function store(Request $request)
    {
        $maps = MapsAuxiliar::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'radius' => $request->radius,
            'aux_tempo' => $request->aux_tempo
        ]);
        return $maps;
    }

    public function destroy($id) {
        $map = MapsAuxiliar::where('empresas_id', auth('api')->user()->empresas_id)
         ->where('aux_tempo', $id)->delete();
          
    }

}
