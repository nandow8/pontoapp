<?php

namespace App\Http\Controllers\API\Comum\Colaboradores;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Admin\Log;  
use App\Models\Comum\UsersCercasVirtuais;  
use App\Utils;

class UsersCercasVirtuaisController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function show($id)
    {
        $map = UsersCercasVirtuais::where('users_id', $id)
            ->where('status', 1)
            ->get();
        
        return $map;
    } 

    public function store(Request $request)
    {
        $maps = UsersCercasVirtuais::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'radius' => $request->radius,
            'aux_tempo' => $request->aux_tempo
        ]);
        return $maps;
    }

    public function destroy($id) {
        $map = UsersCercasVirtuais::where('empresas_id', auth('api')->user()->empresas_id)
         ->where('users_id', $id)->update(['status' => 0]);
          
    }

    public function removecercaById($id)
    {
        $cerca = UsersCercasVirtuais::where('id', $id)->where('empresas_id', auth('api')->user()->empresas_id)
            ->update(['status' => 0]);
    }

}
