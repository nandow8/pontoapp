<?php

namespace App\Http\Controllers\API\Comum\RegistroPonto;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Comum\RegistroPonto;
use Illuminate\Http\Request;
use App\Models\Admin\Log;
use App\Models\Comum\Colaborador;
use App\Models\Comum\RegistroMarcacaoPontos;

class RegistroPontoController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $registroponto = RegistroPonto::where(function($query) use ($search){
                $query->where('status', 1)
                    ->orWhere('email','LIKE', '%' . $search. '%');
            })->paginate(20);
        }else{
            $registroponto = RegistroPonto::where('status', 1)->latest()->paginate(20);
        }

        return $registroponto;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $registro = array();
        $userAuth = auth('api')->user();

        $registroponto = RegistroPonto::where('empresas_id',  $userAuth->empresas_id)
            ->where('users_id',  $userAuth->id)
            ->where('status',  1)
            ->whereYear('created_at', date('Y'))
            ->whereMonth('created_at', date('m'))
            ->whereDay('created_at', date('d'))
            ->first();

        $pontos = RegistroMarcacaoPontos::where('empresas_id', $userAuth->empresas_id)
            ->where('users_id', $userAuth->id)
            ->where('status', 1)
            //->where('permissao', 1)
            ->whereYear('created_at', date('Y'))
            ->whereMonth('created_at', date('m'))
            ->whereDay('created_at', date('d'))
            ->orderBy('created_at')
            ->get();

        $colaborador = Colaborador::where('empresas_id', $userAuth->empresas_id)
           ->where('users_id', $userAuth->id)
            ->where('status', 1)
            ->first();

        
        foreach ($colaborador->users_escalas as $key => $value) {
            foreach ($value->escalas->intervalos as $key => $intervalos) {
                $registro['intervalos'][$key] = $intervalos;
            }
        }

        $colaborador->push($colaborador->users_escalas);
        $colaborador->push($colaborador->users_calendarios);
        $colaborador->push($colaborador->users_ferias);
        $colaborador->push($colaborador->users_cercasvirtuais);

        
        $registro['registroponto'] = $registroponto;
        $registro['pontos'] = $pontos;
        $registro['colaborador'] = $colaborador;

        return $registro;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $registroponto = RegistroPonto::create($data);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => RegistroPonto::class,
            'tabela_id' => $registroponto->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($registroponto, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $registroponto = RegistroPonto::findOrFail($id);

        return $registroponto;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $registroponto = RegistroPonto::findOrFail($id);
        $registroponto->update($request->all());

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => RegistroPonto::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($registroponto, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RegistroPonto::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => RegistroPonto::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
