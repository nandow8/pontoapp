<?php

namespace App\Http\Controllers\API\Comum\RegistroPonto;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Comum\RegistroPonto;
use Illuminate\Http\Request;
use App\Models\Admin\Log;
use App\Models\Comum\Colaborador;
use App\Models\Comum\Intervalo;
use App\Models\Comum\RegistroMarcacaoPontos;
use App\Utils;
use App\Models\Comum\UsersEscala;
use App\Models\Comum\Jornada;
use App\Models\Comum\RegistroMarcacaoPontosAuditoria;
use App\Models\Comum\UsersCercasVirtuais;
use App\Models\Comum\UsersColaboradores;
use App\Models\Comum\UsersFeria;
use DateTime;

class RegistroMarcacaoPontosController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:api');
        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $registroponto = RegistroPonto::where(function($query) use ($search){
                $query->where('status', 1)
                    ->orWhere('email','LIKE', '%' . $search. '%');
            })->paginate(20);
        }else{
            $registroponto = RegistroPonto::where('status', 1)->latest()->paginate(20);
        }

        return $registroponto;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $registro = array();

        $registroponto = RegistroPonto::where('users_id',  auth('api')->user()->id)
            ->where('status',  1)
            ->whereYear('created_at', date('Y'))
            ->whereMonth('created_at', date('m'))
            ->whereDay('created_at', date('d'))
            ->first();

        $colaborador = Colaborador::where('users_id', auth('api')->user()->id)
            ->where('status', 1)
            ->first();

        $pontos = RegistroMarcacaoPontos::where('users_id', auth('api')->user()->id)
            ->where('status', 1)
            ->whereYear('created_at', date('Y'))
            ->whereMonth('created_at', date('m'))
            ->whereDay('created_at', date('d'))
            ->get();
        
        $registro['registroponto'] = $registroponto;
        $registro['colaborador'] = $colaborador;
        $registro['pontos'] = $pontos;

        return $registro;
    }

    public function verificaDistanciaBaterPonto($data)
    {
        $coordenadas = UsersCercasVirtuais::select(
            'cercas_virtuais.descricao',
            'cercas_virtuais.lat',
            'cercas_virtuais.lng'
        )
        ->join('cercas_virtuais', 'cercas_virtuais.idunico', 'users_cercasvirtuais.idunico')
        ->where('users_cercasvirtuais.users_id', $data['users_id'])
        ->where('users_cercasvirtuais.empresas_id', $data['empresas_id'])
        ->where('users_cercasvirtuais.status', 1)
        ->get();
   
        // lat: "-23.63108158656"
        // lng: "-46.323683738358"
        // lat: "-23.631065892798"
        // lng: "-46.323626010646"

        if (count($coordenadas) < 1) // se nao encontra coordenada, é porque nao foi adicionado cerca ao colaborador - entao pode :)
            return true;

        foreach ($coordenadas as $key => $value) {
            $distancia = $this->calculaDistancia($value->lat, $value->lng, $data['latitude'], $data['longitude']);

            if ($distancia <= 15) {
                return true;
            }
        }

        return false;
    }

    function calculaDistancia($lat_inicial, $long_inicial, $lat_final, $long_final){
        $d2r = 0.017453292519943295769236;

        $dlong = ($long_final - $long_inicial) * $d2r;
        $dlat = ($lat_final - $lat_inicial) * $d2r;

        $temp_sin = sin($dlat/2.0);
        $temp_cos = cos($lat_inicial * $d2r);
        $temp_sin2 = sin($dlong/2.0);

        $a = ($temp_sin * $temp_sin) + ($temp_cos * $temp_cos) * ($temp_sin2 * $temp_sin2);
        $c = 2.0 * atan2(sqrt($a), sqrt(1.0 - $a));

        return 6368.1 * $c;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;
        $data['users_id'] = auth('api')->user()->id;

        if (false == $this->verificaSePodeBaterPonto($data['users_id'], $data['empresas_id'])) {
            return false;
        }
        if (false == $this->verificaSePodeBaterPontoFerias($data['users_id'], $data['empresas_id'])) {
            return false;
        }

        if (false == $this->verificaDistanciaBaterPonto($data)) {
            return false;
        }
    
        if (false == $this->verificaMaisDeQuatroBatidas($data['users_id'], $data['empresas_id'])) {
            return false;
        }
        
        $data['registro_pontos_id'] = $this->verificaPontoHoje($data['users_id'], $data['empresas_id'])['id'];
        $currentPhoto = null;
        if($request->foto) {
            $currentPhoto = time().'.' . explode('/', explode(':', substr($request->foto, 0, strpos($request->foto, ';')))[1])[1];
            \Image::make($request->foto)->save(public_path('img/colaboradores/pontos/').$currentPhoto);
        }
        
        $registroponto = RegistroMarcacaoPontos::create([
            'empresas_id' => $data['empresas_id'],
            'users_id' => $data['users_id'],
            'registro_pontos_id' => $data['registro_pontos_id'],
            'foto' => '/img/colaboradores/pontos/' . $currentPhoto,
            'observacao_colaborador' => $data['observacao_colaborador'],
            'origem_ponto' => 'WEB',
            'permissao' => 1,
            'hora_ponto' => str_pad(date('G:i'), 5, '0',  STR_PAD_LEFT),
            'dia_semana_string' => Utils::diaSemanaString(),
            'numero_recibo' => $data['empresas_id'] . $data['users_id'] . date('d') . date('Y') . date('m') . date('s') . date('h') . date('i')
        ]);

        $registroponto_auditoria = RegistroMarcacaoPontosAuditoria::create([
            'empresas_id' => $data['empresas_id'],
            'users_id' => $data['users_id'],
            'registro_pontos_id' => $data['registro_pontos_id'],
            'foto' => '/img/colaboradores/pontos/' . $currentPhoto,
            'observacao_colaborador' => $data['observacao_colaborador'],
            'origem_ponto' => 'WEB',
            'permissao' => 1,
            'hora_ponto' => str_pad(date('G:i'), 5, '0',  STR_PAD_LEFT),
            'dia_semana_string' => Utils::diaSemanaString(),
            'numero_recibo' => $data['empresas_id'] . $data['users_id'] . date('d') . date('Y') . date('m') . date('s') . date('h') . date('i')
        ]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => RegistroMarcacaoPontos::class,
            'tabela_id' => $registroponto->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($registroponto, 201);
    }

    public function verificaMaisDeQuatroBatidas($users_id, $empresas_id) {
        $registro = RegistroMarcacaoPontos::where('empresas_id',  $empresas_id)
        ->where('users_id',  $users_id)
        ->where('status',  1)
        ->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', date('m'))
        ->whereDay('created_at', date('d'))
        ->count();

        if ($registro < 4)
            return true;
            
        return false;
    }

    public function verificaSePodeBaterPonto($users_id, $empresas_id) {
        $diaAtual = date('N');

        $escala = UsersEscala::select('escalas.dia_jornada')
            ->join('escalas', 'escalas.id', 'users_escalas.escalas_id')
            ->where('users_escalas.users_id', $users_id)
            ->where('users_escalas.empresas_id', $empresas_id)
            ->where('users_escalas.status', 1)
            ->first();

        if ($escala->dia_jornada < 8 AND $escala->dia_jornada == $diaAtual) //unico dia da semana
            return true;
        
        if ($escala->dia_jornada == 8 AND $diaAtual > 5) // somente sabado E domingo
            return true;
        
        if ($escala->dia_jornada == 9) {
            return true;
            $data1 = new DateTime();
            $data2 = $this->buscaDia($users_id, $empresas_id);
            
            $dataDIA = new DateTime($data2);
            if ($dataDIA->format('d') == date('d'))
                return true;

            if (
                $data2->diff($data1)->format('%d') == 1
                AND $data2->diff($data1)->format('%h') == 11
	            AND $data2->diff($data1)->format('%i') >= 50)
                    return true;

            return false;
                //Faltam %Y Anos %m Mês, %d dias e %h horas %i minutos
        }

        if ($escala->dia_jornada == 10) {
            $data1 = new DateTime();
            $data2 = $this->buscaDia($users_id, $empresas_id);
            
            $dataDIA = new DateTime($data2);
            if ($dataDIA->format('d') == date('d'))
                return true;

            if (
                $data2->diff($data1)->format('%d') == 2
                AND $data2->diff($data1)->format('%h') == 23
                AND $data2->diff($data1)->format('%i') >= 50)
                    return true;
            
            return false;
        }

        if ($escala->dia_jornada == 11) // livre
            return true;

        if ($escala->dia_jornada == 12 AND $diaAtual < 6) // segunda a sexta
            return true;    

            
        return false;
    }

    public function verificaSePodeBaterPontoFerias() {
        $usersFerias = UsersFeria::select(
            'impedir_registro_ferias',
            'ferias_inicio',
            'ferias_fim'
        )->where('users_id', auth('api')->user()->id)
        ->where('status', 1)
        ->first();

        if (empty($usersFerias)) {
            return true;
        }

        $ferias = null;
        if (date('Y-m-d') >= $usersFerias->ferias_inicio AND date('Y-m-d') <= $usersFerias->ferias_fim) {
            $ferias = 1;
        } else {
            $ferias = 0;
        }

        if ($ferias == 1 AND $usersFerias->impedir_registro_ferias == 'Não') {
            return true;
        }

        if ($ferias == 0){
            return true;
        }

        return false;
    }
    public function verificaSePodeBaterPontoRegistroWeb() {
        return UsersColaboradores::select(
            'permite_ponto_web',
            'permite_marcacao_ponto_web' // foto
            // 'dispositivos_autorizados_web'

        )
        ->where('users_id', auth('api')->user()->id)
        ->first();
    }

    public function buscaDia($users_id, $empresas_id) {
        return RegistroMarcacaoPontos::select('created_at')
                ->where('users_id', $users_id)
                ->where('empresas_id', $empresas_id)
                ->orderBy('updated_at', 'DESC')
                ->first();
    }

    public function pontoManual(Request $request)
    {
        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;
        $data['users_id'] = auth('api')->user()->id;
        $data['registro_pontos_id'] = $this->verificaPontoHoje($data['users_id'], $data['empresas_id'])['id'];

        // if($request->foto) {
        //     $currentPhoto = time().'.' . explode('/', explode(':', substr($request->foto, 0, strpos($request->foto, ';')))[1])[1];
        //     \Image::make($request->foto)->save(public_path('img/colaboradores/ponto').$currentPhoto);
        //     $request->merge(['foto' => $currentPhoto]);
        //     // $currentPhoto = public_path('img/colaboradores/ponto').$currentPhoto;
        // }
        
        $registroponto = RegistroMarcacaoPontos::insertGetId([
            'empresas_id' => $data['empresas_id'],
            'users_id' => $data['users_id'],
            'registro_pontos_id' => $data['registro_pontos_id'],
            'observacao_colaborador' => $data['observacao_colaborador'],
            'justificativa_colaborador' => $data['justificativa_colaborador'],
            'origem_ponto' => 'WEB',
            'aprovado_reprovado' => 0,
            'permissao' => 0,
            'hora_ponto' => str_pad($data['hora_ponto'], 5, '0',  STR_PAD_LEFT),
            'dia_semana_string' => Utils::diaSemanaString(),
            'created_at' => date('Y-m-d') . ' ' . str_pad($data['hora_ponto'], 5, '0',  STR_PAD_LEFT),
            'numero_recibo' => $data['empresas_id'] . $data['users_id'] . date('d') . date('Y') . date('m') . date('s') . date('h') . date('i')
        ]);

        RegistroMarcacaoPontosAuditoria::create([
            'empresas_id' => $data['empresas_id'],
            'users_id' => $data['users_id'],
            'registro_pontos_id' => $data['registro_pontos_id'],
            'observacao_colaborador' => $data['observacao_colaborador'],
            'origem_ponto' => 'WEB',
            'permissao' => 1,
            'hora_ponto' => str_pad(date('G:i'), 5, '0',  STR_PAD_LEFT),
            'dia_semana_string' => Utils::diaSemanaString(),
            'numero_recibo' => $data['empresas_id'] . $data['users_id'] . date('d') . date('Y') . date('m') . date('s') . date('h') . date('i')
        ]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => RegistroMarcacaoPontos::class,
            'tabela_id' => $registroponto,
            'atividade' => 'cadastrou ponto manual'
        ]);

        return response()->json(RegistroMarcacaoPontos::where('id', $registroponto)->first(), 201);
    }

    public function verificaPontoHoje($users_id, $empresas_id) {
        $verificaPontoHoje = RegistroPonto::where('empresas_id',  $empresas_id)
            ->where('users_id',  $users_id)
            ->where('status',  1)
            ->whereYear('created_at', date('Y'))
            ->whereMonth('created_at', date('m'))
            ->whereDay('created_at', date('d'))
            ->count();

        if ($verificaPontoHoje > 0) {
            return RegistroPonto::select('id')
                ->where('empresas_id',  $empresas_id)
                ->where('users_id',  $users_id)
                ->where('status',  1)
                ->whereYear('created_at', date('Y'))
                ->whereMonth('created_at', date('m'))
                ->whereDay('created_at', date('d'))
                ->first();
        } else {
            return RegistroPonto::create([
                'empresas_id' => $empresas_id,
                'users_id' => $users_id,
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $registroponto = RegistroPonto::findOrFail($id);

        return $registroponto;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $registroponto = RegistroMarcacaoPontos::findOrFail($id);
        $registroponto->update([
            'created_at' => $request->created_at,
            'hora_ponto' => str_pad(date('G:i', strtotime($request->created_at)), 5, '0',  STR_PAD_LEFT),
            'justificativa_colaborador' => $request->justificativa_colaborador,
            'observacao_colaborador' => $request->observacao_colaborador,
            'permissao' => 0
        ]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => RegistroMarcacaoPontos::class,
            'tabela_id' => $id,
            'atividade' => 'Solicitou ajuste de ponto'
        ]);

        return response()->json($registroponto, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RegistroPonto::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => RegistroPonto::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }

    public function totalHorasPrevistasMesAtual() {
        $registro = array();
        $totalDiasMesAtual = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        $dataAtual = date('Y-m-d');
        $quantidadeHorasTrabalhadas = 0;

        $colaborador = Colaborador::selectRaw(
                'users_escalas.*,
                "ovo",
                escalas.*,
                "AVAAA",
                users_colaboradores.*'
            )
            ->join('users_escalas', 'users_escalas.users_id', 'users_colaboradores.users_id')
            ->join('escalas', 'escalas.id', 'users_escalas.escalas_id')
            ->where('users_colaboradores.empresas_id', auth('api')->user()->empresas_id)
            ->where('users_colaboradores.users_id', auth('api')->user()->id)
            ->where('users_colaboradores.status', 1)
            ->where('users_escalas.status', 1)
            ->get(); 

        foreach ($colaborador as $key => $value) {
            if ( $value->dia_jornada) {
                if ( $value->dia_jornada = 12) { // 12 segunda a sexta
                    $dias = $totalDiasMesAtual / 7;
                    $trabalhadas = $dias * 5; 

                    $date1 = $dataAtual . ' ' .  $value['inicio_expediente'];
                    $date2 = $dataAtual . ' ' .  $value['fim_expediente'];
                    $horasDiarias = abs(strtotime($date2) - strtotime($date1))/(60*60);

                    $intervalos = Intervalo::where('escalas_id',  $value->escalas_id)->get();
                    $qtdHorasIntervalos = 0;
                    foreach ($intervalos as $key => $value) {
                        $qtdHorasIntervalos += abs(strtotime($dataAtual . ' ' . $value['inicio_intervalo']) - strtotime($dataAtual . ' ' . $value['fim_intervalo']))/(60*60);
                    }
                    
                    $quantidadeHorasTrabalhadas += ($horasDiarias * $trabalhadas) - ($qtdHorasIntervalos * $trabalhadas);
                }

                if ( $value->dia_jornada <= 7) { // um unico dia da semana
                    return Utils::contadiasdasemana($value->dia_jornada, date('m'), date('Y'));
                }

                else {
                    $quantidadeHorasTrabalhadas += 0;
                }
            }

            else {
                $quantidadeHorasTrabalhadas += 0;
            }
        }

        return $quantidadeHorasTrabalhadas;
    }

    public function totalHorasMesAtual() {
        $registros = RegistroMarcacaoPontos::selectRaw(
            'day(created_at ) as day,
            created_at'
        )
    // 
        ->where('users_id', auth('api')->user()->id) // 
        ->whereMonth('created_at', date('m'))
        ->whereNotIn('permissao', [0, 4])
        ->groupBy('created_at')
        ->get();
        
        $grouped = collect($registros)->groupBy('day');

        return $grouped;

        /*
        $chunkRegistros = [];
        foreach($registros as $key => $value){
            $chunkRegistros[$value['day']][$key] = $value;
        }
        return $chunkRegistros;


        #time_format( SEC_TO_TIME( SUM( TIME_TO_SEC( created_at ) ) ),'%H:%i:%s') AS total_horas
            

            select
                day(created_at ),
                created_at 
            from 
                registro_marcacao_pontos 
            where
            	empresas_id = 8
            	and users_id = 60
	            and month(created_at ) = 4
	            and permissao not in (0, 4)
            group by 
                created_at 

                */
    }

    /* apenas exemplo de calculo de horas
        <?php
            setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
            date_default_timezone_set('America/Sao_Paulo');

            $data1 = new DateTime('2020-05-26 13:00:00');
            $d = '2020-05-28 00:46:00';
            $data2 = new DateTime($d);

            if ($data2->format('d') == date('d')){
                echo 'oi';
            }

            if ($data2->diff($data1)->format('%d') == 1
                    AND $data2->diff($data1)->format('%h') == 11
                    AND $data2->diff($data1)->format('%i') >= 45) {
                                echo 'opa';
                            }
                            
                            
            echo $data2->diff($data1)->format('dias: %d horas: %h minutos: %i');
    */
}
