<?php

namespace App\Http\Controllers\API\Comum\Intervalos;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Comum\Intervalo;
use Illuminate\Http\Request;
use App\Models\Admin\Log;

class IntervalosController extends Controller
{

    // { path: '/intervalos', component: require('./pages/comum/intervalos/index.vue').default },
    // { path: '/intervalos/create', component: require('./pages/comum/intervalos/create.vue').default },
    // { path: '/intervalos/:id/edit', component: require('./pages/comum/intervalos/edit.vue').default },
    // { path: '/intervalos/:id', component: require('./pages/comum/intervalos/show.vue').default },

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $intervalo = Intervalo::where(function($query) use ($search){
                $query->where('status', 1)
                    ->orWhere('email','LIKE', '%' . $search. '%');
            })->paginate(20);
        }else{
            $intervalo = Intervalo::where('status', 1)->latest()->paginate(20);
        }

        return $intervalo;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $intervalos = Intervalo::where('status', 1)->latest()->paginate(25);

        return $intervalos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $intervalo = Intervalo::create($data);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Intervalo::class,
            'tabela_id' => $intervalo->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($intervalo, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $intervalo = Intervalo::findOrFail($id);

        return $intervalo;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $intervalo = Intervalo::findOrFail($id);
        $intervalo->update($request->all());

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Intervalo::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($intervalo, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Intervalo::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Intervalo::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
