<?php

namespace App\Http\Controllers\API\Comum\Chatmessages;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Comum\Chatmessage;
use Illuminate\Http\Request;
use App\Models\Admin\Log;

class ChatmessagesController extends Controller
{
    //{ path: '/chatmessages', component: require('./pages/comum/chatmessages/index.vue').default },
    //{ path: '/chatmessages/create', component: require('./pages/comum/chatmessages/create.vue').default },
    //{ path: '/chatmessages/:id/edit', component: require('./pages/comum/chatmessages/edit.vue').default },
    //{ path: '/chatmessages/:id', component: require('./pages/comum/chatmessages/show.vue').default },

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $chatmessage = Chatmessage::where(function($query) use ($search){
                $query->where('status', 1)
                    ->orWhere('email','LIKE', '%' . $search. '%');
            })->paginate(20);
        }else{
            $chatmessage = Chatmessage::where('status', 1)->latest()->paginate(20);
        }

        return $chatmessage;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $chatmessages = Chatmessage::where('status', 1)->latest()->paginate(25);

        return $chatmessages;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $chatmessage = Chatmessage::create($data);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Chatmessage::class,
            'tabela_id' => $chatmessage->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($chatmessage, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chatmessage = Chatmessage::findOrFail($id);

        return $chatmessage;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $chatmessage = Chatmessage::findOrFail($id);
        $chatmessage->update($request->all());

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Chatmessage::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($chatmessage, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Chatmessage::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Chatmessage::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
