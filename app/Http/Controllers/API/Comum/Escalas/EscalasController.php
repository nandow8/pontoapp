<?php

namespace App\Http\Controllers\API\Comum\Escalas;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Comum\Escala;
use Illuminate\Http\Request;
use App\Models\Admin\Log;
use App\Models\Comum\Intervalo;

class EscalasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $escala = Escala::select(
                'escalas.id',
                'escalas.nome_escala',
                'jornadas.id as dia_jornada',
                'jornadas.dias_jornada',
                'empresas.name as empresas_name'
            )->leftJoin('jornadas', 'jornadas.id', 'escalas.dia_jornada')
            ->join('empresas', 'empresas.id', 'escalas.empresas_id')
            ->where(function($query) use ($search){  
                $query->where('escalas.nome_escala', 'LIKE', "%$search%")
                      ->orWhere('empresas.name', 'LIKE', "%$search%");
            })->where('escalas.status', 1)->paginate(20);
        }else{
            $escala = $this->index();
        }

        return $escala;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $escalas = Escala::select(
                'escalas.id',
                'escalas.nome_escala',
                'jornadas.id as dia_jornada',
                'jornadas.dias_jornada',
                'empresas.name as empresas_name'
            )
            ->leftJoin('jornadas', 'jornadas.id', 'escalas.dia_jornada')
            ->join('empresas', 'empresas.id', 'escalas.empresas_id')
            ->where('escalas.status', 1)
            ->orderBy('escalas.id', 'desc')
            ->paginate(25);

        return $escalas;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $escala = Escala::create([
            'empresas_id'=> $data['empresas_id'] ?? null,
            'nome_escala'=> $data['nome_escala'] ?? null,
            'tipo'=> $data['tipo'] ?? null,
            'dia_jornada'=> $data['dia_jornada'] ?? null,
            'tolerancia'=> $data['tolerancia'] ?? null,
            'tolerancia_tempo'=> $data['tolerancia_tempo'] ?? null,
            'inicio_expediente'=> $data['inicio_expediente'] ?? null,
            'fim_expediente'=> $data['fim_expediente'] ?? null,
            'jornada_hora_dia'=> $data['jornada_hora_dia'] ?? null,
            'dias_trabalhar'=> $data['dias_trabalhar'] ?? null,
            'dias_dsr_folga'=> $data['dias_dsr_folga'] ?? null,
            'expediente_livre'=> $data['expediente_livre'] ?? null,
            'inicioexpediente_livre'=> $data['inicioexpediente_livre'] ?? null,
            'tolerancia_por_marcacao'=> $data['tolerancia_por_marcacao'] ?? null,
        ]);

        foreach ($request->intervalos as $key => $value) {
            Intervalo::create([
                'escalas_id' => $escala->id,
                'empresas_id' => $data['empresas_id'] ?? null,
                'inicio_intervalo' => $value['inicio_intervalo'] ?? null,
                'fim_intervalo' => $value['fim_intervalo'] ?? null,
                'abonar' => $value['abonar'] ?? null,
                'pre_assinalar' => $value['pre_assinalar'] ?? null,
                'tolerancia_atraso' => $value['tolerancia_atraso'] ?? null,
            ]);
        }

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Escala::class,
            'tabela_id' => $escala->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($escala, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $escala = Escala::findOrFail($id);
        $escala->push($escala->jornada);
        $escala->push($escala->intervalos);

        return $escala;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $escala = Escala::findOrFail($id);
        
            $escala->empresas_id =  auth('api')->user()->empresas_id;
            $escala->nome_escala = $data['nome_escala'] ?? null;
            $escala->tipo = $data['tipo'] ?? null;
            if($data['dia_jornada'])
                $escala->dia_jornada = preg_replace("/[^0-9]/","", $data['dia_jornada']);
            $escala->tolerancia = $data['tolerancia'] ?? null;
            $escala->tolerancia_tempo = $data['tolerancia_tempo'] ?? null;
            $escala->inicio_expediente = $data['inicio_expediente'] ?? null;
            $escala->fim_expediente = $data['fim_expediente'] ?? null;
            $escala->jornada_hora_dia = $data['jornada_hora_dia'] ?? null;
            $escala->dias_trabalhar = $data['dias_trabalhar'] ?? null;
            $escala->dias_dsr_folga = $data['dias_dsr_folga'] ?? null;
            $escala->expediente_livre = $data['expediente_livre'] ?? null;
            $escala->inicioexpediente_livre = $data['inicioexpediente_livre'] ?? null;
            $escala->tolerancia_por_marcacao = $data['tolerancia_por_marcacao'] ?? null;
        $escala->save();

        
        foreach ($data['intervalos'] as $key => $value) {
            Intervalo::updateOrCreate(
                [
                    'inicio_intervalo' => $value['inicio_intervalo'],
                    'fim_intervalo' => $value['fim_intervalo'],
                ],
                [
                    'escalas_id' => $escala['id'],
                    'empresas_id' => $data['empresas_id'],
                    'inicio_intervalo' => $value['inicio_intervalo'],
                    'fim_intervalo' => $value['fim_intervalo'],
                    'abonar' => $value['abonar'],
                    'pre_assinalar' => $value['pre_assinalar'],
                    'tolerancia_atraso' => $value['tolerancia_atraso'] ?? null,
                    'status' => 1
                ]
            );
        }

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Escala::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($escala, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Escala::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Escala::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
