<?php

namespace App\Http\Controllers\API\Comum\Jornadas;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Comum\Jornada;
use Illuminate\Http\Request;
use App\Models\Admin\Log;

class JornadasController extends Controller
{

    // { path: '/jornadas', component: require('./pages/comum/jornadas/index.vue').default },
    // { path: '/jornadas/create', component: require('./pages/comum/jornadas/create.vue').default },
    // { path: '/jornadas/:id/edit', component: require('./pages/comum/jornadas/edit.vue').default },
    // { path: '/jornadas/:id', component: require('./pages/comum/jornadas/show.vue').default },

    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $jornada = Jornada::where(function($query) use ($search){
                $query->where('status', 1)
                    ->orWhere('email','LIKE', '%' . $search. '%');
            })->paginate(20);
        }else{
            $jornada = Jornada::where('status', 1)->latest()->paginate(20);
        }

        return $jornada;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jornadas = Jornada::where('status', 1)->latest()->paginate(25);

        return $jornadas;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $jornada = Jornada::create($data);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Jornada::class,
            'tabela_id' => $jornada->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($jornada, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jornada = Jornada::where('tipo_jornada', $id)->get();

        return $jornada;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $jornada = Jornada::findOrFail($id);
        $jornada->update($request->all());

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Jornada::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($jornada, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Jornada::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Jornada::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
