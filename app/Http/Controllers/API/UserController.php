<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Empresa;
use App\Models\Comum\UsersEndereco;
use App\Models\Comum\UsersManyEmpresasSetores;
use App\User;
use Illuminate\Support\Facades\Validator;
use Hash;
use App\Models\Admin\Log;

class UserController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function profile()
    {
        return auth('api')->user();
    }

    public function updateProfile(Request $request, $id)
    {
        $user = auth('api')->user();
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'password' => 'sometimes|required|min:6'
        ]);
        $currentPhoto = $user->photo;
        if($request->photo != $currentPhoto){
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('img/profile/').$name);
            $request->merge(['photo' => $name]);
            $userPhoto = public_path('img/profile/').$currentPhoto;
            // if(file_exists($userPhoto)){
            //     @unlink($userPhoto);
            // }
        }
        if(!empty($request->password)){
            $request->merge(['password' => Hash::make($request['password'])]);
        }
        $user->update($request->all());
        return ['message' => "Success"];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = auth('api')->user();
        // $this->authorize('isAdmin');
        $users = User::selectRaw(
            'users.id,
            users.name,
            users.email,
            users.type,
            users.created_at,
            perfis.nome as perfis_nome,
            empresas.name as empresas_name'
        )
        ->join('perfis', 'perfis.id', 'users.idtipo_usuario')
        ->join('empresas', 'empresas.id', 'users.empresas_id');

        if ($authUser->idtipo_usuario != 1000)
            $users = $users->where('users.empresas_id', $authUser->empresas_id);
        
        $users =  $users->where('users.status', 1)
        ->where('users.type', '!=', 'colaborador')
        ->orderBy('users.id', 'desc')
        ->withoutGlobalScope('App\Scopes\VisibleScope')
        ->paginate(20);

        return [
            'users' => $users,
            'codigoEmpresa' => Empresa::where('id', $authUser->empresas_id)->withoutGlobalScope('App\Scopes\VisibleScope')->first()['codigo']
        ];
    }

    public function search(){
        $authUser = auth('api')->user();

        if ($search = \Request::get('q')) {
            $users = User::selectRaw(
                'users.id,
                users.name,
                users.email,
                users.type,
                users.created_at,
                perfis.nome as perfis_nome,
                empresas.name as empresas_name'
            )->where(function($query) use ($search){
                $query->where('users.name','LIKE','%'. $search . '%')
                    ->orWhere('empresas.name','LIKE','%'. $search . '%')
                    ->orWhere('type','LIKE','%'. $search . '%')
                    ->orWhere('email','LIKE', '%' . $search. '%');
            })->join('perfis', 'perfis.id', 'users.idtipo_usuario')
            ->join('empresas', 'empresas.id', 'users.empresas_id');

            if ($authUser->idtipo_usuario != 1000)
                $users = $users->where('users.empresas_id', $authUser->empresas_id);
            
            $users = $users->where('users.status', 1)
            ->where('users.type', '!=', 'colaborador')
            ->withoutGlobalScope('App\Scopes\VisibleScope')
            ->paginate(20);
        }else{
            $users = User::selectRaw(
                'users.id,
                users.name,
                users.email,
                users.type,
                users.created_at,
                perfis.nome as perfis_nome,
                empresas.name as empresas_name'
            )->join('perfis', 'perfis.id', 'users.idtipo_usuario')
            ->join('empresas', 'empresas.id', 'users.empresas_id');

            if ($authUser->idtipo_usuario != 1000)
                $users = $users->where('users.empresas_id', $authUser->empresas_id);
            
            $users = $users->where('users.status', 1)
            ->where('users.type', '!=', 'colaborador')
            ->withoutGlobalScope('App\Scopes\VisibleScope')
            ->paginate(20);
        }

        return [
            'users' => $users,
            'codigoEmpresa' => Empresa::where('id', $authUser->empresas_id)->withoutGlobalScope('App\Scopes\VisibleScope')->first()['codigo']
        ];

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'name' => 'required|string|max:191',
        //     'email' => 'required|string|max:191|unique:users',
        //     'type' => 'required|string|max:191',
        //     'password' => 'required|string|min:6',
        //     'cpf' => 'required|string|min:6',
        //     'rg' => 'required|string|min:6'
        // ]);

        $userAuth = auth('api')->user();

        $this->validate($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|string|max:191|unique:users',
            'idtipo_usuario' => 'required',
            'password' => 'required|string|min:6',
            'cpf' => 'required|string|min:6|unique:users',
            'rg' => 'required|string|min:6|unique:users',
            'filiais_rows.*.id' => 'required', 
        ]);


        $user = User::create([
            'empresas_id' => $userAuth->empresas_id,
            'name' => $request['name'],
            'email' => $request['email'],
            'type' => 'usuario',
            'cpf' => $request['cpf'],
            'rg' => $request['rg'],
            'idtipo_usuario' => $request->idtipo_usuario,
            'password' => Hash::make($request['password']),
        ]);

        foreach ($request->filiais_rows as $key => $value) {
            UsersManyEmpresasSetores::create([
                'setores_id' => 99999,
                'users_id' => $user->id,
                'empresas_id' => $userAuth->empresas_id,
                'filiais_id' => $value['id'],
            ]);
        }

        Log::create([
            'empresas_id' => $userAuth->empresas_id,
            'users_id' => $userAuth->id,
            'tabela' => User::class,
            'tabela_id' => $user->id,
            'atividade' => 'cadastrou',
            'status' => 1
        ]);

        return $request->filiais_rows;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $filiais = array();
        $endereco = [];
        
        foreach ($user->users_filiais as $key => $value) {
            $filiais[$key] = $value->filiais;
        }

        $user->push($filiais);
        // $user->push($user->endereco);
        
        $user->push($filiais);
        $user->push($endereco);

        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            // 'name' => 'required|string|max:191',
            'email' => 'required|string|max:191|unique:users,email,' . $user->id,
            'idtipo_usuario' => 'required',
            // 'password' => 'sometimes|min:6',
        ]);

        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->type = $request['type'];
        $user->cpf = $request['cpf'];
        $user->rg = $request['rg'];
        $user->idtipo_usuario = $request->idtipo_usuario;
        if($request->password) {
            $user->password = Hash::make($request['password']);
        }
            
        $user->save();

        UsersManyEmpresasSetores::where('users_id', $user->id)->update(['status' => 0]);
        foreach ($request->filiais_rows as $key => $value) {
            UsersManyEmpresasSetores::create([
                'setores_id' => 99999,
                'users_id' => $user->id,
                'empresas_id' => auth('api')->user()->empresas_id,
                'filiais_id' => $value['id'],
            ]);
        }

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => User::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return ['message' => 'update'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id)->first();
        $user->status = 0;
        $user->save();

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => User::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return ['message' => 'User Deleted'];
    }
}
