<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Hash;
use Illuminate\Support\Str;

class ApiAuthController extends Controller
{
    public function index(Request $request)
    { 
        $search = $request->all();

        $user = User::where(function($query) use ($search){
            $query->where('email', $search['email']) 
                ->orWhere('matricula', $search['email']);
        })->where('status', 1)->first();
        
        if (Hash::check($search['password'], $user->password)) {
            $apikey = base64_encode(Str::random(40));
            User::where('email', $search['email'])->update(['token' => "$apikey"]);
            // $users = User::select('id', 'name', 'email')->where('email',$search['email'])->get();
            return response()->json(
                [
                    'token' => $apikey,
                    'user' => $user
                ]
            );
        } else {
            return response()->json(['status' => 'fail'], 401);
        }
    }

    public function store(Request $request)
    {
        $data = $request->all();
        
        return User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => app('hash')->make($data['password']),
            'token'  => base64_encode(Str::random(40))
        ]);
    }
}
