<?php

namespace App\Http\Controllers\API\Admin\Perfis;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Admin\Perfi;
use Illuminate\Http\Request;
use App\Models\Admin\Log;

class PerfisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function search(){
        
        if ($search = \Request::get('q')) {
            $perfi = Perfi::where('nome','LIKE', '%' . $search. '%')
                // ->where('empresas_id', auth('api')->user()->empresas_id)
                ->toSql();
        }else{
            $perfi = Perfi::where('status', 1)->latest()->paginate(20);
        }

        return $perfi;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth('api')->user();

        if($user->type == 'master')
            return Perfi::where('status', 1)
                ->whereIn('id', [1000, 2000, 3000])->paginate(25);

         
        $perfis = Perfi::where('status', 1)
            ->whereNotIn('id', [1000, 2000, 3000])
            ->where('empresas_id', $user->empresas_id)
            ->paginate(25);
            
        return $perfis;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $perfi = Perfi::create($data);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Perfi::class,
            'tabela_id' => $perfi->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($perfi, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $perfi = Perfi::findOrFail($id);

        return $perfi;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $perfi = Perfi::findOrFail($id);
        $perfi->update($request->all());

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Perfi::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($perfi, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Perfi::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Perfi::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
