<?php

namespace App\Http\Controllers\API\Admin\Perfis;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Admin\Permissoes;
use Illuminate\Http\Request;
use App\Models\Admin\Log;

class PermissoesController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $permisso = Permissoes::where(function($query) use ($search){
                $query->where('titulo','LIKE', '%' . $search. '%');
            })->where('status', 1)->where('empresas_id', auth('api')->user()->empresas_id)->paginate(20);
        }else{
            $permisso = Permissoes::where('status', 1)->where('empresas_id', auth('api')->user()->empresas_id)->latest()->paginate(20);
        }

        return $permisso;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permissoesList = Permissoes::select(
                'id',
                'titulo',
                'descricao',
                'quem_pertence'
            )
            ->where('status','=', true)
            ->whereNotIn('quem_pertence', ['REGISTRODEPONTO', 'COLABORADORHISTORICOS'])
            ->orderBy( 'quem_pertence', 'ASC')
            ->get();
            
        $permissoesOrdenado = [];
        foreach($permissoesList as $key => $value){
            foreach($value->filhasQuemPertence as $key => $value){
                if ($value->chave_ordem != 'CLIENTES' AND $value->chave_ordem != 'MARCACOESPONTOSAUDITORIA')
                    $permissoesOrdenado[$value->quem_pertence][$value['titulo']][$key] = $value;
            }  
        } 

        return $permissoesOrdenado;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $permisso = Permissoes::create($data);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Permissoes::class,
            'tabela_id' => $permisso->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($permisso, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permisso = Permissoes::findOrFail($id);

        return $permisso;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $permisso = Permissoes::findOrFail($id);
        $permisso->update($request->all());

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Permissoes::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($permisso, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Permissoes::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Permissoes::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
