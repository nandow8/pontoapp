<?php

namespace App\Http\Controllers\API\Admin\Perfis;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Admin\PermissoesUsuarios;
use Illuminate\Http\Request;
use App\Models\Admin\Log;
use App\Models\Admin\Perfi;

class PermissoesUsuariosController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $permisso = PermissoesUsuarios::where(function($query) use ($search){
                $query->where('titulo','LIKE', '%' . $search. '%');
            })->where('status', 1)->where('empresas_id', auth('api')->user()->empresas_id)->paginate(20);
        }else{
            $permisso = PermissoesUsuarios::where('status', 1)->where('empresas_id', auth('api')->user()->empresas_id)->latest()->paginate(20);
        }

        return $permisso;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permissoes = PermissoesUsuarios::where('status', 1)->latest()->paginate(25);

        return $permissoes;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $permisso = PermissoesUsuarios::create($data);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => PermissoesUsuarios::class,
            'tabela_id' => $permisso->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($permisso, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresas_id = auth('api')->user()->empresas_id;
        // verifica perfil
        $perfil = Perfi::where('empresas_id', $empresas_id)
            ->where('id', $id)
            ->first();

        if (!$perfil)
            return false;

        $permisso = PermissoesUsuarios::select('idpermissao')
            ->where('idtipo_usuario', $id)
            ->get();

        if ($permisso->isEmpty()) {
            PermissoesUsuarios::create([
                'status' => 1,
                'empresas_id' => $empresas_id,
                'idtipo_usuario' => $id,
                'idpermissao' => 86 // chat visualizar
            ]);
        }

        return $permisso;   
    }

    public function verificaPermissoes(Request $request)
    {
        $temPermissao = PermissoesUsuarios::select('idpermissao')->where('idtipo_usuario', $request->idtipo_usuario)
            ->where('idpermissao', $request->idpermissao)
            ->where('empresas_id', auth('api')->user()->empresas_id)
            ->first();

        if($temPermissao) {
            return json_encode(true);
        }
        
        return json_encode(false);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        PermissoesUsuarios::where('idtipo_usuario', $id)->delete();

        foreach ($request->chaves_ordem as $key => $idpermissao) {
            PermissoesUsuarios::create([
                'idpermissao' => $idpermissao,
                'idtipo_usuario' => $id,
                'empresas_id' => auth('api')->user()->empresas_id
            ]);
        }

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => PermissoesUsuarios::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($id, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PermissoesUsuarios::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => PermissoesUsuarios::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
