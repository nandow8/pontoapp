<?php

namespace App\Http\Controllers\API\Admin\Cargos;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Admin\Cargo;
use Illuminate\Http\Request;
use App\Models\Admin\Log;

class CargosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $cargo = Cargo::select(
                'cargos.id',
                'cargos.name',
                'empresas.name as empresas_name'
            )->join('empresas', 'empresas.id', 'cargos.empresas_id')
            ->where(function($query) use ($search){
                $query->where('cargos.name','LIKE', "%$search%")
                      ->orWhere('empresas.name','LIKE', "%$search%");
            })->where('cargos.status', 1)
            ->paginate(20);
        }else{
            $cargo = $this->index();
        }

        return $cargo;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cargos = Cargo::select(
            'cargos.id',
            'cargos.name',
            'empresas.name as empresas_name'
        )->join('empresas', 'empresas.id', 'cargos.empresas_id')
        ->where('cargos.status', 1)
        ->orderBy('cargos.id', 'DESC')
        ->paginate(25);

        return $cargos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userAuth = auth('api')->user();
        
        $data = $request->all();
        $data['empresas_id'] = $userAuth->empresas_id;

       $cargo = Cargo::create($data);

        $log = Log::create([
            'empresas_id' => $userAuth->empresas_id,
            'users_id' => $userAuth->id,
            'tabela' => Cargo::class,
            'tabela_id' => $cargo->id,
            'atividade' => 'cadastrou',
            'status' => 1
        ]);

        return response()->json($data, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cargo = Cargo::findOrFail($id);

        return $cargo;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userAuth = auth('api')->user();
        $cargo = Cargo::findOrFail($id);
        $cargo->update($request->all());

        Log::create([
            'empresas_id' => $userAuth->empresas_id,
            'users_id' => $userAuth->id,
            'tabela' => Cargo::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($cargo, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userAuth = auth('api')->user();
        Cargo::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => $userAuth->empresas_id,
            'users_id' => $userAuth->id,
            'tabela' => Cargo::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
