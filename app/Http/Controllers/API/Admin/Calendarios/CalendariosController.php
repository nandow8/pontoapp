<?php

namespace App\Http\Controllers\API\Admin\Calendarios;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Admin\Calendario;
use App\Models\Admin\CalendariosData;
use Illuminate\Http\Request;
use App\Models\Admin\Log;

class CalendariosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $calendario = Calendario::select(
                'calendarios.id',
                'calendarios.nome_calendario',
                'empresas.name as empresas_name'
            )->join('empresas', 'empresas.id', 'calendarios.empresas_id')
            ->where(function($query) use ($search){
                $query->where('calendarios.nome_calendario','LIKE', "%$search%")
                    ->orWhere('empresas.name','LIKE', "%$search%");
            })->where('calendarios.status', 1)
            ->paginate(20);
        }else{
            $calendario = $this->index();
        }

        return $calendario;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calendario = Calendario::select(
            'calendarios.id',
            'calendarios.nome_calendario',
            'empresas.name as empresas_name'
        )->join('empresas', 'empresas.id', 'calendarios.empresas_id')->orderBy('calendarios.id', 'DESC')->where('calendarios.status', 1)->paginate(20);

        return $calendario;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userAuth = auth('api')->user();
        $data = $request->all();
        
        $data['empresas_id'] = $userAuth->empresas_id;
        $calendario = Calendario::create([
            'empresas_id' => $data['empresas_id'],
            'nome_calendario' => $data['nome_calendario'],
            'ano' => $data['ano'],
        ]);

        foreach ($request->rows as $key => $value) {
            $data_inicio = null;
            $data_fim = null;

            if(isset($value['data_periodo_inicio'])) {
                $data_inicio =  $this->formatDateTimeToBD($value['data_periodo_inicio'] . date('H:i'));
            } else {
                $data_inicio = null;
            }

            if(isset($value['data_periodo_fim'])) {
                $data_fim =  $this->formatDateTimeToBD($value['data_periodo_fim'] . date('H:i'));
            } else {
                $data_fim = null;
            }


            CalendariosData::create([
                'empresas_id' => $userAuth->empresas_id,
                'calendarios_id' => $calendario->id,
                'descricao_feriado_ou_recesso' => $value['descricao_feriado_ou_recesso'] ?? null,
                'data_periodo_inicio' =>  $data_inicio ?? null,
                'data_periodo_fim' =>  $data_fim ?? null,
                'dia_inteiro_apuracao' => $value['dia_inteiro_apuracao'] ?? null,
                'apuracao' => $value['apuracao'] ?? null,
                'horario_apuracao' => $value['horario_apuracao'] ?? null,
            ]);
        }

        Log::create([
            'empresas_id' => $userAuth->empresas_id,
            'users_id' => $userAuth->id,
            'tabela' => Calendario::class,
            'tabela_id' => $calendario->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($calendario, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $calendario = Calendario::findOrFail($id);
        $calendario->push($calendario->datas);
        return $calendario;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $calendario = Calendario::findOrFail($id);
        $calendario->update([
            'nome_calendario' => $data['nome_calendario'],
            'ano' => $data['ano'],
        ]);

        CalendariosData::where('empresas_id', $data['empresas_id'])
            ->where('calendarios_id', $id)
            ->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        foreach ($request->rows as $key => $value) {
            $data_inicio = null;
            $data_fim = null;

            if(isset($value['data_periodo_inicio'])) {
                $data_inicio =  $this->formatDateTimeToBD($value['data_periodo_inicio'] . date('H:i'));
            } else {
                $data_inicio = null;
            }

            if(isset($value['data_periodo_fim'])) {
                $data_fim =  $this->formatDateTimeToBD($value['data_periodo_fim'] . date('H:i'));
            } else {
                $data_fim = null;
            }


            CalendariosData::create([
                'empresas_id' => auth('api')->user()->empresas_id,
                'calendarios_id' => $calendario->id,
                'descricao_feriado_ou_recesso' => $value['descricao_feriado_ou_recesso'] ?? null,
                'data_periodo_inicio' =>  $data_inicio ?? null,
                'data_periodo_fim' =>  $data_fim ?? null,
                'dia_inteiro_apuracao' => $value['dia_inteiro_apuracao'] ?? null,
                'apuracao' => $value['apuracao'] ?? null,
                'horario_apuracao' => $value['horario_apuracao'] ?? null,
            ]);
        }

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Calendario::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($calendario, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Calendario::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Calendario::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }

    public function formatDateTimeToBD($datetime, $format = "Y-m-d H:i")
    {

        $data_entrada = $datetime;
        $formato_entrada = "d/m/Y H:i";
        $formato_saida = "Y-m-d H:i";

        try {
            $data = \DateTime::createFromFormat($formato_entrada, $data_entrada);

            $erros = \DateTime::getLastErrors();

            if ($erros['warning_count'] or $erros['error_count']) {
                throw new \Exception("Data inválida: '{$data_entrada}'; a data precisa estar no formato '{$formato_entrada}'."
                    . " Avisos ({$erros['warning_count']}): " . implode(". ", $erros['warnings'])
                    . " Erros ({$erros['error_count']}): " . implode(". ", $erros['errors'])
                );
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit;
        }

        $data_saida = $data->format($formato_saida);

        return $data_saida; // yyyy-mm-dd H:i
    }

}
