<?php

namespace App\Http\Controllers\API\Admin\Calendarios;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Admin\CalendariosData;
use Illuminate\Http\Request;
use App\Models\Admin\Log;

class CalendariosDatasController extends Controller
{
    // Route::get('calendariosdatas/findCalendariosData', 'API\Comum\CalendariosDatas\CalendariosDatasController@search');
    // Route::apiResources(['calendariosdatas' => 'API\Comum\CalendariosDatas\CalendariosDatasController']);

    // { path: '/calendariosdatas', component: require('./pages/comum/calendariosdatas/index.vue').default },
    // { path: '/calendariosdatas/create', component: require('./pages/comum/calendariosdatas/create.vue').default },
    // { path: '/calendariosdatas/:id/edit', component: require('./pages/comum/calendariosdatas/edit.vue').default },
    // { path: '/calendariosdatas/:id', component: require('./pages/comum/calendariosdatas/show.vue').default },

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $calendariosdata = CalendariosData::where(function($query) use ($search){
                $query->where('status', 1)
                    ->orWhere('email','LIKE', '%' . $search. '%');
            })->paginate(20);
        }else{
            $calendariosdata = CalendariosData::where('status', 1)->latest()->paginate(20);
        }

        return $calendariosdata;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $calendariosdatas = CalendariosData::where('status', 1)->latest()->paginate(25);

        return $calendariosdatas;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $calendariosdata = CalendariosData::create($data);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => CalendariosData::class,
            'tabela_id' => $calendariosdata->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($calendariosdata, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $calendariosdata = CalendariosData::findOrFail($id);

        return $calendariosdata;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $calendariosdata = CalendariosData::findOrFail($id);
        $calendariosdata->update($request->all());

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => CalendariosData::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($calendariosdata, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CalendariosData::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => CalendariosData::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
