<?php

namespace App\Http\Controllers\Api\Admin\Menus;

use App\Http\Controllers\Controller;
use App\Models\Admin\OcorrenciasPendente;
use App\Models\Comum\Chat;
use App\Models\Comum\Chatmessage;
use App\Models\Comum\RegistroMarcacaoPontos;
use Illuminate\Http\Request;

class NavbarController extends Controller
{
    public $user = null;
    
    public function __construct() {
        $this->user = auth('api')->user();
    }

    public function ocorrenciasPendentes() {
        if ($this->user->idtipo_usuario == 3000)
            return false;

        $ocorrencias = OcorrenciasPendente::select('id')
            ->whereNull('aprovar_ou_reprovar')
            ->first();
        
        return $ocorrencias;
    }

    public function marcacoesPendentes()
    {
        if ($this->user->idtipo_usuario == 3000)
            return false;

        $marcacoes = RegistroMarcacaoPontos::select('id')
            ->where('permissao', 0)
            ->first();

        return $marcacoes;
    }

    public function chatPendentes()
    {
        if ($this->user->idtipo_usuario == 3000)
            return false;
            
        $chat = Chat::select('id')
        ->where('chamado', 'aberto')
        ->first();

        return $chat;
    }
}