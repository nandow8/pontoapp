<?php

namespace App\Http\Controllers\API\Admin\OcorrenciasJustificadas;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Admin\OcorrenciasJustificada;
use Illuminate\Http\Request;
use App\Models\Admin\Log;

class OcorrenciasJustificadasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function search(){
        if ($search = \Request::get('q')) {

            $ocorrenciasjustificada = OcorrenciasJustificada::select(
                'ocorrencias_justificadas.id',
                'ocorrencias_justificadas.descricao',
                'empresas.name as empresas_name',
                'ocorrencias_justificadas.status'
            )
            ->join('empresas', 'empresas.id', 'ocorrencias_justificadas.empresas_id');
            
            if ($search == 'Sim' OR $search == 'Não') {
                if ( $search == 'Não') $buscaSimNao = 0;
                if ( $search == 'Sim') $buscaSimNao = 1;

                $ocorrenciasjustificada = $ocorrenciasjustificada->where('ocorrencias_justificadas.status', $buscaSimNao);
            } else {
                $ocorrenciasjustificada = $ocorrenciasjustificada->where(function($query) use ($search){
                    $query->where('ocorrencias_justificadas.descricao','LIKE', "%$search%")
                          ->orWhere('empresas.name','LIKE', "%$search%");
                });
            }

            $ocorrenciasjustificada = $ocorrenciasjustificada->paginate(20);
        }else{
            $ocorrenciasjustificada = $this->index();
        }

        return $ocorrenciasjustificada;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ocorrenciasjustificadas = OcorrenciasJustificada::select(
            'ocorrencias_justificadas.id',
            'ocorrencias_justificadas.descricao',
            'empresas.name as empresas_name',
            'ocorrencias_justificadas.status'
        )->join('empresas', 'empresas.id', 'ocorrencias_justificadas.empresas_id')
        ->paginate(20);

        return $ocorrenciasjustificadas;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $ocorrenciasjustificada = OcorrenciasJustificada::create($data);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => OcorrenciasJustificada::class,
            'tabela_id' => $ocorrenciasjustificada->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($ocorrenciasjustificada, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ocorrenciasjustificada = OcorrenciasJustificada::findOrFail($id);

        return $ocorrenciasjustificada;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $ocorrenciasjustificada = OcorrenciasJustificada::findOrFail($id);
        $ocorrenciasjustificada->update($request->all());

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => OcorrenciasJustificada::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($ocorrenciasjustificada, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OcorrenciasJustificada::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => OcorrenciasJustificada::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
