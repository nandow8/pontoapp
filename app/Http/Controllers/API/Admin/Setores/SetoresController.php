<?php

namespace App\Http\Controllers\API\Admin\Setores;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Admin\Setores;
use Illuminate\Http\Request;
use App\Models\Admin\Log;

class SetoresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function search(){
        if ($search = \Request::get('q')) {
            $setores = Setores::select(
                'setores.id',
                'setores.name',
                'empresas.name as empresas_name'
            )->join('empresas', 'empresas.id', 'setores.empresas_id')
            ->where(function($query) use ($search){
                $query->where('setores.name', 'LIKE', "%$search%")
                      ->orWhere('empresas.name', 'LIKE', "%$search%");
            })->where('setores.status', 1)
            ->paginate(20);
        }else{
            $setores = $this->index();
        }

        return $setores;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setores = Setores::select(
            'setores.id',
            'setores.name',
            'empresas.name as empresas_name'
        )->join('empresas', 'empresas.id', 'setores.empresas_id')
        ->paginate(20);

        return $setores;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $setore = Setores::create($data);

        $log = Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Setores::class,
            'tabela_id' => $setore->id,
            'atividade' => 'cadastrou',
            'status' => 1
        ]);

        return response()->json($setore, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $setore = Setores::findOrFail($id);

        return $setore;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $setore = Setores::findOrFail($id);
        $setore->update($request->all());

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Setores::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($setore, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Setores::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Setores::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
