<?php

namespace App\Http\Controllers\API\Admin\Filial;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Admin\Filial;
use Illuminate\Http\Request;
use App\Models\Admin\EmpresasEndereco;
use App\Models\Admin\EmpresasManySetores;
use App\Models\Admin\Log;

class FilialController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $filial = Filial::selectRaw(
                    'empresas.id as empresas_id,
                    empresas.name as empresas_name,
                    filiais.id,
                    filiais.cnpj,
                    filiais.name'
                )
                ->join('empresas', 'empresas.id', 'filiais.empresas_id')
                ->where(function($query) use ($search){
                    $query->where('filiais.name', 'LIKE', "%$search%");
                    $query->orWhere('empresas.name', 'LIKE', "%$search%");
                })
                ->where('filiais.status', 1)
                ->where('empresas.status', 1)
            ->paginate(20);
        }else{
            $filial = Filial::selectRaw(
                    'empresas.id as empresas_id,
                    empresas.name as empresas_name,
                    filiais.id,
                    filiais.cnpj,
                    filiais.name'
                )->join('empresas', 'empresas.id', 'filiais.empresas_id')
                ->where('filiais.status', 1)
                ->where('empresas.status', 1)->orderBy('filiais.id', 'DESC')->paginate(20);
        }

        return $filial;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filial = Filial::selectRaw(
            'empresas.id as empresas_id,
            empresas.name as empresas_name,
            filiais.id,
            filiais.cnpj,
            filiais.name'
        )
        ->join('empresas', 'empresas.id', 'filiais.empresas_id')
        ->where('filiais.status', 1)
        ->where('empresas.status', 1)
        ->orderBy('filiais.created_at')
        ->paginate(25);

        return $filial;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'cnpj' => 'required|string|max:191',
            'cep' => 'required|string|max:191',
            'logradouro' => 'required|string|max:191',
            'numero' => 'required|string|max:191',
            'estado' => 'required|string|max:191',
            'cidade' => 'required|string|max:191',
            'bairro' => 'required|string|max:191',
            'telefone' => 'required|string|max:191',
            'email' => 'required',
        ]);

        $filial = Filial::create([
            'name' => $request->name,
            'cnpj' => $request->cnpj,
            'empresas_id' => auth('api')->user()->empresas_id,
        ]);

        $endereco = EmpresasEndereco::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'filiais_id' => $filial->id,
            'cep' => $request->cep,
            'logradouro' => $request->logradouro,
            'numero' => $request->numero,
            'complemento' => $request->complemento,
            'estado' => $request->estado,
            'cidade' => $request->cidade,
            'bairro' => $request->bairro,
            'telefone' => $request->telefone,
            'telefone_alternativo' => $request->telefone_alternativo,
            'email' => $request->email,
            'auth_user_id' => auth('api')->user()->id,
            'contato' => $request->contato,
            'status' => 1
        ]);

        Log::create([
            'users_id' => auth('api')->user()->id,
            'tabela' => Filial::class,
            'tabela_id' => $filial->id,
            'atividade' => 'cadastro'
        ]);

        return response()->json($filial, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $filial = Filial::findOrFail($id);

        $filial->push($filial->empresas_endereco);

        return $filial;
    }

    // public function show($id)
    // {
    //     $filial = Filial::findOrFail($id)->where('empresas_id', auth('api')->user()->empresas_id);
    //     $setores = array();

    //     foreach ($filial->empresas_setores as $key => $value) {
    //         $setores[$key] = $value->setores;
    //     }

    //     $filial->push($filial->empresas_endereco);
    //     $filial->push($setores);

    //     return $filial;
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'cnpj' => 'required|string|max:191',
            'empresas_id' => 'required',
            'cep' => 'required|string|max:191',
            'logradouro' => 'required|string|max:191',
            'numero' => 'required|string|max:191',
            'estado' => 'required|string|max:191',
            'cidade' => 'required|string|max:191',
            'bairro' => 'required|string|max:191',
        ]);

        $filial = Filial::findOrFail($id);
        $filial->update([
            'name' => $request->name,
            'cnpj' => $request->cnpj,
        ]);

        $endereco = EmpresasEndereco::where('filiais_id', $filial->id)->first();
        $endereco->update([
            'cep' => $request->cep,
            'logradouro' => $request->logradouro,
            'numero' => $request->numero,
            'complemento' => $request->complemento,
            'estado' => $request->estado,
            'cidade' => $request->cidade,
            'bairro' => $request->bairro,
            'telefone' => $request->telefone,
            'telefone_alternativo' => $request->telefone_alternativo,
            'email' => $request->email,
            'auth_user_id' => auth('api')->user()->id,
            'filial' => 1,
            'contato' => $request->contato,
            'status' => 1
        ]);

        Log::create([
            'users_id' => auth('api')->user()->id,
            'tabela' => Filial::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($filial, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Filial::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'users_id' => auth('api')->user()->id,
            'tabela' => Filial::class,
            'tabela_id' => $id,
            'atividade' => 'destruiu'
        ]);

        return response()->json(null, 204);
    }
}
