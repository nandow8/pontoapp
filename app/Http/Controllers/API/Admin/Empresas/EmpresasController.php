<?php

namespace App\Http\Controllers\API\Admin\Empresas;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Admin\Empresa;
use Illuminate\Http\Request;
use App\Models\Admin\EmpresasEndereco;
use App\Models\Admin\EmpresasManySetores;
use App\Models\Admin\Log;
use App\Models\Comum\UsersManyEmpresasSetores;
use App\User;
use Hash;

class EmpresasController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $empresa = Empresa::where(function($query) use ($search){
                $query->where('name', 'LIKE', "%$search%");
                $query->where('codigo', 'LIKE', "%$search%");
                $query->where('cnpj', 'LIKE', "%$search%");
            })->where('status', 1)->paginate(20);
        }else{
            $empresa = Empresa::where('status', 1)->latest()->paginate(20);
        }

        return $empresa;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $empresas = Empresa::latest()->paginate(25);

        return $empresas;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'cnpj' => 'required|string|max:191',
            'cep' => 'required|string|max:191',
            // 'codigo' => 'required|unique:empresas,codigo|max:4',
            'codigo' => 'required|max:4',
            'logradouro' => 'required|string|max:191',
            'numero' => 'required|string|max:191',
            'estado' => 'required|string|max:191',
            'telefone' => 'required|string|max:191',
            'email' => 'required|string|max:191',
            'cliente_password' => 'required',
            'cidade' => 'required|string|max:191',
            'bairro' => 'required|string|max:191', 
            'cliente_password' => 'required|string|min:6', 
        ]);

        if (Empresa::where('codigo', $request->codigo)->first())
            return response()->json(0, 400);

        if (User::where('email', $request->email)->first())
            return response()->json(0, 400);

        $empresa = Empresa::create([
            'name' => $request->name,
            'cnpj' => $request->cnpj,
            'codigo' => $request->codigo,
            'status' => 1,
        ]);

        $endereco = EmpresasEndereco::create([
            'empresas_id' => $empresa->id,
            'cep' => $request->cep,
            'logradouro' => $request->logradouro,
            'numero' => $request->numero,
            'complemento' => $request->complemento,
            'estado' => $request->estado,
            'cidade' => $request->cidade,
            'bairro' => $request->bairro,
            'telefone' => $request->telefone,
            'telefone_alternativo' => $request->telefone_alternativo,
            'email' => $request->email,
            'auth_user_id' => auth('api')->user()->id,
            'status' => 1
        ]);

        $user = User::create([
            'empresas_id' => $empresa->id,
            'name' => $request->name,
            'email' => $request->email,
            'type' => 'admin',
            'idtipo_usuario' => 2000, // 2000 cliente
            'password' => Hash::make($request->cliente_password),
        ]);

        $empresa->update(['user_cliente_id' => $user->id]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Empresa::class,
            'tabela_id' => $user->id,
            'atividade' => 'cadastrou',
            'status' => 1
        ]);

        return response()->json($empresa, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = Empresa::findOrFail($id);

        $empresa->push($empresa->empresas_endereco);

        return $empresa;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'cnpj' => 'required|string|max:191',
            'cep' => 'required|string|max:191',
            'codigo' => 'required|max:4|sometimes:empresas,codigo',
            'logradouro' => 'required|string|max:191',
            'numero' => 'required|string|max:191',
            'estado' => 'required|string|max:191',
            'cidade' => 'required|string|max:191',
            'bairro' => 'required|string|max:191',
            'email' => 'required|sometimes:users,email',
        ]);

        $empresa = Empresa::findOrFail($id);
        $empresa->update([
            'name' => $request->name,
            'cnpj' => $request->cnpj,
            'codigo' => $request->codigo
        ]);

        $user = User::findOrFail($empresa->user_cliente_id);
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->cliente_password) {
            $user->password = Hash::make($request->cliente_password);
        }    
        $user->save();

        $endereco = EmpresasEndereco::where('empresas_id', $empresa->id)->first();
        $endereco->update([
            'cep' => $request->cep,
            'logradouro' => $request->logradouro,
            'numero' => $request->numero,
            'complemento' => $request->complemento,
            'estado' => $request->estado,
            'cidade' => $request->cidade,
            'bairro' => $request->bairro,
            'telefone' => $request->telefone,
            'telefone_alternativo' => $request->telefone_alternativo,
            'email' => $request->email,
            'auth_user_id' => $request->auth_user_id,
            'filial' => 0,
            'status' => 1
        ]);

        // EmpresasManySetores::where('empresas_id', $empresa->id)->update(['status' => 0]);

        // foreach ($request->setores_rows as $key => $value) {
        //     EmpresasManySetores::create([
        //         'setores_id' => $value['id'],
        //         'empresas_id' => $empresa->id,
        //     ]);
        // }

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Empresa::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json(true, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empresa = Empresa::where('id', $id)->first();
        if ($empresa->status == 1) {
            $empresa->update(['status' => 0]);
        } else {
            $empresa->update(['status' => 1]);
        }

        $user = User::findOrFail($empresa->user_cliente_id);
        if ($user->status == 1) {
            $user->update(['status' => 0]);
        } else {
            $user->update(['status' => 1]);
        }
        return $user;
        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Empresa::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }

    public function empresasByempresasId()
    {
        return Empresa::select('cnpj')->where('status', 1)->first();
    }
}
