<?php

namespace App\Http\Controllers\API\Admin\Logs;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Admin\Log;
use Illuminate\Http\Request;

class LogsController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $log = Log::where(function($query) use ($search){
                $query->where('status', 1)
                    ->orWhere('email','LIKE', '%' . $search. '%');
            })->paginate(20);
        }else{
            $log = Log::where('status', 1)->latest()->paginate(20);
        }

        return $log;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 

        // if(\Gate::allows('isAdmin') OR \Gate::allows('isMaster')) {
            $logs = Log::selectRaw(
                'users.name,
                empresas.name AS empresas_name,
                setores.name AS setores_name,
                logs.id,
                logs.atividade,
                logs.created_at'
            )
            ->leftJoin('users', 'users.id', 'logs.users_id')
            ->join('empresas', 'empresas.id', 'users.empresas_id')
            ->join('setores', 'setores.empresas_id', 'users.empresas_id')
            ->groupBy('logs.id')
            ->where('logs.status', 1)
            ->orderBy('logs.id')
            ->paginate(25);

            return $logs;
        // }

        return json_encode(0);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $log = Log::create($request->all());

        return response()->json($log, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $log = Log::findOrFail($id);

        return $log;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $log = Log::findOrFail($id);
        $log->update($request->all());

        return response()->json($log, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        Log::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        return response()->json(null, 204);
    }
}
