<?php

namespace App\Http\Controllers\API\Admin\Controledeponto;

use App\Http\Controllers\Controller;
use App\Models\Admin\Afdafdt;
use App\Models\Comum\RegistroPonto;
use Illuminate\Http\Request;
use App\Models\Admin\Log;
use App\Models\Comum\Colaborador;
use App\Models\Comum\Intervalo;
use App\Models\Comum\RegistroMarcacaoPontos;
use App\Utils;
use App\Models\Comum\UsersEscala;
use App\Models\Comum\Jornada;
use App\User;
use DateTime;

class ImportafdController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:api');
    }

    public function search()
    {
        if ($search = \Request::get('q')) {
            $empresa = Afdafdt::where(function($query) use ($search){
                $query->where('name', 'LIKE', "%$search%");
            })
            ->where('status', 1)->paginate(20);
        }else{
            $empresa = $this->index();
        }

        return $empresa;
    }

    public function index()
    {
        $afdafdt = Afdafdt::where('status', 1)
            ->paginate(25);
         
        return $afdafdt;
    }

    public function store(Request $request)
    {
        $arquivo_tmp = $_FILES['file']['tmp_name'];
        $dados = file($arquivo_tmp);

        $registro = RegistroMarcacaoPontos::select('registro_pontos_id')->where('status', 1)->orderBy('registro_pontos_id', 'DESC')->first();
        $countParaDeletarMaisTarde = auth('api')->user()->empresas_id . date('m') . date('d') . date('H') . date('i');

        $usuariosImportados = [];

        foreach($dados as $key => $linha){
            if ($key == 0) {
                $buscaAfdafdt = Afdafdt::where('name', '=', $linha)->first();
                
                if (empty($buscaAfdafdt)) {
                    Afdafdt::create([
                        'name' => $linha,
                        'empresas_id' => auth('api')->user()->empresas_id,
                        'num_para_reverter_import' => $countParaDeletarMaisTarde
                    ]);
                } else {
                    return response()->json($key, 401);
                }
            } else {
                $pis = (string) substr($linha, 22, 12);
                
                $user = User::where('pis', '=', $pis)->first();
                if (!$user) {
                    $usuariosImportados['nao_importados'][$key] = $pis;
                    continue;
                }

                $usuariosImportados['importados'][$key] = $user;

                $nsr = substr($linha, 0, 9);
                $marcacaoPonto = substr($linha, 9, 1);
                
                $dia = substr($linha, 10, 2);
                $mes = substr($linha, 12, 2);
                $ano = substr($linha, 14, 4);
                $data =  $ano . '-' . $mes . '-' . $dia;

                if ($request->inicio != '') {
                    $diaRequest_inicio = date('d', strtotime($request->inicio));
                    $mesRequest_inicio = date('m', strtotime($request->inicio));
                    $anoRequest_inicio = date('Y', strtotime($request->inicio));

                    if ($diaRequest_inicio > $dia AND $mesRequest_inicio > $mes AND $anoRequest_inicio > $ano)
                        continue;
                }

                if ($request->fim != '') {
                    $diaRequest_fim = date('d', strtotime($request->fim));
                    $mesRequest_fim = date('m', strtotime($request->fim));
                    $anoRequest_fim = date('Y', strtotime($request->fim));

                    if ($diaRequest_fim < $dia AND $mesRequest_fim < $mes AND $anoRequest_fim < $ano)
                        continue;
                }

        
                $hora = substr($linha, 18, 2);
                $minuto = substr($linha, 20, 2);
                $horario = $hora . ':' . $minuto;
        
                $diasemana = array('Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sabado');
                $diasemana_numero = date('w', strtotime($data));

                $horarioBD = $data . ' ' . $horario;
        
                RegistroMarcacaoPontos::create([
                    'empresas_id' => auth('api')->user()->empresas_id,
                    'users_id' => $user->id,
                    'aprovado_reprovado' => 1,
                    'origem_ponto' => 'AFD',
                    'registro_pontos_id' => $registro->registro_pontos_id + 1,
                    'tipo_marcacao_afd_afdt' => $marcacaoPonto,
                    'dia_semana_string' => $dia . '/' . $mes . '/' . $ano . ' ' .  $diasemana[$diasemana_numero],
                    'hora_ponto' => $horario,
                    'nsr' => $nsr,
                    'num_para_reverter_import' => $countParaDeletarMaisTarde,
                    'permissao' => 1,
                    'created_at' => date('Y-m-d h:i:s', strtotime($horarioBD))
                ]);
            }
        }
        return response()->json($usuariosImportados, 200);
    }

    public function destroy($id)
    {
        RegistroMarcacaoPontos::where('num_para_reverter_import', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);
        Afdafdt::where('num_para_reverter_import', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => RegistroMarcacaoPontos::class,
            'tabela_id' => $id,
            'atividade' => 'removeu importações'
        ]);

        return response()->json(null, 204);
    }
}
