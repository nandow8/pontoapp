<?php

namespace App\Http\Controllers\API\Admin\Controledeponto;

use App\Http\Controllers\Controller;

use App\Models\Comum\RegistroPonto;
use Illuminate\Http\Request;
use App\Models\Admin\Log;
use App\Models\Admin\OcorrenciasPendente;
use App\Models\Comum\Colaborador;
use App\Models\Comum\Intervalo;
use App\Models\Comum\RegistroMarcacaoPontos;
use App\Utils;
use App\Models\Comum\UsersEscala;
use App\Models\Comum\Jornada;
use App\Models\Comum\UsersColaboradores;
use DateTime;
use Hamcrest\Util;
use Illuminate\Support\Facades\Log as FacadesLog;

class BancodehorasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $registroponto = RegistroMarcacaoPontos::selectRaw(
                'users.id as id,
                users.name as name,
                users.matricula as matricula,
                setores.name as setor,
                cargos.name as cargo,
                empresas.name as empresas_name,
                (
                	SELECT  COUNT(rmp.dia_semana_string) as qtd FROM registro_marcacao_pontos rmp 
			   		where rmp.users_id = users.id
                	GROUP BY rmp.dia_semana_string
                    HAVING qtd < 4
                    limit 1
                ) as inconsistencia,
                (
                    SELECT  COUNT(rmpp.permissao) as qtd FROM registro_marcacao_pontos rmpp 
                    where rmpp.users_id = users.id 
                    AND rmpp.permissao <> 1
                    GROUP BY rmpp.users_id
                    limit 1
                ) as inconsistena_permissao'
            )
            ->join('users', 'users.id', 'registro_marcacao_pontos.users_id')
            ->join('users_colaboradores', 'users_colaboradores.users_id', 'users.id')
            ->join('setores', 'setores.id', 'users_colaboradores.setores_id')
            ->join('cargos', 'cargos.id', 'users_colaboradores.cargos_id')
            ->join('empresas', 'empresas.id', 'registro_marcacao_pontos.empresas_id')
            ->where(function($query) use ($search){
                $query->where('users.name', 'LIKE', "%$search%")
                    ->orWhere('users.matricula','LIKE', "%$search%")
                    ->orWhere('setores.name','LIKE', "%$search%")
                    ->orWhere('empresas.name','LIKE', "%$search%")
                    ->orWhere('cargos.name','LIKE', "%$search%")
                    ->orWhere('empresas.name','LIKE', "%$search%");
            })
            ->groupBy('users.id')
            ->paginate(20);
        }else{
            $registroponto = $this->index();
        }

        return $registroponto;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registro = array();

        $pontos = RegistroMarcacaoPontos::selectRaw(
                'users.id as id,
                users.name as name,
                users.matricula as matricula,
                setores.name as setor,
                cargos.name as cargo,
                empresas.name as empresas_name,
                (
                	SELECT  COUNT(rmp.dia_semana_string) as qtd FROM registro_marcacao_pontos rmp 
			   		where rmp.users_id = users.id
                	GROUP BY rmp.dia_semana_string
                    HAVING qtd < 4
                    limit 1
                ) as inconsistencia,
                (
                    SELECT  COUNT(rmpp.permissao) as qtd FROM registro_marcacao_pontos rmpp 
                    where rmpp.users_id = users.id 
                    AND rmpp.permissao <> 1
                    GROUP BY rmpp.users_id
                    limit 1
                ) as inconsistena_permissao'
            )
            ->join('users', 'users.id', 'registro_marcacao_pontos.users_id')
            ->join('users_colaboradores', 'users_colaboradores.users_id', 'users.id')
            ->join('setores', 'setores.id', 'users_colaboradores.setores_id')
            ->join('cargos', 'cargos.id', 'users_colaboradores.cargos_id')
            ->join('empresas', 'empresas.id', 'registro_marcacao_pontos.empresas_id')
            ->groupBy('users.id')
            ->paginate(20);
            
            #->whereYear('created_at', date('Y'))
            #->whereMonth('created_at', date('m'))
            #->whereDay('created_at', date('d'))
        
            return $pontos;
    }

    public function exibePorData(Request $request) {
        $data = $request->all();
        $return = array();

        $colaborador = Colaborador::selectRaw(
            'users_escalas.*,
            escalas.*,
            jornadas.*,
            users_colaboradores.*,
            users.name'
        )
        ->join('users_escalas', 'users_escalas.users_id', 'users_colaboradores.users_id')
        ->join('escalas', 'escalas.id', 'users_escalas.escalas_id')
        ->join('jornadas', 'jornadas.id', 'escalas.dia_jornada')
        ->join('users', 'users.id', 'users_colaboradores.users_id')
        // ->where('users_colaboradores.empresas_id', $data[])
        ->where('users_colaboradores.users_id', $data['id'])
        ->where('users_colaboradores.status', 1)
        ->where('users_escalas.status', 1)
        ->first(); 

        
        $return['colaborador'] = $colaborador;
        $return['horasTrabalhadas'] = Utils::horasTrabalhadasTotal($data['id']);
        $return['horasAbonadas'] = Utils::horasAbonadasTotal($data['id']);
        $return['horasPrevistas'] = Utils::horasPrevistasTotal($data['id'], $colaborador->dia_jornada);
        $return['saldoTotal'] = Utils::somaTempo(Utils::subtraiTempo($return['horasTrabalhadas'], $return['horasPrevistas']), $return['horasAbonadas']);
        $return['horasPrevistasTrabalhadasAbonosSaldos'] = $this->horasPrevistasTrabalhadasAbonosSaldos($data['id'], $colaborador->dia_jornada, $data['dataInicio'], $data['dataFim']);
        
        return $return;
    }

    public function horasPrevistasTrabalhadasAbonosSaldos($users_id, $dia_jornada, $dataInicio = null, $dataFim = null) {
        $mesesAnos = Utils::mesesAnoTrabalhados($users_id, $dataInicio, $dataFim);
        
        $list = [];
        foreach ($mesesAnos['inicio'] as $key => $value) {
            $list['horasTrabalhadas'][$key] = Utils::horasTrabalhadasTotalByDate($users_id, $mesesAnos['inicio'][$key], $mesesAnos['fim'][$key]);
            $list['horasAbonadas'][$key] = Utils::horasAbonadasTotalByDate($users_id, $mesesAnos['inicio'][$key], $mesesAnos['fim'][$key]);
            $list['horasPrevistas'][$key] = Utils::horasPrevistasMesAtualTotal($users_id, $dia_jornada);
            
            $numeroMes = explode('-', $value);
            $list['meses'][$key] = $numeroMes[1] < 10 
                ? Utils::exibeNomeDoMes(substr($numeroMes[1], 1)) . ' de ' . $numeroMes[0]
                : Utils::exibeNomeDoMes($numeroMes[1]) . ' de ' . $numeroMes[0];

            $list['saldos'][$key] = Utils::somaTempo(Utils::subtraiTempo($list['horasTrabalhadas'][$key], $list['horasPrevistas'][$key]), $list['horasAbonadas'][$key]);
        }

        return $list;
    }

}
