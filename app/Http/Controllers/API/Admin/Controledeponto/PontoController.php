<?php

namespace App\Http\Controllers\API\Admin\Controledeponto;

use App\Http\Controllers\Controller;

use App\Models\Comum\RegistroPonto;
use Illuminate\Http\Request;
use App\Models\Admin\Log;
use App\Models\Admin\OcorrenciasPendente;
use App\Models\Comum\Colaborador;
use App\Models\Comum\Intervalo;
use App\Models\Comum\RegistroMarcacaoPontos;
use App\Utils;
use App\Models\Comum\UsersEscala;
use App\Models\Comum\Jornada;
use App\Models\Comum\UsersColaboradores;
use DateTime;
use Hamcrest\Util;
use Illuminate\Support\Facades\DB;

class PontoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            if ($search != 'Sim') {
                $registroponto = $this->searchSemInconsistencia($search);
            } else {
                $registroponto = $this->searchInconsistencia($search);
            }
        }else{
            $registroponto = $this->index();
        }

        return $registroponto;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registro = array();

        $pontos = RegistroMarcacaoPontos::selectRaw(
                'users.id as id,
                users.name as name,
                users.matricula as matricula,
                setores.name as setor,
                cargos.name as cargo,
                empresas.name as empresas_name,
                (
                	SELECT  COUNT(rmp.dia_semana_string) as qtd FROM registro_marcacao_pontos rmp 
			   		where rmp.users_id = users.id
                	GROUP BY rmp.dia_semana_string
                    HAVING qtd < 4
                    limit 1
                ) as inconsistencia,
                (
                    SELECT  COUNT(rmpp.permissao) as qtd FROM registro_marcacao_pontos rmpp 
                    where rmpp.users_id = users.id 
                    AND rmpp.permissao <> 1
                    GROUP BY rmpp.users_id
                    limit 1
                ) as inconsistena_permissao'
            )
            ->join('users', 'users.id', 'registro_marcacao_pontos.users_id')
            ->join('users_colaboradores', 'users_colaboradores.users_id', 'users.id')
            ->join('setores', 'setores.id', 'users_colaboradores.setores_id')
            ->join('cargos', 'cargos.id', 'users_colaboradores.cargos_id')
            ->join('empresas', 'empresas.id', 'registro_marcacao_pontos.empresas_id')
            ->groupBy('users.id')
            ->paginate(25);
            
            #->whereYear('created_at', date('Y'))
            #->whereMonth('created_at', date('m'))
            #->whereDay('created_at', date('d'))
        
            return $pontos;
    }

    public function exibeMarcacoesDePonto(Request $request) {
        $data = $request->all();

        $registros = RegistroMarcacaoPontos::select(
            'id',
            'users_id',
            'empresas_id',
            'registro_pontos_id',
            'dia_semana_string',
            'aprovado_reprovado',
            'permissao',
            'hora_ponto',
            'created_at',
            'foto',
            'origem_ponto',
            'justificativa_colaborador',
            'observacao_colaborador',
            'justificativa_empresa',
            'observacao_empresa'
        )
        ->where('users_id', $data['id']);
        // ->whereMonth('created_at', date('m'))
        // ->whereYear('created_at', date('Y'))
        //->whereBetween('created_at', [$quantidadeDiasColaboradorVisualiza, date('Y-m-d')])
        // $registros->where('permissao', '!=', 4);
        $registros->orderBy('created_at', 'ASC');
        

        if ($data['dataInicio'] != '') {
            $dataInicioReplace = str_replace("/", "-", $data["dataInicio"]);
            $dataInicio = date('Y-m-d', strtotime($dataInicioReplace));

            $registros->whereDate('created_at', '>=', $dataInicio);
        } 
        // else {
        //     $registros->whereDate('created_at', '>=', date('Y-m-d', strtotime("-30 days")));
        // }

        if ($data['dataFim'] != '') {
            $dataFimReplace = str_replace("/", "-", $data["dataFim"]);
            $dataFim = date('Y-m-d', strtotime($dataFimReplace));
            
            $registros->whereDate('created_at', '<=', $dataFim);
        }
        
        $array = [];
        $collection = collect($registros->get())->groupBy('dia_semana_string');
        foreach ($collection as $key => $value) {
             $array[$key] = $value;
        }
        
        $reverso = null;

        $escala = $this->buscaEscala($data['id']);
        $horasPrevistas = Utils::diferencaHoras($escala['inicio_expediente'], $escala['fim_expediente']);

        // krsort($array);
        foreach ($array as $chave => $valor) {
            $reverso['registros'][$chave] = $valor;

            $reverso['registros'][$chave]['previstas'] = $horasPrevistas;
            $ocorrencias = OcorrenciasPendente::whereDate('periodo_inicio', '=', date(Utils::convertFromDateBr(substr($chave, 0, 10))))->get();
            $reverso['registros'][$chave]['ocorrencias'] = $ocorrencias;

            if (count($ocorrencias) > 0 AND $ocorrencias[0]->dia_ou_datahora != 1 AND $ocorrencias[0]->aprovar_ou_reprovar == 1) {
                $reverso['registros'][$chave]['trabalhadas'] = $horasPrevistas;
                $reverso['registros'][$chave]['saldos'] = '00:00';
                $reverso['registros'][$chave]['abono'] = $horasPrevistas;
            }
            
            else if (count($ocorrencias) > 0 AND $ocorrencias[0]->dia_ou_datahora == 1 AND $ocorrencias[0]->aprovar_ou_reprovar == 1) {
                $abono = Utils::diferencaHoras(
                    substr($ocorrencias[0]->periodo_inicio, 11, 5), 
                    substr($ocorrencias[0]->periodo_fim, 11, 5)
                );

                $reverso['registros'][$chave]['trabalhadas'] = Utils::calculaHorasTrabalhadasEintervalos($valor);
                $reverso['registros'][$chave]['saldos'] = Utils::saldoHoras($abono ,Utils::saldoHoras(Utils::calculaHorasTrabalhadasEintervalos($valor), $horasPrevistas));
                $reverso['registros'][$chave]['abono'] = $abono;
            }
            else {
                $reverso['registros'][$chave]['trabalhadas'] = Utils::calculaHorasTrabalhadasEintervalos($valor);
                $reverso['registros'][$chave]['saldos'] = Utils::saldoHoras(Utils::calculaHorasTrabalhadasEintervalos($valor), $horasPrevistas);
                $reverso['registros'][$chave]['abono'] = '00:00';
            }

            $reverso['registros'][$chave]['intervalos'] = Utils::calculaHorasTrabalhadasEintervalos($valor, 1);
            

        }

        return response()->json(
            [
                'marcacoes_de_pontos' => $reverso['registros'],
                'total' => $this->totalHorasPrevistasMesAtual(),
            ],
        );
    }

    public function buscaEscala($users_id)
    {
        return UsersEscala::select(
            'users_escalas.data_vigente',
            'escalas.created_at',
            'escalas.nome_escala',
            'escalas.tipo',
            'escalas.intervalos_id',
            'escalas.dia_jornada',
            'escalas.tolerancia',
            'escalas.tolerancia_tempo',
            'escalas.inicio_expediente',
            'escalas.fim_expediente',
            'escalas.jornada_hora_dia',
            'escalas.dias_trabalhar',
            'escalas.dias_dsr_folga',
            'escalas.status',
            'escalas.expediente_livre',
            'escalas.inicioexpediente_livre',
            'escalas.tolerancia_por_marcacao',
        )
        ->join('escalas', 'escalas.id', 'users_escalas.escalas_id')
        ->where('users_escalas.users_id', $users_id)
        // ->where('users_escalas.empresas_id', auth('api')->user()->empresas_id)
        ->where('users_escalas.status', 1)
        ->first();
    }

    public function totalHorasPrevistasMesAtual() { // acho que nem ta usando
        $registro = array();
        $totalDiasMesAtual = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        $dataAtual = date('Y-m-d');
        $quantidadeHorasTrabalhadas = 0;

        $colaborador = Colaborador::selectRaw(
                'users_escalas.*,
                "ovo",
                escalas.*,
                "AVAAA",
                users_colaboradores.*'
            )
            ->join('users_escalas', 'users_escalas.users_id', 'users_colaboradores.users_id')
            ->join('escalas', 'escalas.id', 'users_escalas.escalas_id')
            ->where('users_colaboradores.empresas_id', 8)
            ->where('users_colaboradores.users_id', 60)
            ->where('users_colaboradores.status', 1)
            ->where('users_escalas.status', 1)
            ->get(); 
            
        foreach ($colaborador as $key => $value) {
            if ( $value->dia_jornada) {
                if ( $value->dia_jornada = 12) { // 12 segunda a sexta
                    
                    $dias = $totalDiasMesAtual / 7;
                    $trabalhadas = $dias * 5; 

                    $date1 = $dataAtual . ' ' .  $value['inicio_expediente'];
                    $date2 = $dataAtual . ' ' .  $value['fim_expediente'];
                    $horasDiarias = abs(strtotime($date2) - strtotime($date1))/(60*60);

                    $intervalos = Intervalo::where('escalas_id',  $value->escalas_id)->get();
                    
                    $qtdHorasIntervalos = 0;
                    foreach ($intervalos as $key => $value) {
                        $qtdHorasIntervalos += abs(strtotime($dataAtual . ' ' . $value['inicio_intervalo']) - strtotime($dataAtual . ' ' . $value['fim_intervalo']))/(60*60);
                    }

                    $quantidadeHorasTrabalhadas += ($horasDiarias * $trabalhadas) - ($qtdHorasIntervalos * $trabalhadas);
                    
                    return $quantidadeHorasTrabalhadas;
                }

                if ( $value->dia_jornada <= 7) { // um unico dia da semana
                    return Utils::contadiasdasemana($value->dia_jornada, date('m'), date('Y'));
                }
            }
        }

        return $quantidadeHorasTrabalhadas;
    }


    public function store(Request $request) {
        
    }

    public function aprovarPonto(Request $request)
    {
        $ponto = RegistroMarcacaoPontos::where('id', $request->ponto['id'])->first();
        
        $ponto->permissao = 1;
        $ponto->justificativa_empresa = $request->ponto['aprova_justificativa'] ?? null;
        $ponto->observacao_empresa = $request->ponto['aprova_observacao'] ?? null;    
        $ponto->save();

        return response()->json($ponto, 201);
    }

    public function reprovarPonto(Request $request)
    {
        $ponto = RegistroMarcacaoPontos::where('id', $request->ponto['id'])->first();
        
        $ponto->permissao = 4;
        $ponto->justificativa_empresa = $request->ponto['reprova_justificativa_empresa'] ?? null;
        $ponto->observacao_empresa = $request->ponto['reprova_observacao_empresa'] ?? null;    
        $ponto->save();

        return response()->json($ponto, 201);
    }

    public function corrigirPonto(Request $request)
    {
        $hora_ponto = str_pad($request->ponto['hora_ponto'], 5, '0',  STR_PAD_LEFT);
        $timestamp = Utils::formatDateTimeToView($request->ponto['created_at'], 'Y-m-d')  . ' ' .  $hora_ponto . ':00';

        $ponto = RegistroMarcacaoPontos::where('id', $request->ponto['id'])->first();
        $ponto->justificativa_empresa = $request->ponto['justificativa'] ?? null;
        $ponto->observacao_empresa = $request->ponto['observacao'] ?? null;    
        $ponto->created_at = $timestamp;
        $ponto->updated_at = $timestamp;
        $ponto->hora_ponto = $hora_ponto;
        $ponto->save();
    }

    public function lancarPontoManual(Request $request)
    {
        $data = $request->ponto;
        $data['empresas_id'] = auth('api')->user()->empresas_id;
        $data['users_id'] = $request->ponto['users_id'];
        $data['registro_pontos_id'] = $request->ponto['registro_pontos_id'];
        
        $registroponto = RegistroMarcacaoPontos::insertGetId([
            'empresas_id' => $data['empresas_id'],
            'users_id' => $data['users_id'],
            'registro_pontos_id' => $data['registro_pontos_id'],
            //'foto' => '/img/colaboradores/ponto' . $currentPhoto,
            // 'observacao_colaborador' => $data['observacao_colaborador'],
            // 'justificativa_colaborador' => $data['justificativa_colaborador'],
            'origem_ponto' => 'WEB',
            'aprovado_reprovado' => 1,
            'permissao' => 1,
            'hora_ponto' => str_pad($data['hora_ponto_vazio'], 5, '0',  STR_PAD_LEFT),
            'dia_semana_string' => Utils::diaSemanaStringAdminManual(date('Y-m-d', strtotime($data['created_at']))),
            'created_at' => date('Y-m-d', strtotime($data['created_at'])) . ' ' . str_pad($data['hora_ponto_vazio'], 5, '0',  STR_PAD_LEFT) . ':00',
            'numero_recibo' => $data['empresas_id'] . $data['users_id'] . date('d') . date('Y') . date('m') . date('s') . date('h') . date('i')
        ]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => RegistroMarcacaoPontos::class,
            'tabela_id' => $registroponto,
            'atividade' => 'cadastrou ponto manual'
        ]);

        return response()->json(RegistroMarcacaoPontos::where('id', $registroponto)->first(), 201);
    }

    public function lancarOcorrencia(Request $request)
    {        
        $data = $request->ponto;
        
        $ocorrencia = null;
        if ($data['dia_ou_datahora'] == 1) {
            $ocorrencia = OcorrenciasPendente::create([
                'created_at' => $data['created_at'],
                'empresas_id' => $data['empresas_id'],
                'users_id' => $data['users_id'],
                'ocorrencias_justificadas_id' => $data['ocorrencias_justificadas_id'],
                'descricao' => $data['descricao'] ?? null,
                'dia_ou_datahora' => $data['dia_ou_datahora'],
                'periodo_inicio' => $data['periodo_inicio'],
                'periodo_fim' => substr($data['periodo_inicio'], 0, 10) . ' ' .substr($data['periodo_fim'], 11, 5),
                'aprovar_ou_reprovar' => $data['aprovar_ou_reprovar'] ?? null,
                'status' => 1,
            ]);
        } else {
            $listaDatas = Utils::exibeDatasEmUmIntervalo($data['periodo_inicio'], $data['periodo_fim']);
        
            foreach ($listaDatas as $key => $value) {
                $ocorrencia = OcorrenciasPendente::create([
                    'created_at' => $value . ' 00:00:00',
                    'updated_at' => $value . ' 00:00:00',
                    'empresas_id' => $data['empresas_id'],
                    'users_id' => $data['users_id'],
                    'ocorrencias_justificadas_id' => $data['ocorrencias_justificadas_id'],
                    'descricao' => $data['descricao'] ?? null,
                    'dia_ou_datahora' => $data['dia_ou_datahora'],
                    'periodo_inicio' => $value,
                    'periodo_fim' => $data['periodo_fim'],
                    'aprovar_ou_reprovar' => $data['aprovar_ou_reprovar'] ?? null,
                    'status' => 1,
                ]);
            }
            RegistroMarcacaoPontos::where('users_id', $data['users_id'])
                ->whereBetween(DB::raw('DATE(created_at)'), array(
                        explode(' ', $data['periodo_inicio'])[0], explode(' ', $data['periodo_fim'])[1])
                    )
                ->update(
                [
                    'ocorrencias_pendentes_id' => $ocorrencia->id
                ]
            );
        }


        return response()->json($data, 201);
    }

    public function searchInconsistencia ($search) {
        $registroponto = RegistroMarcacaoPontos::selectRaw(
            'users.id as id,
            users.name as name,
            users.matricula as matricula,
            setores.name as setor,
            cargos.name as cargo,
            empresas.name as empresas_name,
            (
                SELECT  COUNT(rmp.dia_semana_string) as qtd FROM registro_marcacao_pontos rmp 
                   where rmp.users_id = users.id
                GROUP BY rmp.dia_semana_string
                HAVING qtd < 4
                limit 1
            ) as inconsistencia,
            (
                SELECT  COUNT(rmpp.permissao) as qtd FROM registro_marcacao_pontos rmpp 
                where rmpp.users_id = users.id 
                AND rmpp.permissao <> 1
                GROUP BY rmpp.users_id
                limit 1
            ) as inconsistena_permissao'
        )
        ->join('users', 'users.id', 'registro_marcacao_pontos.users_id')
        ->join('users_colaboradores', 'users_colaboradores.users_id', 'users.id')
        ->join('setores', 'setores.id', 'users_colaboradores.setores_id')
        ->join('cargos', 'cargos.id', 'users_colaboradores.cargos_id')
        ->join('empresas', 'empresas.id', 'registro_marcacao_pontos.empresas_id')
        ->groupBy('users.id')
        ->whereRaw('(
                SELECT  COUNT(rmp.dia_semana_string) as qtd FROM registro_marcacao_pontos rmp 
                where rmp.users_id = users.id
                GROUP BY rmp.dia_semana_string
                HAVING qtd < 4
            ) > 0 OR (
                SELECT  COUNT(rmpp.permissao) as qtd FROM registro_marcacao_pontos rmpp 
                where rmpp.users_id = users.id 
                AND rmpp.permissao <> 1
                GROUP BY rmpp.users_id
                limit 1
            ) > 0')
        ->paginate(25);

        return $registroponto;
    }

    public function searchSemInconsistencia ($search) {
        $registroponto = RegistroMarcacaoPontos::selectRaw(
            'users.id as id,
            users.name as name,
            users.matricula as matricula,
            setores.name as setor,
            cargos.name as cargo,
            empresas.name as empresas_name,
            (
                SELECT  COUNT(rmp.dia_semana_string) as qtd FROM registro_marcacao_pontos rmp 
                   where rmp.users_id = users.id
                GROUP BY rmp.dia_semana_string
                HAVING qtd < 4
            ) as inconsistencia,
            (
                SELECT  COUNT(rmpp.permissao) as qtd FROM registro_marcacao_pontos rmpp 
                where rmpp.users_id = users.id 
                AND rmpp.permissao <> 1
                GROUP BY rmpp.users_id
                limit 1
            ) as inconsistena_permissao'
        )
        ->join('users', 'users.id', 'registro_marcacao_pontos.users_id')
        ->join('users_colaboradores', 'users_colaboradores.users_id', 'users.id')
        ->join('setores', 'setores.id', 'users_colaboradores.setores_id')
        ->join('cargos', 'cargos.id', 'users_colaboradores.cargos_id')
        ->join('empresas', 'empresas.id', 'registro_marcacao_pontos.empresas_id')
        ->where(function($query) use ($search){
            $query->where('users.name', 'LIKE', "%$search%")
                ->orWhere('users.matricula','LIKE', "%$search%")
                ->orWhere('setores.name','LIKE', "%$search%")
                ->orWhere('empresas.name','LIKE', "%$search%")
                ->orWhere('cargos.name','LIKE', "%$search%");
        })
        ->groupBy('users.id')
        ->paginate(25);

        return $registroponto;
    }
}
