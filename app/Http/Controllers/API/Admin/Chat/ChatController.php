<?php

namespace App\Http\Controllers\API\Admin\Chat;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Comum\Chat;
use Illuminate\Http\Request;
use App\Models\Admin\Log;
use App\Models\Comum\Chatmessage;
use App\User;

class ChatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $chat = Chat::selectRaw(
                'users.name as name,
                setores.name as setor,
                cargos.name as cargo,
                chats.id as id,
                empresas.name as empresas_name'
            )
            ->join('chatmessages', 'chatmessages.chats_id', 'chats.id')
            ->join('users', 'users.id', 'chats.users_id')
            ->join('users_colaboradores', 'users_colaboradores.users_id', 'users.id')
            ->join('setores', 'setores.id', 'users_colaboradores.setores_id')
            ->join('cargos', 'cargos.id', 'users_colaboradores.cargos_id')
            ->join('empresas', 'empresas.id', 'chatmessages.empresas_id')
            ->where('chats.status', 1)
            ->where('chats.chamado', 'aberto')
            ->where(function($query) use ($search){
                $query->where('chats.chamado', 1)
                      ->orWhere('users.name','LIKE', "%$search%")
                      ->orWhere('setores.name','LIKE', "%$search%")
                      ->orWhere('cargos.name','LIKE', "%$search%")
                      ->orWhere('empresas.name','LIKE', "%$search%");
            })
            ->groupBy('chats.id')->paginate(20);
        }else{
            $chat = $this->index();
        }

        return $chat;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chat = Chat::selectRaw(
                'users.name as name,
                setores.name as setor,
                cargos.name as cargo,
                chats.id as id,
                empresas.name as empresas_name'
            )
            ->join('chatmessages', 'chatmessages.chats_id', 'chats.id')
            ->join('users', 'users.id', 'chats.users_id')
            ->join('users_colaboradores', 'users_colaboradores.users_id', 'users.id')
            ->join('setores', 'setores.id', 'users_colaboradores.setores_id')
            ->join('cargos', 'cargos.id', 'users_colaboradores.cargos_id')
            ->join('empresas', 'empresas.id', 'chatmessages.empresas_id')
            ->where('chats.status', 1)
            ->where('chats.chamado', 'aberto')
            ->groupBy('chats.id')
            ->paginate(20);

        return $chat;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;
        $data['users_id'] = auth('api')->user()->id;

        $chat = Chat::where('id', $data['id'])->first(); 
        
        $chat->update(['chamado' => 'fechado']);

        $chatmessage = Chatmessage::create([
            'empresas_id' => $data['empresas_id'],
            // 'users_id' => $data['users_id'],
            'chats_id'=> $data['id'],
            'mensagem' => $data['mensagem'],
            'colaborador_ou_suporte'=> 'suporte',
            'quem_respondeu' => $data['users_id']
        ]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Chat::class,
            'tabela_id' => $data['id'],
            'atividade' => 'cadastrou'
        ]);

        // broadcast(new \App\Events\SendMessage($chatmessage, User::findOrFail($chat->users_id)));
        
        return response()->json($chatmessage, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chat = Chat::select(
            'chats.id',
            'chats.users_id',
            'chatmessages.mensagem',
            'chatmessages.colaborador_ou_suporte',
            'chatmessages.created_at'
        )
        ->join('chatmessages', 'chatmessages.chats_id', 'chats.id')
        ->where('chats.id', $id)
        ->where('chats.status', 1)
        ->get();

        return [
            'chat' => $chat,
            'nameColaborador' => User::where('id', $chat[0]->users_id)->first()['name']
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $chat = Chat::findOrFail($id);
        $chat->update($request->all());

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Chat::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($chat, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Chat::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => Chat::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
