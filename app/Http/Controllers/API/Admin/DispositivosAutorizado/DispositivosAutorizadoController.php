<?php

namespace App\Http\Controllers\API\Admin\DispositivosAutorizado;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Admin\DispositivosAutorizado;
use Illuminate\Http\Request;
use App\Models\Admin\Log;

class DispositivosAutorizadoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $dispositivosautorizado = DispositivosAutorizado::select(
                'users.name as user_name',
                'empresas.name as empresa_name',
                'dispositivos_autorizados.id',
                'dispositivos_autorizados.finger_print',
                'dispositivos_autorizados.sistema_operacional',
                'dispositivos_autorizados.sistema_operacional_versao',
                'dispositivos_autorizados.dispositivo_para_acesso',
                'dispositivos_autorizados.navegador',
                'dispositivos_autorizados.created_at',
                'dispositivos_autorizados.status'
            )->join('users', 'users.id', 'dispositivos_autorizados.users_id')
            ->join('empresas', 'empresas.id', 'dispositivos_autorizados.empresas_id')
            ->where(function($query) use ($search){
                $query
                    ->orWhere('users.name','LIKE', '%' . $search. '%')
                    ->orWhere('empresas.name','LIKE', '%' . $search. '%')
                    ->orWhere('dispositivos_autorizados.dispositivo_para_acesso','LIKE', '%' . $search. '%');
            })->paginate(20);
        }else{
            $dispositivosautorizado = $this->index();
        }

        return $dispositivosautorizado;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dispositivosautorizado = DispositivosAutorizado::select(
                'users.name as user_name',
                'empresas.name as empresa_name',
                'dispositivos_autorizados.id',
                'dispositivos_autorizados.finger_print',
                'dispositivos_autorizados.sistema_operacional',
                'dispositivos_autorizados.sistema_operacional_versao',
                'dispositivos_autorizados.dispositivo_para_acesso',
                'dispositivos_autorizados.navegador',
                'dispositivos_autorizados.created_at',
                'dispositivos_autorizados.status'
            )
            ->join('users', 'users.id', 'dispositivos_autorizados.users_id')
            ->join('empresas', 'empresas.id', 'dispositivos_autorizados.empresas_id')
            ->orderBy('dispositivos_autorizados.id', 'DESC')
            ->paginate(20);

        return $dispositivosautorizado;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userAuth = auth('api')->user();
        $data = $request->all();
        $data['empresas_id'] = $userAuth->empresas_id;
        $data['users_id'] = $userAuth->id;
        
        $data['status'] = false;

        $verificaDispositivo = DispositivosAutorizado::where('users_id', $data['users_id'])
            ->where('dispositivo_para_acesso', 'computador')
            ->where('sistema_operacional', $data['sistema_operacional'])
            ->where('navegador', $data['navegador'])
            ->where('finger_print', $data['finger_print'])
            ->first();

        if($verificaDispositivo)
            return false;

        $dispositivosautorizado = DispositivosAutorizado::create($data);

        if ($dispositivosautorizado) {
            session(['permissoes_dispositivos' => ['permite_dispositivo' => 'AGUARDANDO', 'user_dispositivo' => $dispositivosautorizado]]);
        }

        Log::create([
            'empresas_id' => $userAuth->empresas_id,
            'users_id' => $userAuth->id,
            'tabela' => DispositivosAutorizado::class,
            'tabela_id' => $dispositivosautorizado->id,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($dispositivosautorizado, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userAuth = auth('api')->user();
        
        $dispositivosautorizado = DispositivosAutorizado::where('users_id', $userAuth->id)
            ->where('dispositivo_para_acesso', 'computador')
            ->get();

        return $dispositivosautorizado;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dispositivosautorizado = DispositivosAutorizado::findOrFail($id);
        $dispositivosautorizado->update($request->all());

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => DispositivosAutorizado::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($dispositivosautorizado, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DispositivosAutorizado::where('id', $id)->update(['deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => DispositivosAutorizado::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(true, 200);
    }

    public function alteraStatus($id, Request $request)
    {
        $disp = DispositivosAutorizado::where('id', $id)->first();

        $status = $disp->status == 1 ? 0 : 1;

        $disp->update(['status' => $status]);

        return response()->json($disp, 204);
    }
}
