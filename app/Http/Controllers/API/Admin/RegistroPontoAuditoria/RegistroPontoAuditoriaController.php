<?php

namespace App\Http\Controllers\API\Admin\RegistroPontoAuditoria;

use App\Http\Controllers\Controller;
use App\Models\Admin\Log;
use App\Models\Comum\RegistroMarcacaoPontosAuditoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log as FacadesLog;

class RegistroPontoAuditoriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function search() {
        if ($search = \Request::get('q')) {
            $marcacoesPendentes = RegistroMarcacaoPontosAuditoria::selectRaw(
                'registro_marcacao_pontos_auditoria.*,
                users.name,
                empresas.name as empresas_name'
            )
            ->join('users', 'users.id', 'registro_marcacao_pontos_auditoria.users_id')
            ->join('empresas', 'empresas.id', 'registro_marcacao_pontos_auditoria.empresas_id')
                ->where(function($query) use ($search){
                    $query->where('users.name', 'LIKE', "%$search%")
                    ->orWhere('registro_marcacao_pontos_auditoria.created_at', 'LIKE', "%$search%")
                    ->orWhere('empresas.name', 'LIKE', "%$search%");
                });
                
            if (\Request::get('pdf') == 'pdf') {
                FacadesLog::info($marcacoesPendentes->toSql());
                $marcacoesPendentes = $marcacoesPendentes->get();
            } else {
                FacadesLog::info('else');
                $marcacoesPendentes= $marcacoesPendentes->paginate(20);
            }
            
        }else{
            $marcacoesPendentes = $this->index();
        }

        return $marcacoesPendentes;
    }

    public function index() {
        return RegistroMarcacaoPontosAuditoria::selectRaw(
            'registro_marcacao_pontos_auditoria.*,
            users.name,
            empresas.name as empresas_name'
        )
        ->join('users', 'users.id', 'registro_marcacao_pontos_auditoria.users_id')
        ->join('empresas', 'empresas.id', 'registro_marcacao_pontos_auditoria.empresas_id')
        #->where('registro_marcacao_pontos_auditoria.empresas_id', auth('api')->user()->empresas_id)
        #->where('registro_marcacao_pontos_auditoria.permissao', 0)
        #->where('registro_marcacao_pontos_auditoria.status', 1)
        ->paginate(20);
    }

    public function modificaPermissao(Request $request) {
        RegistroMarcacaoPontosAuditoria::where('id', $request->id)
                ->update([
                    'permissao' => $request->permissao
                ]);
    }

    public function update(Request $request, $id) {
        $registroponto = RegistroMarcacaoPontosAuditoria::findOrFail($id);
        $timestamp = strtotime($registroponto->created_at);

        $registroponto->update([
            // 'created_at' => strtotime(date('Y-m-d', $timestamp) . ' ' .  str_pad($request->hora_ponto, 5, '0',  STR_PAD_LEFT)),
            'justificativa_empresa' => $request->justificativa_empresa,
            'observacao_empresa' => $request->observacao_empresa,
            // 'hora_ponto' => str_pad($request->hora_ponto, 5, '0',  STR_PAD_LEFT),
            'permissao' => $request->permissao
        ]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => RegistroMarcacaoPontosAuditoria::class,
            'tabela_id' => $id,
            'atividade' => 'Solicitou ajuste de ponto'
        ]);

        return response()->json($this->index(), 200);
    }
}
