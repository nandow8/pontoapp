<?php

namespace App\Http\Controllers\API\Admin\CercasVirtuais;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Admin\CercasVirtuai;
use Illuminate\Http\Request;
use App\Models\Admin\Log;

class CercasVirtuaisController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $cercasvirtuai = CercasVirtuai::select(
                'cercas_virtuais.id',
                'cercas_virtuais.empresas_id',
                'cercas_virtuais.idunico',
                'cercas_virtuais.descricao',
                'cercas_virtuais.lat',
                'cercas_virtuais.lng',
                'cercas_virtuais.radius',
                'empresas.name as empresas_name'
            )
            ->leftJoin('empresas', 'empresas.id', 'cercas_virtuais.empresas_id')
            ->where(function($query) use ($search){
                $query->where('cercas_virtuais.descricao','LIKE', "%$search%")
                      ->orWhere('empresas.name','LIKE', "%$search%");
            })->where('cercas_virtuais.status', 1)->paginate(20);
        }else{
            $cercasvirtuai = CercasVirtuai::where('status', 1)->latest()->paginate(20);
        }

        return $cercasvirtuai;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cercasvirtuais = CercasVirtuai::select(
                                'cercas_virtuais.id',
                                'cercas_virtuais.empresas_id',
                                'cercas_virtuais.idunico',
                                'cercas_virtuais.descricao',
                                'cercas_virtuais.lat',
                                'cercas_virtuais.lng',
                                'cercas_virtuais.radius',
                                'empresas.name as empresas_name'
                            )
                            ->leftJoin('empresas', 'empresas.id', 'cercas_virtuais.empresas_id')
                            ->where('cercas_virtuais.status', 1)
                            ->groupBy('cercas_virtuais.idunico')
                            ->orderBy('cercas_virtuais.id', 'desc')
                            ->paginate(20);

        return $cercasvirtuais;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;

        $idUnico = $data['empresas_id'] . rand(1, 9999);

        foreach ($data['coordenadas'] as $value) {
            CercasVirtuai::create([
                'idunico' => $idUnico,
                'empresas_id' => auth('api')->user()->empresas_id,
                'descricao' => $value['descricao'],
                'lat' => $value['lat'],
                'lng' => $value['lng'],
                'radius' => $value['radius'],
            ]);
        }

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => CercasVirtuai::class,
            'tabela_id' => $idUnico,
            'atividade' => 'cadastrou'
        ]);

        return response()->json($idUnico, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cercasvirtuai = CercasVirtuai::where('idunico', $id)
            ->where('status', 1)
            ->get();

        return $cercasvirtuai;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        foreach ($request->coordenadas as $value) {
            CercasVirtuai::where('id', $value['id'])
                ->update([
                    'descricao' => $value['descricao'],
                    'lat' => $value['lat'],
                    'lng' => $value['lng'],
                    'radius' => $value['radius'],
                ]);
        }

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => CercasVirtuai::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($request->coordenadas, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CercasVirtuai::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => CercasVirtuai::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }
}
