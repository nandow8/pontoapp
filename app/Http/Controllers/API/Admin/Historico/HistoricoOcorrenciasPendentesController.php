<?php

namespace App\Http\Controllers\API\Admin\Historico;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Admin\OcorrenciasPendente;
use Illuminate\Http\Request;
use App\Models\Admin\Log;

class HistoricoOcorrenciasPendentesController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:api');
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $ocorrenciaspendente = OcorrenciasPendente::selectRaw(
                'ocorrencias_pendentes.*,
                ocorrencias_justificadas.descricao,
                users.name,
                empresas.name as empresas_name'
            )
            ->join('users', 'users.id', 'ocorrencias_pendentes.users_id')
            ->join('ocorrencias_justificadas', 'ocorrencias_justificadas.id', 'ocorrencias_pendentes.ocorrencias_justificadas_id')
            ->join('empresas', 'empresas.id', 'ocorrencias_pendentes.empresas_id')
            ->where(function($query) use ($search){
                $query->where('ocorrencias_justificadas.descricao','LIKE', "%$search%")
                    ->orWhere('users.name', 'LIKE', "%$search%")
                    ->orWhere('empresas.name', 'LIKE', "%$search%");
            })->paginate(20);
        }else{
            $ocorrenciaspendente = $this->index();
        }

        return $ocorrenciaspendente;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ocorrenciaspendentes = OcorrenciasPendente::selectRaw(
                'ocorrencias_pendentes.*,
                ocorrencias_justificadas.descricao,
                users.name,
                empresas.name as empresas_name'
            )
            ->join('users', 'users.id', 'ocorrencias_pendentes.users_id')
            ->join('ocorrencias_justificadas', 'ocorrencias_justificadas.id', 'ocorrencias_pendentes.ocorrencias_justificadas_id')
            ->join('empresas', 'empresas.id', 'ocorrencias_pendentes.empresas_id')
            // ->whereNull('ocorrencias_pendentes.aprovar_ou_reprovar')
            // ->where('ocorrencias_pendentes.status', 1)
            ->orderBy('ocorrencias_pendentes.id', 'DESC')
            ->paginate(20);

        return $ocorrenciaspendentes;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['empresas_id'] = auth('api')->user()->empresas_id;
        $data['users_id'] = auth('api')->user()->id;

        $ocorrenciaspendente = OcorrenciasPendente::create($data);             

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => OcorrenciasPendente::class,
            'tabela_id' => $ocorrenciaspendente->id ?? OcorrenciasPendente::where('empresas_id', $data['empresas_id'])->orderBy('id', 'desc')->first()['id'],
            'atividade' => 'cadastrou'
        ]);

        return response()->json($ocorrenciaspendente, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ocorrenciaspendente = OcorrenciasPendente::findOrFail($id);

        return $ocorrenciaspendente;
    }

    public function showByUserId($id) {
        $ocorrencia = OcorrenciasPendente::selectRaw(
                'ocorrencias_pendentes.*,
                ocorrencias_justificadas.descricao'
            )
            ->leftJoin('users', 'users.id', '=', 'ocorrencias_pendentes.users_id')
            ->leftJoin('ocorrencias_justificadas', 'ocorrencias_justificadas.id', '=', 'ocorrencias_pendentes.ocorrencias_justificadas_id')
            ->where('users.id', $id)
            ->where('users.empresas_id',  auth('api')->user()->empresas_id)
            ->whereYear('ocorrencias_pendentes.created_at', date('Y'))
            ->whereMonth('ocorrencias_pendentes.created_at', date('m'))
            ->whereDay('ocorrencias_pendentes.created_at', date('d'))
            ->get();

        return $ocorrencia;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $ocorrenciaspendente = OcorrenciasPendente::findOrFail($id);
        $ocorrenciaspendente->update($request->all());

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => OcorrenciasPendente::class,
            'tabela_id' => $id,
            'atividade' => 'editou'
        ]);

        return response()->json($ocorrenciaspendente, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OcorrenciasPendente::where('id', $id)->update(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => OcorrenciasPendente::class,
            'tabela_id' => $id,
            'atividade' => 'removeu'
        ]);

        return response()->json(null, 204);
    }

    public function aprovarOUreprovar(Request $request) {
        OcorrenciasPendente::where('id', $request->id)
            ->update([
                'aprovar_ou_reprovar' => $request->aprovaOUreprova
            ]);
        return response()->json(true, 200);
    }
}
