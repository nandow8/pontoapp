<?php

namespace App\Http\Controllers\Api\Admin\Historico;

use App\Http\Controllers\Controller;
use App\Models\Admin\Log;
use Illuminate\Http\Request;
use App\Models\Comum\RegistroMarcacaoPontos;

class HistoricoMarcacoesPendentesController extends Controller
{
    public function search() {
        if ($search = \Request::get('q')) {
            $marcacoesPendentes = RegistroMarcacaoPontos::selectRaw(
                'registro_marcacao_pontos.*,
                users.name,
                empresas.name as empresas_name'
            )
            ->join('users', 'users.id', 'registro_marcacao_pontos.users_id')
            ->join('empresas', 'empresas.id', 'registro_marcacao_pontos.empresas_id')
                ->where(function($query) use ($search){
                    $query->where('registro_marcacao_pontos.created_at', 'LIKE', "%$search%")
                    ->orWhere('users.name', 'LIKE', "%$search%")
                    ->orWhere('empresas.name', 'LIKE', "%$search%");
                })
                // ->where('registro_marcacao_pontos.permissao', 0)
                // ->where('registro_marcacao_pontos.status', 1)
                ->paginate(20);
        }else{
            $marcacoesPendentes = $this->index();
        }

        return $marcacoesPendentes;
    }

    public function index() {
        return RegistroMarcacaoPontos::selectRaw(
            'registro_marcacao_pontos.*,
            users.name,
            empresas.name as empresas_name'
        )
        ->join('users', 'users.id', 'registro_marcacao_pontos.users_id')
        ->join('empresas', 'empresas.id', 'registro_marcacao_pontos.empresas_id')
        // ->where('registro_marcacao_pontos.permissao', 0)
        // ->where('registro_marcacao_pontos.status', 1)
        ->paginate(20);
    }

    public function modificaPermissao(Request $request) {
        RegistroMarcacaoPontos::where('id', $request->id)
                ->update([
                    'permissao' => $request->permissao
                ]);
    }

    public function update(Request $request, $id) {
        $registroponto = RegistroMarcacaoPontos::findOrFail($id);
        $timestamp = strtotime($registroponto->created_at);

        $registroponto->update([
            // 'created_at' => strtotime(date('Y-m-d', $timestamp) . ' ' .  str_pad($request->hora_ponto, 5, '0',  STR_PAD_LEFT)),
            'justificativa_empresa' => $request->justificativa_empresa,
            'observacao_empresa' => $request->observacao_empresa,
            // 'hora_ponto' => str_pad($request->hora_ponto, 5, '0',  STR_PAD_LEFT),
            'permissao' => $request->permissao
        ]);

        Log::create([
            'empresas_id' => auth('api')->user()->empresas_id,
            'users_id' => auth('api')->user()->id,
            'tabela' => RegistroMarcacaoPontos::class,
            'tabela_id' => $id,
            'atividade' => 'Solicitou ajuste de ponto'
        ]);

        return response()->json($this->index(), 200);
    }
}
