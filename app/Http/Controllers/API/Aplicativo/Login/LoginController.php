<?php

namespace App\Http\Controllers\API\Aplicativo\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Illuminate\Support\Str;
use App\Models\Comum\Escala;

class LoginController extends Controller
{
    public function search() {

    }
    
    public function index(Request $request)
    { 
        $search = $request->all();

        $user = User::where('email', $search['email']) 
                ->orWhere('matricula', $search['email'])
                ->where('status', 1)->first();
        
        if (Hash::check($search['password'], $user->password)) {
            $apikey = base64_encode(Str::random(40));
            User::where('email', $search['email'])->update(['token' => "$apikey"]);
            // $users = User::select('id', 'name', 'email')->where('email',$search['email'])->get();
            return response()->json(
                [
                    'token' => $apikey,
                    'user' => $user
                ]
            );
        } else {
            return response()->json(['status' => 'fail'], 401);
        }
    }

    public function store(Request $request)
    {
        return Escala::all();
        $search = $request->all();

        $user = User::where('email', $search['email']) 
                ->orWhere('matricula', $search['email'])
                ->where('status', 1)->first();
        
        if (Hash::check($search['password'], $user->password)) {
            $apikey = base64_encode(Str::random(40));
            User::where('email', $search['email'])->update(['token' => "$apikey"]);
            // $users = User::select('id', 'name', 'email')->where('email',$search['email'])->get();
            return response()->json(
                [
                    'token' => $apikey,
                    'user' => $user
                ]
            );
        } else {
            return response()->json(['status' => 'fail'], 401);
        }

        // $data = $request->all();
        
        // return User::create([
        //     'name'     => $data['name'],
        //     'email'    => $data['email'],
        //     'password' => app('hash')->make($data['password']),
        //     'token'  => base64_encode(Str::random(40))
        // ]);
    }
}
