<?php

namespace App\Models\Admin;
use App\Models\BaseModel;

class PermissoesUsuarios extends BaseModel
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissoes_usuarios';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    
}