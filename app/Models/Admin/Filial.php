<?php

namespace App\Models\Admin;
use App\Models\BaseModel;
use App\Scopes\VisibleScope;

class Filial extends BaseModel
{    
    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new VisibleScope);
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'filiais';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function empresas_endereco()
    {
        return $this->hasOne('App\Models\Admin\EmpresasEndereco', 'filiais_id')->where('status', 1);
    }
    
    public function empresas_setores()
    {
        return $this->hasMany('App\Models\Admin\EmpresasManySetores', 'empresas_id', 'id')
            ->where('status', 1)
            ->where('filial', 1);
    }
        
}