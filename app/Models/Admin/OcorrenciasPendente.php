<?php

namespace App\Models\Admin;
use App\Models\BaseModel;
use App\Scopes\VisibleScope;

class OcorrenciasPendente extends BaseModel
{
    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new VisibleScope);
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ocorrencias_pendentes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    
}