<?php

namespace App\Models\Admin;
use App\Models\BaseModel;
use App\Scopes\VisibleScope;

class Log extends BaseModel
{
    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new VisibleScope);
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'logs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function users()
    {
        return $this->hasOne('App\User', 'users_id', 'id');
    }
    
}