<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use App\Scopes\VisibleScope;

class Afdafdt extends BaseModel
{   
    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new VisibleScope);
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'afd_afdt';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    
}