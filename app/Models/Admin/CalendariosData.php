<?php

namespace App\Models\Admin;
use App\Models\BaseModel;
use App\Scopes\VisibleScope;
use Illuminate\Database\Eloquent\SoftDeletes;

class CalendariosData extends BaseModel
{
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new VisibleScope);
    }
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'calendarios_datas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    
}