<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use App\Scopes\VisibleScope;

class EmpresasManySetores extends BaseModel
{    
    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new VisibleScope);
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'empresas_many_setores';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function empresas()
    {
        return $this->hasOne('App\Models\Admin\Empresas', 'empresas_id');
    }
    
    public function setores()
    {
        return $this->belongsTo('App\Models\Admin\Setores', 'setores_id', 'id');
    }
    
}