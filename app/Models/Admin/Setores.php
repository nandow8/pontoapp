<?php

namespace App\Models\Admin;
use App\Models\BaseModel;
use App\Scopes\VisibleScope;

class Setores extends BaseModel
{
    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new VisibleScope);
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'setores';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    
}