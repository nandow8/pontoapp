<?php

namespace App\Models\Admin;
use App\Models\BaseModel;

class Permissoes extends BaseModel
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissoes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function filhasQuemPertence(){
        return $this->hasMany('App\Models\Admin\Permissoes', 'avo', 'quem_pertence');
	}
	
	public function netosQuemPertence(){
        return $this->hasMany('App\Models\Admin\Permissoes', 'pais', 'quem_pertence');
    }

    public function checarPermissaoByUser($idPermissao, $idusuario){
        $temPermissao = PermissoesUsuarios::where('idusuario', $idusuario)->where('idpermissao', $idPermissao)->first();

        if($temPermissao) {
            return true;
        }
        
        return false;
    }

    
    const ADMINISTRATIVO_VISUALIZAR = 1;
    
    const CLIENTES_VISUALIZAR = 2;
    const CLIENTES_ADICIONAR = 3;
    const CLIENTES_EDITAR = 4;
    const CLIENTES_DELETAR = 5;
    
    const USUÁRIOS_VISUALIZAR = 6;
    const USUÁRIOS_ADICIONAR = 7;
    const USUÁRIOS_EDITAR = 8;
    const USUÁRIOS_DELETAR = 9;
    
    const UNIDADES_VISUALIZAR = 10;
    
    const FILIAIS_VISUALIZAR = 11;
    const FILIAIS_ADICIONAR = 12;
    const FILIAIS_EDITAR = 13;
    const FILIAIS_DELETAR = 14;
    
    const CONFIGURACOES_VISUALIZAR = 15;
    
    const GERAIS_VISUALIZAR = 16;
    const GERAIS_ADICIONAR = 17;
    const GERAIS_EDITAR = 18;
    const GERAIS_DELETAR = 19;
    
    const CERCASVIRTUAIS_VISUALIZAR = 20;
    const CERCASVIRTUAIS_ADICIONAR = 21;
    const CERCASVIRTUAIS_EDITAR = 22;
    const CERCASVIRTUAIS_DELETAR = 23;
    
    const DISPOSITIVOSAUTORIZADOS_VISUALIZAR = 24;
    const DISPOSITIVOSAUTORIZADOS_ADICIONAR = 25;
    const DISPOSITIVOSAUTORIZADOS_EDITAR = 26;
    const DISPOSITIVOSAUTORIZADOS_DELETAR = 27;
    
    const PERFIS_VISUALIZAR = 28;
    const PERFIS_ADICIONAR = 29;
    const PERFIS_EDITAR = 30;
    const PERFIS_DELETAR = 31;
    
    const CARGOS_VISUALIZAR = 32;
    const CARGOS_ADICIONAR = 33;
    const CARGOS_EDITAR = 34;
    const CARGOS_DELETAR = 35;
    
    const SETORES_VISUALIZAR = 36;
    const SETORES_ADICIONAR = 37;
    const SETORES_EDITAR = 38;
    const SETORES_DELETAR = 39;
    
    const COLABORADORES_VISUALIZAR = 40;
    const COLABORADORES_ADICIONAR = 41;
    const COLABORADORES_EDITAR = 42;
    const COLABORADORES_DELETAR = 43;
    
    const CALENDARIOSDEFERIADOS_VISUALIZAR = 44;
    const CALENDARIOSDEFERIADOS_ADICIONAR = 45;
    const CALENDARIOSDEFERIADOS_EDITAR = 46;
    const CALENDARIOSDEFERIADOS_DELETAR = 47;
    
    const ESCALAS_VISUALIZAR = 48;
    const ESCALAS_ADICIONAR = 49;
    const ESCALAS_EDITAR = 50;
    const ESCALAS_DELETAR = 51;
    
    const CONTROLEDEPONTO_VISUALIZAR = 52;
    
    const PONTO_VISUALIZAR = 53;
    const PONTO_ADICIONAR = 54;
    const PONTO_EDITAR = 55;
    const PONTO_DELETAR = 56;
    
    const CONTROLEDEHORAS_VISUALIZAR = 57;
    const CONTROLEDEHORAS_ADICIONAR = 58;
    const CONTROLEDEHORAS_EDITAR = 59;
    const CONTROLEDEHORAS_DELETAR = 60;
    
    const FECHAMENTO_VISUALIZAR = 61;
    const FECHAMENTO_ADICIONAR = 62;
    const FECHAMENTO_EDITAR = 63;
    const FECHAMENTO_DELETAR = 64;
    
    const SOLICITACOES_VISUALIZAR = 65;
    
    const OCORRESNCIASPENDENTES_VISUALIZAR = 66;
    const OCORRESNCIASPENDENTES_ADICIONAR = 67;
    const OCORRESNCIASPENDENTES_EDITAR = 68;
    const OCORRESNCIASPENDENTES_DELETAR = 69;
    
    const MARCACOESPENDENTES_VISUALIZAR = 70;
    const MARCACOESPENDENTES_ADICIONAR = 71;
    const MARCACOESPENDENTES_EDITAR = 72;
    const MARCACOESPENDENTES_DELETAR = 73;
    
    const HISTORICO_VISUALIZAR = 74;
    const HISTORICO_ADICIONAR = 75;
    const HISTORICO_EDITAR = 76;
    const HISTORICO_DELETAR = 77;

    const MARCACOESPONTOSAUDITORIA_VISUALIZAR = 116;
    const MARCACOESPONTOSAUDITORIA_ADICIONAR = 117;
    const MARCACOESPONTOSAUDITORIA_EDITAR = 118;
    const MARCACOESPONTOSAUDITORIA_DELETAR = 119;
    
    const OCORRENCIASJUSTIFICADAS_VISUALIZAR = 78;
    const OCORRENCIASJUSTIFICADAS_ADICIONAR = 79;
    const OCORRENCIASJUSTIFICADAS_EDITAR = 80;
    const OCORRENCIASJUSTIFICADAS_DELETAR = 81;
    
    const RELATORIOS_VISUALIZAR = 82;
    const RELATORIOS_ADICIONAR = 83;
    const RELATORIOS_EDITAR = 84;
    const RELATORIOS_DELETAR = 85;
    
    const CHAT_VISUALIZAR = 86;
    const CHAT_ADICIONAR = 87;
    const CHAT_EDITAR = 88;
    const CHAT_DELETAR = 89;
    
    const LOGS_VISUALIZAR = 90;
    const LOGS_ADICIONAR = 91;
    const LOGS_EDITAR = 92;
    const LOGS_DELETAR = 93;

    const REGISTRODEPONTO_VISUALIZAR = 94;

    const COLABORADORREGISTRODEPONTO_VISUALIZAR = 95;
    const COLABORADORREGISTRODEPONTO_ADICIONAR = 96;
    const COLABORADORREGISTRODEPONTO_EDITAR = 97;
    const COLABORADORREGISTRODEPONTO_DELETAR = 98;

    const COLABORADORHISTORICOS_VISUALIZAR = 99;
    
    const COLABORADORPONTOS_VISUALIZAR = 100;
    const COLABORADORPONTOS_ADICIONAR = 101;
    const COLABORADORPONTOS_EDITAR = 102;
    const COLABORADORPONTOS_DELETAR = 103;
    
    const COLABORADORBANCODEHORAS_VISUALIZAR = 104;
    const COLABORADORBANCODEHORAS_ADICIONAR = 105;
    const COLABORADORBANCODEHORAS_EDITAR = 106;
    const COLABORADORBANCODEHORAS_DELETAR = 107;

    const COLABORADORCHAT_VISUALIZAR = 108;
    const COLABORADORCHAT_ADICIONAR = 109;
    const COLABORADORCHAT_EDITAR = 110;
    const COLABORADORCHAT_DELETAR = 111;

    const COLABORADODISPOSITIVO_VISUALIZAR = 120;
}