<?php

namespace App\Models\Comum;
use App\Models\BaseModel;
use App\Scopes\VisibleScope;

class Escala extends BaseModel
{
    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new VisibleScope);
    }
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'escalas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function intervalos()
    {
        return $this->hasMany('App\Models\Comum\Intervalo', 'escalas_id', 'id')
            ->where('status', 1);
    }

    public function jornada()
    {
        return $this->belongsTo('App\Models\Comum\Jornada', 'dia_jornada', 'id');
    }
}