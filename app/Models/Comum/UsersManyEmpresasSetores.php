<?php

namespace App\Models\Comum;
use App\Models\BaseModel;
use App\Scopes\VisibleScope;

class UsersManyEmpresasSetores extends BaseModel
{
    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new VisibleScope);
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_many_empresas_setores';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function setores()
    {
        return $this->belongsTo('App\Models\Admin\Setores', 'setores_id', 'id');
    }

    public function filiais()
    {
        return $this->belongsTo('App\Models\Admin\Filial', 'filiais_id', 'id');
    }
}
