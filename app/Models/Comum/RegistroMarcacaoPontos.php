<?php

namespace App\Models\Comum;
use App\Models\BaseModel;
use App\Scopes\VisibleScope;

class RegistroMarcacaoPontos extends BaseModel
{
    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new VisibleScope);
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'registro_marcacao_pontos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    
}