<?php

namespace App\Models\Comum;
use App\Models\BaseModel;
use App\Scopes\VisibleScope;

class UsersColaboradores extends BaseModel
{    
    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new VisibleScope);
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_colaboradores';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    
}