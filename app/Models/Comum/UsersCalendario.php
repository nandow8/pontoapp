<?php

namespace App\Models\Comum;
use App\Models\BaseModel;
use App\Scopes\VisibleScope;

class UsersCalendario extends BaseModel
{    
    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new VisibleScope);
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_calendarios';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function calendarios()
    {
        return $this->belongsTo('App\Models\Admin\Calendario', 'calendarios_id', 'id')->select(['id', 'nome_calendario', 'ano']);
    }
    
}