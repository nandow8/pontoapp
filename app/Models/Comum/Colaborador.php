<?php

namespace App\Models\Comum;
use App\Models\BaseModel;
use App\Scopes\VisibleScope;

class Colaborador extends BaseModel
{
    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new VisibleScope);
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_colaboradores';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function users()
    {
        return $this->belongsTo('App\User', 'users_id', 'id');
    }

    public function users_escalas()
    {
        return $this->hasMany('App\Models\Comum\UsersEscala', 'users_id', 'users_id')
            ->where('status', 1);;
    }
    
    public function users_calendarios()
    {
        return $this->hasMany('App\Models\Comum\UsersCalendario', 'users_id', 'users_id')
            ->where('status', 1);
    }
    
    public function users_ferias()
    {
        return $this->hasMany('App\Models\Comum\UsersFeria', 'users_id', 'users_id')
            ->where('status', 1);
    }
    
    public function users_cercasvirtuais()
    {
        return $this->hasMany('App\Models\Comum\UsersCercasVirtuais', 'users_id', 'users_id')
            ->where('status', 1);
    }

    public function users_enderecos()
    {
        return $this->hasMany('App\Models\Comum\UsersEndereco', 'users_id', 'users_id')
            ->where('status', 1);
    }
    
    public function users_dispositivo()
    {
        return $this->hasMany('App\Models\Admin\DispositivosAutorizado', 'users_id', 'users_id');
    }
}