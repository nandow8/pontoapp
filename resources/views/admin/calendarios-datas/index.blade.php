<template>
    <div>
        <BreadcrumbsComponent principal="Calendariosdatas"/>

        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header">Calendariosdatas</div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <router-link to="/calendarios-datas/create" class="btn btn-success btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar </router-link>

                            <div class="form-inline my-2 my-lg-0 float-right">
                                <div class="input-group">
                                    <input class="form-control" @keyup="searchit" v-model="search" type="search" placeholder="Buscar..." aria-label="Search">
                                    <span class="input-group-append">
                                        <button class="btn btn-info btn-icon" @click="searchit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="table-responsive">
                                <table class="table">
                                    <VclTable v-if="loading" :columns="6" :rows="5"></VclTable>
                                    <tbody v-else>
                                    <tr>
                                        <th>#</th><th>Empresas Id</th><th>Calendarios Id</th><th>Descricao Feriado Ou Recesso</th><th>Ferramentas</th>
                                    </tr>
                                    <tr v-for="item in calendariosdatas.data" :key="item.id">
                                        <td>{{ item.id }}</td>
                                        <td>{{ $item->empresas_id }}</td><td>{{ $item->calendarios_id }}</td><td>{{ $item->descricao_feriado_ou_recesso }}</td>
                                        <td>
                                            <router-link v-bind:to="{ path: '/calendarios-datas/' + item.id }" title="Editar Post"><button class="btn btn-info btn-sm"><i class="fas fa-eye" aria-hidden="true"></i></button></router-link>
                                            <router-link v-bind:to="{ path: '/calendarios-datas/' + item.id + '/edit' }" title="Editar Post"><button class="btn btn-primary btn-sm"><i class="fas fa-edit" aria-hidden="true"></i></button></router-link>

                                            <button v-if="$gate.isAdmin()" class="btn btn-danger btn-sm" @click="destroy(item.id, item.name)"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <pagination :data="calendariosdatas" @pagination-change-page="pagination"></pagination>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </div>
</template>

<script>
    import BreadcrumbsComponent from '../../../components/menus/headers/BreadcrumbsComponent'
    import crud from '@components/crud'
    import { VclTable } from 'vue-content-loading';

    export default {
        components: {
            BreadcrumbsComponent, VclTable
        },

        data(){
            return {
                loading: true,
                editmode: false,
                calendariosdatas : {},
                form: new Form({
                    id: '',
                    <td>{{ $item->empresas_id }}</td><td>{{ $item->calendarios_id }}</td><td>{{ $item->descricao_feriado_ou_recesso }}</td>
                }),
                search: ''
            }
        },

        methods: {
            searchit: _.debounce(() => {
                Fire.$emit('searching');
            },1000),

            pagination(page = 1){
                this.loading = true
                axios.get(`/api/calendarios-datas?page=${page}`)
				.then(response => {
					this.calendariosdatas = response.data;
                    this.loading = false
				});
            },

            destroy(id, name){
                crud.delete(id, this, 'calendarios-datas')
            },

            load(){
                axios.get('/api/calendarios-datas').then(({ data}) => {
                    (this.calendariosdatas = data)
                    this.loading = false
                });
            },

        },
        created() {
            Fire.$on('searching', () => {
                let query = this.search;
                axios.get(`api/calendarios-datas/findCalendariosdatas?q=${query}`)
                    .then((data) => {
                      this.calendariosdatas = data.data
                    });
            });

            this.load()

            Fire.$on('AfterCreate', () => {
                this.load()
            });

        }
    }
</script>
