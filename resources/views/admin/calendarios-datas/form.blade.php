<template>
    <div>
        <VclBulletList :rows="1" v-if="isLoading"></VclBulletList>
        
        <form v-else @submit.prevent="editmode ? update() : create()">
            <div class="form-group">
    <label for="empresas_id" class="control-label">'Empresas Id'</label>
    <input class="form-control" name="empresas_id" type="number" id="empresas_id">
</div>
<div class="form-group">
    <label for="calendarios_id" class="control-label">'Calendarios Id'</label>
    <input class="form-control" name="calendarios_id" type="number" id="calendarios_id">
</div>
<div class="form-group">
    <label for="descricao_feriado_ou_recesso" class="control-label">'Descricao Feriado Ou Recesso'</label>
    <textarea 
    v-model="form.descricao_feriado_ou_recesso"
    rows="5" name="descricao_feriado_ou_recesso" 
    type="textarea" 
    v-bind:readonly="is_readonly"
    placeholder="descricao_feriado_ou_recesso"
    class="form-control" 
    :class="{ 'is-invalid': form.errors.has('descricao_feriado_ou_recesso') }"
></textarea>
<has-error :form="form" field="descricao_feriado_ou_recesso"></has-error>

</div>
<div class="form-group">
    <label for="data_feriado" class="control-label">'Data Feriado'</label>
    <input class="form-control" name="data_feriado" type="date" id="data_feriado" >
</div>
<div class="form-group">
    <label for="data_periodo_inicio" class="control-label">'Data Periodo Inicio'</label>
    <input class="form-control" name="data_periodo_inicio" type="datetime-local" id="data_periodo_inicio" >
</div>
<div class="form-group">
    <label for="data_periodo_fim" class="control-label">'Data Periodo Fim'</label>
    <input class="form-control" name="data_periodo_fim" type="datetime-local" id="data_periodo_fim" >
</div>
<div class="form-group">
    <label for="hora_ou_periodo" class="control-label">'Hora Ou Periodo'</label>
    <div class="radio">
    <label><input name="hora_ou_periodo" type="radio" value="1" {{ (isset($calendariosdata) && 1 == $calendariosdata->hora_ou_periodo) ? 'checked' : '' }}> Yes</label>
</div>
<div class="radio">
    <label><input name="hora_ou_periodo" type="radio" value="0" @if (isset($calendariosdata)) {{ (0 == $calendariosdata->hora_ou_periodo) ? 'checked' : '' }} @else {{ 'checked' }} @endif> No</label>
</div>
</div>
<div class="form-group">
    <label for="dia_inteiro_apuracao" class="control-label">'Dia Inteiro Apuracao'</label>
    <div class="radio">
    <label><input name="dia_inteiro_apuracao" type="radio" value="1" {{ (isset($calendariosdata) && 1 == $calendariosdata->dia_inteiro_apuracao) ? 'checked' : '' }}> Yes</label>
</div>
<div class="radio">
    <label><input name="dia_inteiro_apuracao" type="radio" value="0" @if (isset($calendariosdata)) {{ (0 == $calendariosdata->dia_inteiro_apuracao) ? 'checked' : '' }} @else {{ 'checked' }} @endif> No</label>
</div>
</div>
<div class="form-group">
    <label for="apuracao" class="control-label">'Apuracao'</label>
    <input class="form-control" name="apuracao" type="text" id="apuracao" >
</div>
<div class="form-group">
    <label for="horario_apuracao" class="control-label">'Horario Apuracao'</label>
    <input class="form-control" name="horario_apuracao" type="time" id="horario_apuracao" >
</div>
<div class="form-group">
    <label for="status" class="control-label">'Status'</label>
    <div class="radio">
    <label><input name="status" type="radio" value="1" {{ (isset($calendariosdata) && 1 == $calendariosdata->status) ? 'checked' : '' }}> Yes</label>
</div>
<div class="radio">
    <label><input name="status" type="radio" value="0" @if (isset($calendariosdata)) {{ (0 == $calendariosdata->status) ? 'checked' : '' }} @else {{ 'checked' }} @endif> No</label>
</div>
</div>


            <div class="modal-footer">
                <router-link to="/NOME" class="btn btn-primary btn-neutral">Voltar</router-link>
                <template v-if="!is_readonly">
                    <button v-show="!editmode" type="submit" class="btn btn-primary">Salvar</button>
                    <button v-show="editmode" type="submit" class="btn btn-primary">Editar</button>
                </template>
            </div>
        </form>
    </div>
</template>

<script>
    import crud from '@components/crud'
    import { VclBulletList } from 'vue-content-loading';

    export default {
        props: {
            is_readonly: Boolean,
            loading: Boolean
        },

        components: {
            VclBulletList
        },

        data() {
            return {
                isLoading: this.loading,
                editmode: this.$route.params.id ? true : false,
                NOME : {},
                form: new Form({
                    id: '',

                }),
                search: ''
            }
        },

        methods: {
            getData(){
                this.$store.commit('PRELOADER', true)
                this.form.reset()
                axios.get(`/api/ROTA/${this.$route.params.id}`)
                .then(({data}) => {
                    (this.form.fill(data))
                    this.isLoading = false
                })
                .catch((err) => {
                    this.isLoading = false
                    toast({
                        type: 'error',
                        title: 'Algo de errado aconteceu, por favor entre em contato com o suporte!'
                    })

                    console.log(err)
                })

                this.isLoading = false
            },

            create(){
                crud.create(this, 'ROTA')
            },

            update(){
                crud.update(this, 'ROTA')
            }
        },
        created() {
            if(this.editmode) {
                this.getData()
            }
        }
    }
</script>
