@extends('layouts.appfora')

@section('content')
    <img class="wave" src="{{ asset('stylelogin/wavenandao.png') }}" />
    <div class="container">
        <div class="img">
            <img src="{{ asset('stylelogin/img.svg') }}" alt="">
        </div>
        <div class="login-container">
            <form method="POST" action="{{ route('login') }}" id="loginid">
                @csrf

                <input type="hidden" name="finger_print">
                <input type="hidden" name="sistema_operacional">
                <input type="hidden" name="sistema_operacional_versao">
                <input type="hidden" name="dispositivo_para_acesso">
                <input type="hidden" name="navegador">

                @if (session('status'))
                    <div class="alert alert-danger" style="color: orangered;">
                        {{ session('status') }}
                    </div>
                    <br>
                    <br>
                @endif
                
                <img class="avatar" src="{{  asset('stylelogin/avatar.svg') }}" alt="">
                <h2>Ponto Digital</h2>
                <div class="input-div one">
                    <div class="i">
                        <i class="fas fa-user"></i>
                    </div>
                    <div>
                        <h5>Código</h5>
                        <input maxlength="4" id="codigo" placeholder="Código" type="matricula" class="input form-control @error('matricula') is-invalid @enderror" name="codigo" value="{{ old('codigo') }}" required autocomplete="codigo" autofocus>
                    </div>
                </div>
                <div class="input-div one">
                    <div class="i">
                        <i class="fas fa-user"></i>
                    </div>
                    <div>
                        <h5>Username</h5>
                        <input id="email" placeholder="Email" type="matricula" class="input form-control @error('matricula') is-invalid @enderror" name="matricula" value="{{ old('matricula') }}" required autocomplete="matricula">
                    </div>
                </div>
                <div id="aguardeappend" class="input-div two">
                    <div class="i">
                        <i class="fas fa-lock"></i>
                    </div>
                    <div>
                        <h5>Password</h5>
                        <input id="password" placeholder="Senha" type="password" class="input form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    </div>
                </div>
                @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}">{{ __('Esqueceu sua senha?') }}</a>
                @endif
                <input type="submit" class="btn" value="Login">
            </form>
        </div>
    </div>
@endsection

@push('js')
<script>
    var client = new ClientJS();

    $(document).ready(function () {
        $('[name="finger_print"]').val(client.getFingerprint())
        $('[name="sistema_operacional"]').val(client.getOS())
        $('[name="sistema_operacional_versao"]').val(client.getOSVersion())
        $('[name="dispositivo_para_acesso"]').val(client.isMobile() == true ? 'celular' : 'computador')
        $('[name="navegador"]').val(client.getBrowser())
    });

    $('#loginid').submit(function(){
        $(this).find(':input[type=submit]').hide()
        $('#aguardeappend').append('<br><h4 style="color: blue;"> Aguarde </h4>')
    });

            // console.log("getSoftwareVersion", client.getSoftwareVersion());
            // console.log("getBrowserData", client.getBrowserData());
            // console.log("getFingerprint", client.getFingerprint());
            // console.log("getUserAgent", client.getUserAgent());
            // console.log("getUserAgentLowerCase", client.getUserAgentLowerCase());

            // console.log("getBrowser", client.getBrowser());
            // console.log("getBrowserVersion", client.getBrowserVersion());
            // console.log("getBrowserMajorVersion", client.getBrowserMajorVersion());
            // console.log("isIE", client.isIE());
            // console.log("isChrome", client.isChrome());
            // console.log("isFirefox", client.isFirefox());
            // console.log("isSafari", client.isSafari());
            // console.log("isOpera", client.isOpera());

            // console.log("getEngine", client.getEngine());
            // console.log("getEngineVersion", client.getEngineVersion());

            
            // console.log("getOS", client.getOS());
            // console.log("getOSVersion", client.getOSVersion());
            // console.log("isWindows", client.isWindows());
            // console.log("isMac", client.isMac());
            // console.log("isLinux", client.isLinux());
            // console.log("isUbuntu", client.isUbuntu());
            // console.log("isSolaris", client.isSolaris());

            // console.log("getDevice", client.getDevice());
            // console.log("getDeviceType", client.getDeviceType());
            // console.log("getDeviceVendor", client.getDeviceVendor());

            // console.log("getCPU", client.getCPU());

            // console.log("isMobile", client.isMobile());
            // console.log("isMobileMajor", client.isMobileMajor());
            // console.log("isMobileAndroid", client.isMobileAndroid());
            // console.log("isMobileOpera", client.isMobileOpera());
            // console.log("isMobileWindows", client.isMobileWindows());
            // console.log("isMobileBlackBerry", client.isMobileBlackBerry());

            // console.log("isMobileIOS", client.isMobileIOS());
            // console.log("isIphone", client.isIphone());
            // console.log("isIpad", client.isIpad());
            // console.log("isIpod", client.isIpod());

            // console.log("getScreenPrint", client.getScreenPrint());
            // console.log("getColorDepth", client.getColorDepth());
            // console.log("getCurrentResolution", client.getCurrentResolution());
            // console.log("getAvailableResolution", client.getAvailableResolution());
            // console.log("getDeviceXDPI", client.getDeviceXDPI());
            // console.log("getDeviceYDPI", client.getDeviceYDPI());

            // console.log("getPlugins", client.getPlugins());
            // console.log("isJava", client.isJava());
            // console.log("getJavaVersion", client.getJavaVersion());
            // console.log("isFlash", client.isFlash());
            // console.log("getFlashVersion", client.getFlashVersion());
            // console.log("isSilverlight", client.isSilverlight());
            // console.log("getSilverlightVersion", client.getSilverlightVersion());

            // console.log("getMimeTypes", client.getMimeTypes());
            // console.log("isMimeTypes", client.isMimeTypes());

            // console.log("isFont", client.isFont());
            // console.log("getFonts", client.getFonts());

            // console.log("isLocalStorage", client.isLocalStorage());
            // console.log("isSessionStorage", client.isSessionStorage());
            // console.log("isCookie", client.isCookie());

            // console.log("getTimeZone", client.getTimeZone());

            // console.log("getLanguage", client.getLanguage());
            // console.log("getSystemLanguage", client.getSystemLanguage());

            // console.log("isCanvas", client.isCanvas());
            // console.log("getCanvasPrint", client.getCanvasPrint());
                  
  
  
</script>
@endpush