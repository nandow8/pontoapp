@extends('layouts.appfora')

@section('content')
    {{-- <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}
    <div style="display: flex; justify-content: center; align-items: center;">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong style="color: orangered;">{{ $message }}</strong>
            </span>
        @enderror
    </div>

    <div class="container" style="display: flex; justify-content: center;">
        <div class="login-container">
            <form method="POST" action="{{ route('password.email') }}">
                    @csrf                
                
                <img class="avatar" src="{{  asset('stylelogin/avatar.svg') }}" alt="">
                <h2>Ponto Digital</h2>
                <h5>Digite seu Email</h5>
                <div class="input-div one">
                    <div class="i">
                        <i class="fas fa-user"></i>
                    </div>
                    <div>
                        <h5>Username</h5>
                        <input id="email" type="email" class="input form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>  
                    </div>
                </div>
                {{-- <div style="display: flex; justify-content: space-around;">
                    <a href="{{ route('login') }}">{{ __('Voltar') }}</a> --}}
                    <input type="submit" class="btn" value="Enviar">
                {{-- </div> --}}
            </form>
        </div>
    </div>
@endsection
