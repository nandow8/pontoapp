<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'AppPontoDigital') }}</title>
        <!-- Favicon -->
        <link href="{{ asset('argon') }}/img/brand/favicon.png" rel="icon" type="image/png">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <!-- Icons -->
        <link href="{{ asset('argon') }}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
        <link href="{{ asset('argon') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">

        @stack('css')

        <!-- Argon CSS -->
        <link type="text/css" href="{{ asset('argon') }}/css/argon.css?v=1.0.0" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('argon') }}/vendor/animate.css/animate.min.css">
        <link rel="stylesheet" href="{{ asset('argon') }}/vendor/sweetalert2/dist/sweetalert2.min.css">
        <link rel="stylesheet" href="{{ asset('argon') }}/vendor/select2/dist/css/select2.min.css">
        {{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}
        <style>
            .router-link-exact-active {
                background-color: azure;
                color: black !important;
            }
        </style>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlHzImDCagrOppCrUjRU2FT__KHKsUYhs&libraries=geometry,places,drawing"
        async defer></script>
        {{-- rob AIzaSyAvN6oKQziwbEWFIHvZCKV4wGMNv27f5Ic --}}
    </head>
    <body class="{{ $class ?? '' }}">
        <?php

            $permissoesList = array();
            if (auth()->check()) {
                foreach (auth()->user()->permissoes() as $key => $value) {
                    $permissoesList[$key] = $value->idpermissao;
                }
            }

            // dd(array_search(6, $permissoesList));
        ?>
        <div id="app">
            @auth()
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                @if (!in_array(request()->route()->getName(), ['welcome', 'page.pricing', 'page.lock']))
                    {{-- <template v-if="verificaRotaCSS != 1"> --}}
                        @include('layouts.navbars.sidebar')
                    {{-- </template> --}}
                @endif
            @endauth

            <div class="main-content">
                {{-- <template v-if="verificaRotaCSS != 1"> --}}
                    @include('layouts.navbars.navbar')
                {{-- </template> --}}
                <preloader-component></preloader-component>
                <router-view></router-view>
            </div>


            @if(!auth()->check() || in_array(request()->route()->getName(), ['welcome', 'page.pricing', 'page.lock']))
                @include('layouts.footers.guest')
            @endif
        </div>

        <script src="{{ asset('argon') }}/vendor/jquery/dist/jquery.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/js-cookie/js.cookie.js"></script>
        <script src="{{ asset('argon') }}/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/lavalamp/js/jquery.lavalamp.min.js"></script>
        {{-- <script src="{{ asset('argon') }}/vendor/sweetalert2/dist/sweetalert2.min.js"></script> --}}
        <script src="{{ asset('argon') }}/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>
        {{-- <script src="{{ asset('js/setup_sweetalert.js') }}"></script> --}}
        <script src="{{ asset('argon') }}/vendor/select2/dist/js/select2.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="{{ asset('js/client.min.js') }}" ></script>
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        
            var BASE_URL = '{{url('')}}/';
        </script>
        
        @stack('js')

        <!-- Argon JS -->
        <script src="{{ asset('argon') }}/js/argon.js?v=1.0.0"></script>
        <script src="{{ asset('argon') }}/js/demo.min.js"></script>

        @auth
            <?php 
                $user_dispositivo = session('permissoes_dispositivos');
            ?>

            <script>
                window.user = @json(auth()->user());
                window.permissoes = @json(auth()->user()->permissoes());
                window.user_dispositivo = @json($user_dispositivo);
            </script>
        @endauth
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
