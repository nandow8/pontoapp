<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner scroll-scrollx_visible">
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="{{ route('home') }}">
                {{-- <img src="{{ asset('argon') }}/img/brand/blue.png" class="navbar-brand-img" alt="..."> --}}
                <h2 style="color: darkblue; font-size: 23px;">Ponto Digital</h2>
            </a>
            <div class="ml-auto">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    @if(array_search(App\Models\Admin\Permissoes::ADMINISTRATIVO_VISUALIZAR, $permissoesList) !== false)
                        <li class="nav-item {{ $parentSection == 'administrativo' ? 'active' : '' }}">
                            <a class="nav-link collapsed" href="#navbar-administrativo" data-toggle="collapse" role="button" aria-expanded="{{ $parentSection == 'administrativo' ? 'true' : '' }}" aria-controls="navbar-administrativo">
                                <i class="fas fa-user-tie text-primary"></i>
                                <span class="nav-link-text">{{ __('Administrativo') }}</span>
                            </a>
                            <div class="collapse {{ $parentSection == 'administrativo' ? 'show' : '' }}" id="navbar-administrativo">
                                <ul class="nav nav-sm flex-column">
                                    @if(array_search(App\Models\Admin\Permissoes::CLIENTES_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'empresas' ? 'active' : '' }}">
                                            <router-link to="/admin/empresas" class="nav-link">{{ __('Clientes') }}</router-link>
                                        </li>
                                    @endif
                                    @if(array_search(App\Models\Admin\Permissoes::USUÁRIOS_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'usuarios' ? 'active' : '' }}">
                                            <router-link to="/users" class="nav-link">{{ __('Usuários') }}</router-link>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </li>
                    @endif

                    @if(array_search(App\Models\Admin\Permissoes::UNIDADES_VISUALIZAR, $permissoesList) !== false)
                        <li class="nav-item {{ $parentSection == 'unidades' ? 'active' : '' }}">
                            <a class="nav-link collapsed" href="#navbar-unidades" data-toggle="collapse" role="button" aria-expanded="{{ $parentSection == 'unidades' ? 'true' : '' }}" aria-controls="navbar-unidades">
                                <i class="fas fa-home text-primary"></i>
                                <span class="nav-link-text">{{ __('Unidades') }}</span>
                            </a>
                            <div class="collapse {{ $parentSection == 'unidades' ? 'show' : '' }}" id="navbar-unidades">
                                <ul class="nav nav-sm flex-column">
                                    @if(array_search(App\Models\Admin\Permissoes::FILIAIS_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'filiais' ? 'active' : '' }}">
                                            <router-link to="/admin/filiais" class="nav-link">{{ __('Filiais') }}</router-link>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </li>
                    @endif

                    @if(array_search(App\Models\Admin\Permissoes::CONFIGURACOES_VISUALIZAR, $permissoesList) !== false)
                        <li class="nav-item {{ $parentSection == 'configuracoes' ? 'active' : '' }}">
                            <a class="nav-link collapsed" href="#navbar-configuracoes" data-toggle="collapse" role="button" aria-expanded="{{ $parentSection == 'configuracoes' ? 'true' : '' }}" aria-controls="navbar-configuracoes">
                                <i class="ni ni-settings text-danger"></i>
                                <span class="nav-link-text">{{ __('Configurações') }}</span>
                            </a>
                            <div class="collapse {{ $parentSection == 'configuracoes' ? 'show' : '' }}" id="navbar-configuracoes">
                                <ul class="nav nav-sm flex-column">
                                    @if(array_search(App\Models\Admin\Permissoes::GERAIS_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'gerais' ? 'active' : '' }}">
                                            <router-link to="/admin/gerais" class="nav-link">{{ __('Gerais') }}</router-link>
                                        </li>
                                    @endif
                                    @if(array_search(App\Models\Admin\Permissoes::CERCASVIRTUAIS_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'cercas-virtuais' ? 'active' : '' }}">
                                            <router-link to="/admin/cercas-virtuais" class="nav-link">{{ __('Cercas Virtuais') }}</router-link>
                                        </li>
                                    @endif
                                    @if(array_search(App\Models\Admin\Permissoes::DISPOSITIVOSAUTORIZADOS_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'dispositivos-autorizado' ? 'active' : '' }}">
                                            <router-link to="/admin/dispositivos-autorizado" class="nav-link">{{ __('Dispositivos Autorizados') }}</router-link>
                                        </li>
                                    @endif
                                    @if(array_search(App\Models\Admin\Permissoes::PERFIS_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'perfis' ? 'active' : '' }}">
                                            <router-link to="/admin/perfis" class="nav-link">{{ __('Perfis') }}</router-link>
                                        </li>
                                    @endif
                                    @if(array_search(App\Models\Admin\Permissoes::CARGOS_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'cargos' ? 'active' : '' }}">
                                            <router-link to="/admin/cargos" class="nav-link">{{ __('Cargos') }}</router-link>
                                        </li>
                                    @endif
                                    @if(array_search(App\Models\Admin\Permissoes::SETORES_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'setores' ? 'active' : '' }}">
                                            <router-link to="/admin/setores" class="nav-link">{{ __('Setores') }}</router-link>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </li>
                    @endif

                    @if(array_search(App\Models\Admin\Permissoes::COLABORADORES_VISUALIZAR, $permissoesList) !== false)
                        <li class="nav-item {{ $elementName == 'colaboradores' ? 'active' : '' }}">
                            <router-link to="/colaboradores" class="nav-link">
                                <i class="fas fa-users text-success"></i>
                                <span class="nav-link-text">{{ __('Colaboradores') }}</span>
                            </router-link>
                        </li>
                    @endif

                    @if(array_search(App\Models\Admin\Permissoes::CALENDARIOSDEFERIADOS_VISUALIZAR, $permissoesList) !== false)
                        <li class="nav-item {{ $elementName == 'admin/calendarios' ? 'active' : '' }}">
                            <router-link to="/admin/calendarios" class="nav-link">
                                <i class="ni ni-calendar-grid-58 text-red"></i>
                                <span class="nav-link-text">{{ __('Calendários de Feriados') }}</span>
                            </router-link>
                        </li>
                    @endif

                    @if(array_search(App\Models\Admin\Permissoes::ESCALAS_VISUALIZAR, $permissoesList) !== false)
                        <li class="nav-item {{ $elementName == 'escalas' ? 'active' : '' }}">
                            <router-link to="/escalas" class="nav-link">
                                <i class="ni ni-ui-04 text-info"></i>
                                <span class="nav-link-text">{{ __('Escalas') }}</span>
                            </router-link>
                        </li>
                    @endif

                    @if(array_search(App\Models\Admin\Permissoes::CONTROLEDEPONTO_VISUALIZAR, $permissoesList) !== false)
                        <li class="nav-item {{ $parentSection == 'controledeponto' ? 'active' : '' }}">
                            <a class="nav-link collapsed" href="#navbar-controledeponto" data-toggle="collapse" role="button" aria-expanded="{{ $parentSection == 'controledeponto' ? 'true' : '' }}" aria-controls="navbar-controledeponto">
                                <i class="fas fa-qrcode text-default"></i>
                                <span class="nav-link-text">{{ __('Controle de Ponto') }}</span>
                            </a>
                            <div class="collapse {{ $parentSection == 'controledeponto' ? 'show' : '' }}" id="navbar-controledeponto">
                                <ul class="nav nav-sm flex-column">
                                    @if(array_search(App\Models\Admin\Permissoes::PONTO_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'ponto' ? 'active' : '' }}">
                                            <router-link to="/admin/controledeponto/pontos" class="nav-link">{{ __('Ponto') }}</router-link>
                                        </li>
                                    @endif
                                    @if(array_search(App\Models\Admin\Permissoes::CONTROLEDEHORAS_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'bancodehoras' ? 'active' : '' }}">
                                            <router-link to="/admin/controledeponto/bancodehoras" class="nav-link">{{ __('Banco de Horas') }}</router-link>
                                        </li>
                                    @endif
                                    @if(array_search(App\Models\Admin\Permissoes::FECHAMENTO_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'fechamento' ? 'active' : '' }}">
                                            <router-link to="/admin/fechamento" class="nav-link">{{ __('Fechamento') }}</router-link>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </li>
                    @endif

                    @if(array_search(App\Models\Admin\Permissoes::SOLICITACOES_VISUALIZAR, $permissoesList) !== false)
                        <li class="nav-item {{ $parentSection == 'solicitacoes' ? 'active' : '' }}">
                            <a class="nav-link collapsed" href="#navbar-solicitacoes" data-toggle="collapse" role="button" aria-expanded="{{ $parentSection == 'solicitacoes' ? 'true' : '' }}" aria-controls="navbar-solicitacoes">
                                <i class="ni ni-email-83 text-muted"></i>
                                <span class="nav-link-text">{{ __('Solicitações') }}</span>
                            </a>
                            <div class="collapse {{ $parentSection == 'solicitacoes' ? 'show' : '' }}" id="navbar-solicitacoes">
                                <ul class="nav nav-sm flex-column">
                                    @if(array_search(App\Models\Admin\Permissoes::OCORRESNCIASPENDENTES_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'ocorrenciaspendentes' ? 'active' : '' }}">
                                            <router-link to="/ocorrenciaspendentes" class="nav-link">{{ __('Ocorrências Pendentes') }}</router-link>
                                        </li>
                                    @endif
                                    @if(array_search(App\Models\Admin\Permissoes::MARCACOESPENDENTES_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'marcacoespendentes' ? 'active' : '' }}">
                                            <router-link to="/admin/marcacoespendentes" class="nav-link">{{ __('Marcações Pendentes') }}</router-link>
                                        </li>
                                    @endif
                                    @if(array_search(App\Models\Admin\Permissoes::HISTORICO_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'historico' ? 'active' : '' }}">
                                            <router-link to="/admin/historico" class="nav-link">{{ __('Histórico') }}</router-link>
                                        </li>
                                    @endif
                                    
                                    @if(array_search(App\Models\Admin\Permissoes::MARCACOESPONTOSAUDITORIA_VISUALIZAR, $permissoesList) !== false)
                                        <li class="nav-item {{ $elementName == 'registro-ponto-auditoria' ? 'active' : '' }}">
                                            <router-link to="/admin/registro-ponto-auditoria" class="nav-link">{{ __('Marcações auditoria') }}</router-link>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </li>
                    @endif

                    @if(array_search(App\Models\Admin\Permissoes::OCORRENCIASJUSTIFICADAS_VISUALIZAR, $permissoesList) !== false)
                        <li class="nav-item {{ $elementName == 'ocorrenciasjustificadas' ? 'active' : '' }}">
                            <router-link to="/admin/ocorrenciasjustificadas" class="nav-link">
                                <i class="ni ni-notification-70 text-warning"></i>
                                <span class="nav-link-text">{{ __('Ocorrências Justificadas') }}</span>
                            </router-link>
                        </li>
                    @endif

                    @if(array_search(App\Models\Admin\Permissoes::RELATORIOS_VISUALIZAR, $permissoesList) !== false)
                        <li class="nav-item {{ $elementName == 'relatorios' ? 'active' : '' }}">
                            <router-link to="/admin/relatorios" class="nav-link">
                                <i class="ni ni-paper-diploma text-primary"></i>
                                <span class="nav-link-text">{{ __('Relatórios') }}</span>
                            </router-link>
                        </li>
                    @endif

                    @if(array_search(App\Models\Admin\Permissoes::CHAT_VISUALIZAR, $permissoesList) !== false)
                        <li class="nav-item {{ $elementName == 'chat' ? 'active' : '' }}">
                            <router-link to="/chat" class="nav-link">
                                <i class="far fa-comments text-warning"></i>
                                <span class="nav-link-text">{{ __('Chat') }}</span>
                            </router-link>
                        </li>
                    @endif

                    @if(array_search(App\Models\Admin\Permissoes::LOGS_VISUALIZAR, $permissoesList) !== false)
                        <li class="nav-item {{ $elementName == 'logs' ? 'active' : '' }}">
                            <router-link to="/logs" class="nav-link">
                                <i class="fas fa-user-lock text-danger"></i>
                                <span class="nav-link-text">{{ __('Logs') }}</span>
                            </router-link>
                        </li>
                    @endif













                    @php $dispositivo = session('permissoes_dispositivos')['permite_dispositivo']; @endphp

                    @if ($dispositivo == 'SEM_DISPOSITIVO_CADASTRADO' OR $dispositivo == 'AGUARDANDO' )
                        @if(array_search(App\Models\Admin\Permissoes::COLABORADODISPOSITIVO_VISUALIZAR, $permissoesList) !== false)
                            <li class="nav-item {{ $elementName == 'cadastrar-dispositivo' ? 'active' : '' }}">
                                <router-link to="/cadastrar-dispositivo" class="nav-link">
                                    <i class="ni ni-tv-2 text-dark"></i>
                                    <span class="nav-link-text">{{ __('Cadastrar Dispositivo') }}</span>
                                </router-link>
                            </li>
                        @endif
                    @else

                        @if(array_search(App\Models\Admin\Permissoes::REGISTRODEPONTO_VISUALIZAR, $permissoesList) !== false)
                            @if(array_search(App\Models\Admin\Permissoes::COLABORADORREGISTRODEPONTO_VISUALIZAR, $permissoesList) !== false)
                                <li class="nav-item {{ $elementName == 'registro-ponto' ? 'active' : '' }}">
                                    <router-link to="/registro-ponto" class="nav-link">
                                        <i class="fa fa-edit text-success"></i>
                                        <span class="nav-link-text">{{ __('Registro de Ponto') }}</span>
                                    </router-link>
                                </li>
                            @endif
                        @endif

                        @if(array_search(App\Models\Admin\Permissoes::COLABORADORHISTORICOS_VISUALIZAR, $permissoesList) !== false)
                            <li class="nav-item {{ $parentSection == 'solicitacoes' ? 'active' : '' }}">
                                <a class="nav-link collapsed" href="#navbar-solicitacoes" data-toggle="collapse" role="button" aria-expanded="{{ $parentSection == 'solicitacoes' ? 'true' : '' }}" aria-controls="navbar-solicitacoes">
                                    <i class="fa fa-history text-primary"></i>
                                    <span class="nav-link-text">{{ __('Históricos') }}</span>
                                </a>
                                <div class="collapse {{ $parentSection == 'solicitacoes' ? 'show' : '' }}" id="navbar-solicitacoes">
                                    <ul class="nav nav-sm flex-column">
                                        @if(array_search(App\Models\Admin\Permissoes::COLABORADORPONTOS_VISUALIZAR, $permissoesList) !== false)
                                            <li class="nav-item {{ $elementName == 'ocorrenciaspendentes' ? 'active' : '' }}">
                                                <router-link to="/historicos/pontos" class="nav-link">{{ __('Pontos') }}</router-link>
                                            </li>
                                        @endif
                                        @if(array_search(App\Models\Admin\Permissoes::COLABORADORBANCODEHORAS_VISUALIZAR, $permissoesList) !== false)
                                            <li class="nav-item {{ $elementName == 'bancodehoras' ? 'active' : '' }}">
                                                <router-link to="/historicos/bancodehoras/1/edit" class="nav-link">{{ __('Banco de Horas') }}</router-link>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </li>
                        @endif
                        
                    @endif

                    @if(array_search(App\Models\Admin\Permissoes::COLABORADORCHAT_VISUALIZAR, $permissoesList) !== false)
                        <li class="nav-item {{ $elementName == 'colaboradorchat' ? 'active' : '' }}">
                            <router-link to="/colaboradorchat" class="nav-link">
                                <i class="far fa-comments text-warning"></i>
                                <span class="nav-link-text">{{ __('Chat') }}</span>
                            </router-link>
                        </li>
                    @endif


                </ul>
                <!-- Divider -->
                <hr class="my-3">
                <!-- Heading -->
                {{-- <h6 class="navbar-heading p-0 text-muted">{{ __('Documentation') }}</h6> --}}
                <!-- Navigation -->
                {{-- <ul class="navbar-nav mb-md-3">
                    <li class="nav-item">
                        <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html" target="_blank">
                            <i class="ni ni-spaceship"></i>
                            <span class="nav-link-text">Getting started</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/foundation/colors.html" target="_blank">
                            <i class="ni ni-palette"></i>
                            <span class="nav-link-text">Foundation</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/components/alerts.html" target="_blank">
                            <i class="ni ni-ui-04"></i>
                            <span class="nav-link-text">Components</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/plugins/charts.html" target="_blank">
                            <i class="ni ni-chart-pie-35"></i>
                            <span class="nav-link-text">Plugins</span>
                        </a>
                    </li>
                </ul> --}}
            </div>
        </div>
    </div>
</nav>
