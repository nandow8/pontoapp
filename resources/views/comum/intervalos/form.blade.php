<template>
    <div>
        <VclBulletList :rows="1" v-if="isLoading"></VclBulletList>
        
        <form v-else @submit.prevent="editmode ? update() : create()">
            <div class="form-group">
    <label for="empresas_id" class="control-label">'Empresas Id'</label>
    <input class="form-control" name="empresas_id" type="number" id="empresas_id">
</div>
<div class="form-group">
    <label for="inicio_intervalo" class="control-label">'Inicio Intervalo'</label>
    <input class="form-control" name="inicio_intervalo" type="time" id="inicio_intervalo">
</div>
<div class="form-group">
    <label for="fim_intervalo" class="control-label">'Fim Intervalo'</label>
    <input class="form-control" name="fim_intervalo" type="time" id="fim_intervalo">
</div>
<div class="form-group">
    <label for="abonar" class="control-label">'Abonar'</label>
    <div class="radio">
    <label><input name="abonar" type="radio" value="1" > Yes</label>
</div>
<div class="radio">
    <label><input name="abonar" type="radio" value="0"> No</label>
</div>
</div>
<div class="form-group">
    <label for="pre_assinalar" class="control-label">'Pre Assinalar'</label>
    <div class="radio">
    <label><input name="pre_assinalar" type="radio" value="1" > Yes</label>
</div>
<div class="radio">
    <label><input name="pre_assinalar" type="radio" value="0"> No</label>
</div>
</div>
<div class="form-group">
    <label for="status" class="control-label">'Status'</label>
    <div class="radio">
    <label><input name="status" type="radio" value="1" {{ (isset($intervalo) && 1 == $intervalo->status) ? 'checked' : '' }}> Yes</label>
</div>
<div class="radio">
    <label><input name="status" type="radio" value="0"> No</label>
</div>
</div>


            <div class="modal-footer">
                <router-link to="/NOME" class="btn btn-primary btn-neutral">Voltar</router-link>
                <template v-if="!is_readonly">
                    <button v-show="!editmode" type="submit" class="btn btn-primary">Salvar</button>
                    <button v-show="editmode" type="submit" class="btn btn-primary">Editar</button>
                </template>
            </div>
        </form>
    </div>
</template>

<script>
    import crud from '@components/crud'
    import { VclBulletList } from 'vue-content-loading';

    export default {
        props: {
            is_readonly: Boolean,
            loading: Boolean
        },

        components: {
            VclBulletList
        },

        data() {
            return {
                isLoading: this.loading,
                editmode: this.$route.params.id ? true : false,
                NOME : {},
                form: new Form({
                    id: '',

                }),
                search: ''
            }
        },

        methods: {
            getData(){
                this.isLoading = true
                this.form.reset()
                axios.get(`/api/ROTA/${this.$route.params.id}`)
                .then(({data}) => {
                    (this.form.fill(data))
                    this.isLoading = false
                })
                .catch((err) => {
                    this.isLoading = false
                    toast({
                        type: 'error',
                        title: 'Algo de errado aconteceu, por favor entre em contato com o suporte!'
                    })

                    console.log(err)
                })

                this.isLoading = false
            },

            create(){
                crud.create(this, 'ROTA')
            },

            update(){
                crud.update(this, 'ROTA')
            }
        },
        created() {
            if(this.editmode) {
                this.getData()
            }
        }
    }
</script>
