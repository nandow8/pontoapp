<template>
    <div>
        <VclBulletList :rows="1" v-if="isLoading"></VclBulletList>
        
        <form v-else @submit.prevent="editmode ? update() : create()">
            <div class="form-group">
    <label for="dias_jornada" class="control-label">'Dias Jornada'</label>
    <input
    v-model="form.dias_jornada"
    v-bind:readonly="is_readonly"
    type="text"
    name="dias_jornada"
    placeholder="dias_jornada"
    class="form-control"
    :class="{ 'is-invalid': form.errors.has('dias_jornada') }"
/>
<has-error :form="form" field="dias_jornada"></has-error>
</div>


            <div class="modal-footer">
                <router-link to="/NOME" class="btn btn-primary btn-neutral">Voltar</router-link>
                <template v-if="!is_readonly">
                    <button v-show="!editmode" type="submit" class="btn btn-primary">Salvar</button>
                    <button v-show="editmode" type="submit" class="btn btn-primary">Editar</button>
                </template>
            </div>
        </form>
    </div>
</template>

<script>
    import crud from '@components/crud'
    import { VclBulletList } from 'vue-content-loading';

    export default {
        props: {
            is_readonly: Boolean,
            loading: Boolean
        },

        components: {
            VclBulletList
        },

        data() {
            return {
                isLoading: this.loading,
                editmode: this.$route.params.id ? true : false,
                NOME : {},
                form: new Form({
                    id: '',

                }),
                search: ''
            }
        },

        methods: {
            getData(){
                this.isLoading = true
                this.form.reset()
                axios.get(`/api/ROTA/${this.$route.params.id}`)
                .then(({data}) => {
                    (this.form.fill(data))
                    this.isLoading = false
                })
                .catch((err) => {
                    this.isLoading = false
                    toast({
                        type: 'error',
                        title: 'Algo de errado aconteceu, por favor entre em contato com o suporte!'
                    })

                    console.log(err)
                })

                this.isLoading = false
            },

            create(){
                crud.create(this, 'ROTA')
            },

            update(){
                crud.update(this, 'ROTA')
            }
        },
        created() {
            if(this.editmode) {
                this.getData()
            }
        }
    }
</script>
