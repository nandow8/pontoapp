<template>
    <div>
        <VclBulletList :rows="1" v-if="isLoading"></VclBulletList>
        
        <form v-else @submit.prevent="editmode ? update() : create()">
            <div class="form-group">
    <label for="empresas_id" class="control-label">'Empresas Id'</label>
    <input class="form-control" name="empresas_id" type="number" id="empresas_id" value="{{ isset($chatmessage->empresas_id) ? $chatmessage->empresas_id : ''}}" >
</div>
<div class="form-group">
    <label for="users_id" class="control-label">'Users Id'</label>
    <input class="form-control" name="users_id" type="number" id="users_id" value="{{ isset($chatmessage->users_id) ? $chatmessage->users_id : ''}}" >
</div>
<div class="form-group">
    <label for="chats_id" class="control-label">'Chats Id'</label>
    <input class="form-control" name="chats_id" type="number" id="chats_id" value="{{ isset($chatmessage->chats_id) ? $chatmessage->chats_id : ''}}" >
</div>
<div class="form-group">
    <label for="mensagem" class="control-label">'Mensagem'</label>
    <textarea 
    v-model="form.mensagem"
    rows="5" name="mensagem" 
    type="textarea" 
    v-bind:readonly="is_readonly"
    placeholder="mensagem"
    class="form-control" 
    :class="{ 'is-invalid': form.errors.has('mensagem') }"
></textarea>
<has-error :form="form" field="mensagem"></has-error>

</div>
<div class="form-group">
    <label for="status" class="control-label">'Status'</label>
    <div class="radio">
    <label><input name="status" type="radio" value="1" {{ (isset($chatmessage) && 1 == $chatmessage->status) ? 'checked' : '' }}> Yes</label>
</div>
<div class="radio">
    <label><input name="status" type="radio" value="0" @if (isset($chatmessage)) {{ (0 == $chatmessage->status) ? 'checked' : '' }} @else {{ 'checked' }} @endif> No</label>
</div>
</div>


            <div class="modal-footer">
                <router-link to="/NOME" class="btn btn-primary btn-neutral">Voltar</router-link>
                <template v-if="!is_readonly">
                    <button v-show="!editmode" type="submit" class="btn btn-primary">Salvar</button>
                    <button v-show="editmode" type="submit" class="btn btn-primary">Editar</button>
                </template>
            </div>
        </form>
    </div>
</template>

<script>
    import crud from '@components/crud'
    import { VclBulletList } from 'vue-content-loading';

    export default {
        props: {
            is_readonly: Boolean,
            loading: Boolean
        },

        components: {
            VclBulletList
        },

        data() {
            return {
                isLoading: this.loading,
                editmode: this.$route.params.id ? true : false,
                NOME : {},
                form: new Form({
                    id: '',

                }),
                search: ''
            }
        },

        methods: {
            getData(){
                this.isLoading = true
                this.form.reset()
                axios.get(`/api/ROTA/${this.$route.params.id}`)
                .then(({data}) => {
                    (this.form.fill(data))
                    this.isLoading = false
                })
                .catch((err) => {
                    this.isLoading = false
                    toast({
                        type: 'error',
                        title: 'Algo de errado aconteceu, por favor entre em contato com o suporte!'
                    })

                    console.log(err)
                })
            },

            create(){
                crud.create(this, 'ROTA')
            },

            update(){
                crud.update(this, 'ROTA')
            }
        },
        created() {
            if(this.editmode) {
                this.getData()
            }
        }
    }
</script>
