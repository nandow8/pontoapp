export default class Gate{

    constructor(user, permissoes){
        this.user = user;
        this.permissoes = permissoes
        this.verificaAcesso = false 
    }

    isMaster() {
        if(this.user.type === 'master'){
            return true;
        }
    }
    
    isAdmin(){
        if(this.user.type === 'admin' || this.user.type === 'master'){
            return true;
        }
    }
    
    isUser(){
        if(this.user.type === 'user' || this.user.type === 'master'){
            return true;
        }
    }
    
    isEmpresa(){
        if(this.user.type === 'empresa' || this.user.type === 'master'){
            return true;
        };
    }

    isAdministrativoVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false
        this.verificaPerfil(this.permissoes, 1)
        if (this.verificaAcesso) {
            return true
        }
    }

    isClientesVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 2); if (this.verificaAcesso) { return true }
    }

    isClientesAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 3); if (this.verificaAcesso) { return true }
    }

    isClientesEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 4); if (this.verificaAcesso) { return true }
    }

    isClientesDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 5); if (this.verificaAcesso) { return true }
    }


    isUsuarioVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 6); if (this.verificaAcesso) { return true }
    }

    isUsuarioAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 7); if (this.verificaAcesso) { return true }
    }

    isUsuarioEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 8); if (this.verificaAcesso) { return true }
    }

    isUsuarioDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 9); if (this.verificaAcesso) { return true }
    }


    isUnidadesVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false
        this.verificaPerfil(this.permissoes, 10)
        if (this.verificaAcesso) {
            return true
        }
    }

    isFiliaisVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 11); if (this.verificaAcesso) { return true }
    }

    isFiliaisAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 12); if (this.verificaAcesso) { return true }
    }

    isFiliaisEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 13); if (this.verificaAcesso) { return true }
    }

    isFiliaisDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 14); if (this.verificaAcesso) { return true }
    }




    isConfiguracoesVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false
        this.verificaPerfil(this.permissoes, 15)
        if (this.verificaAcesso) {
            return true
        }
    }

    isGeraisVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 16); if (this.verificaAcesso) { return true }
    }

    // isGeraisAdicionar() {
        // //if(this.user.type === 'master'){ return true; }
    //     this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 17); if (this.verificaAcesso) { return true }
    // }

    // isGeraisEditar() {
        // //if(this.user.type === 'master'){ return true; }
    //     this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 18); if (this.verificaAcesso) { return true }
    // }

    // isGeraisDeletar() {
        // //if(this.user.type === 'master'){ return true; }
    //     this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 19); if (this.verificaAcesso) { return true }
    // }

    isCercasVirtuaisVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 20); if (this.verificaAcesso) { return true }
    }

    isCercasVirtuaisAdicionar() {
        // //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 21); if (this.verificaAcesso) { return true }
    }

    isCercasVirtuaisEditar() {
        // //if(this.user.type === 'master'){ return true; }
       this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 22); if (this.verificaAcesso) { return true }
    }

    isCercasVirtuaisDeletar() {
        // //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 23); if (this.verificaAcesso) { return true }
    }

    isDispositivosAutorizadosVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 24); if (this.verificaAcesso) { return true }
    }

    isDispositivosAutorizadosAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 25); if (this.verificaAcesso) { return true }
    }

    isDispositivosAutorizadosEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 26); if (this.verificaAcesso) { return true }
    }

    isDispositivosAutorizadosDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 27); if (this.verificaAcesso) { return true }
    }

//PERFIS
    // isPerfisVisualizar() {
        // //if(this.user.type === 'master'){ return true; }
    //     this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 28); if (this.verificaAcesso) { return true }
    // }

    // isPerfisAdicionar() {
        // //if(this.user.type === 'master'){ return true; }
    //     this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 29); if (this.verificaAcesso) { return true }
    // }

    // isPerfisEditar() {
        // //if(this.user.type === 'master'){ return true; }
    //     this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 30); if (this.verificaAcesso) { return true }
    // }

    // isPerfisDeletar() {
        // //if(this.user.type === 'master'){ return true; }
    //     this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 31); if (this.verificaAcesso) { return true }
    // }

    isCargosVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 32); if (this.verificaAcesso) { return true }
    }

    isCargosAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 33); if (this.verificaAcesso) { return true }
    }

    isCargosEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 34); if (this.verificaAcesso) { return true }
    }

    isCargosDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 35); if (this.verificaAcesso) { return true }
    }

    isSetoresVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 36); if (this.verificaAcesso) { return true }
    }

    isSetoresAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 37); if (this.verificaAcesso) { return true }
    }

    isSetoresEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 38); if (this.verificaAcesso) { return true }
    }

    isSetoresDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 39); if (this.verificaAcesso) { return true }
    }


    isColaboradoresVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 40); if (this.verificaAcesso) { return true }
    }

    isColaboradoresAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 41); if (this.verificaAcesso) { return true }
    }

    isColaboradoresEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 42); if (this.verificaAcesso) { return true }
    }

    isColaboradoresDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 43); if (this.verificaAcesso) { return true }
    }


    isCalendarioVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 44); if (this.verificaAcesso) { return true }
    }

    isCalendarioAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 45); if (this.verificaAcesso) { return true }
    }

    isCalendarioEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 46); if (this.verificaAcesso) { return true }
    }

    isCalendarioDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 47); if (this.verificaAcesso) { return true }
    }

    isEscalasVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 48); if (this.verificaAcesso) { return true }
    }

    isEscalasAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 49); if (this.verificaAcesso) { return true }
    }

    isEscalasEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 50); if (this.verificaAcesso) { return true }
    }

    isEscalasDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 51); if (this.verificaAcesso) { return true }
    }


    isControleDePontoVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false
        this.verificaPerfil(this.permissoes, 52)
        if (this.verificaAcesso) {
            return true
        }
    }

    isPontoVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 53); if (this.verificaAcesso) { return true }
    }

    isPontoAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 54); if (this.verificaAcesso) { return true }
    }

    isPontoEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 55); if (this.verificaAcesso) { return true }
    }

    isPontoDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 56); if (this.verificaAcesso) { return true }
    }

    isControleHorasVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 57); if (this.verificaAcesso) { return true }
    }

    isControleHorasAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 58); if (this.verificaAcesso) { return true }
    }

    isControleHorasEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 59); if (this.verificaAcesso) { return true }
    }

    isControleHorasDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 60); if (this.verificaAcesso) { return true }
    }

    isFechamentoVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 61); if (this.verificaAcesso) { return true }
    }

    isFechamentoAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 62); if (this.verificaAcesso) { return true }
    }

    isFechamentoEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 63); if (this.verificaAcesso) { return true }
    }

    isFechamentoDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 64); if (this.verificaAcesso) { return true }
    }


    isSolicitacoesVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false
        this.verificaPerfil(this.permissoes, 65)
        if (this.verificaAcesso) {
            return true
        }
    }

    isOcorrenciasPendentesVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 66); if (this.verificaAcesso) { return true }
    }

    isOcorrenciasPendentesAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 67); if (this.verificaAcesso) { return true }
    }

    isOcorrenciasPendentesEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 68); if (this.verificaAcesso) { return true }
    }

    isOcorrenciasPendentesDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 69); if (this.verificaAcesso) { return true }
    }

    isMarcacoesPendentesVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 70); if (this.verificaAcesso) { return true }
    }

    isMarcacoesPendentesAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 71); if (this.verificaAcesso) { return true }
    }

    isMarcacoesPendentesEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 72); if (this.verificaAcesso) { return true }
    }

    isMarcacoesPendentesDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 73); if (this.verificaAcesso) { return true }
    }

    isHistoricoVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 74); if (this.verificaAcesso) { return true }
    }

    // isHistoricoAdicionar() {
    //     //if(this.user.type === 'master'){ return true; }
    //     this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 75); if (this.verificaAcesso) { return true }
    // }

    // isHistoricoEditar() {
    //     //if(this.user.type === 'master'){ return true; }
    //     this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 76); if (this.verificaAcesso) { return true }
    // }

    // isHistoricoDeletar() {
    //     //if(this.user.type === 'master'){ return true; }
    //     this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 77); if (this.verificaAcesso) { return true }
    // }

    isMarcacoesPontosAuditoriaVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 116); if (this.verificaAcesso) { return true }
    }

    isMarcacoesPontosAuditoriaAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 117); if (this.verificaAcesso) { return true }
    }

    isMarcacoesPontosAuditoriaEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 118); if (this.verificaAcesso) { return true }
    }

    isMarcacoesPontosAuditoriaDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 119); if (this.verificaAcesso) { return true }
    }

// sozinho
    isOcorrenciasJustificadasVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 78); if (this.verificaAcesso) { return true }
    }

    isOcorrenciasJustificadasAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 79); if (this.verificaAcesso) { return true }
    }

    isOcorrenciasJustificadasEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 80); if (this.verificaAcesso) { return true }
    }

    isOcorrenciasJustificadasDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 81); if (this.verificaAcesso) { return true }
    }


//sozinho
    isRelatoriosVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 82); if (this.verificaAcesso) { return true }
    }

    isRelatoriosAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 83); if (this.verificaAcesso) { return true }
    }

    isRelatoriosEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 84); if (this.verificaAcesso) { return true }
    }

    isRelatoriosDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 85); if (this.verificaAcesso) { return true }
    }


//sozinho
    isChatVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 86); if (this.verificaAcesso) { return true }
    }

    isChatAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 87); if (this.verificaAcesso) { return true }
    }

    isChatEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 88); if (this.verificaAcesso) { return true }
    }

    isChatDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 89); if (this.verificaAcesso) { return true }
    }

//sozinho
    isLogsVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 90); if (this.verificaAcesso) { return true }
    }

    isLogsAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 91); if (this.verificaAcesso) { return true }
    }

    isLogsEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 92); if (this.verificaAcesso) { return true }
    }

    isLogsDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 93); if (this.verificaAcesso) { return true }
    }

    isRegistroDePontoVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false
        this.verificaPerfil(this.permissoes, 94)
        if (this.verificaAcesso) {
            return true
        }
    }

    isColaboradorRegistroDePontoVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 95); if (this.verificaAcesso) { return true }
    }

    isColaboradorRegistroDePontoAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 96); if (this.verificaAcesso) { return true }
    }

    isColaboradorRegistroDePontoEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 97); if (this.verificaAcesso) { return true }
    }

    isColaboradorRegistroDePontoDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 98); if (this.verificaAcesso) { return true }
    }






    isColaboradorHistoricosVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false
        this.verificaPerfil(this.permissoes, 99)
        if (this.verificaAcesso) {
            return true
        }
    }

    isColaboradorPontosVisualizar() { // colaborador
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 100); if (this.verificaAcesso) { return true }
    }

    isColaboradorPontosAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 101); if (this.verificaAcesso) { return true }
    }

    isColaboradorPontosEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 102); if (this.verificaAcesso) { return true }
    }

    isColaboradorPontosDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 103); if (this.verificaAcesso) { return true }
    }

    isColaboradorBancoDeHorasVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 104); if (this.verificaAcesso) { return true }
    }

    isColaboradorBancoDeHorasAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 105); if (this.verificaAcesso) { return true }
    }

    isColaboradorBancoDeHorasEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 106); if (this.verificaAcesso) { return true }
    }

    isColaboradorBancoDeHorasDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 107); if (this.verificaAcesso) { return true }
    }

    // chat colaborador

    isColaboradorChatVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 108); if (this.verificaAcesso) { return true }
    }

    isColaboradorChatAdicionar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 109); if (this.verificaAcesso) { return true }
    }

    isColaboradorChatEditar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 110); if (this.verificaAcesso) { return true }
    }

    isColaboradorChatDeletar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 111); if (this.verificaAcesso) { return true }
    }

    isColaboradoDispositivoVisualizar() {
        //if(this.user.type === 'master'){ return true; }
        this.verificaAcesso = false; this.verificaPerfil(this.permissoes, 120); if (this.verificaAcesso) { return true }
    }


    
    

    verificaPerfil(permissoes, idpermissao) {
        permissoes.forEach(element => {
            if (element.idpermissao == idpermissao) {
                this.verificaAcesso = true
            }
        });
    }
}
