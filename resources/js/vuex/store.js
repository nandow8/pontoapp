import Vue from 'vue'
import Vuex from 'vuex'

import preloader from './modules/preloader/preloader'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        preloader
    }
})