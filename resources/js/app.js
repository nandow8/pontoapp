/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import moment from 'moment'
import { Form, HasError, AlertError } from 'vform'
import swal from 'sweetalert2'
import Gate from "./Gate";
import store from './vuex/store'
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import VueTheMask from 'vue-the-mask'
import { Datetime } from 'vue-datetime'
// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css'
import { Settings as LuxonSettings } from 'luxon';

LuxonSettings.defaultLocale = 'pt-br';

Vue.use(VueTheMask)
Vue.use(Loading);
Vue.use(Datetime)

Vue.component('datetime', Datetime);


Vue.prototype.$gate = new Gate(window.user, window.permissoes);
window.usuario = window.user

window.moment = moment

window.Form = Form
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

window.swal = swal;
const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});
window.toast = toast;

import VueRouter from 'vue-router'

Vue.use(VueRouter)

import SistemaLayoutComponent from './templates/SistemaLayoutComponent'

let routes = [
    
    { path: '/admin/dashboard', component: require('./components/Dashboard.vue').default },
    { path: '/profile', component: require('./components/Profile.vue').default },
    { path: '/admin/developer', component: require('./components/Developer.vue').default },
    { path: '*', component: require('./components/NotFound.vue').default },

    {
        path: '/',
        component: SistemaLayoutComponent,
        meta: {auth: true},
        children: [
            { path: '/users', component: require('./pages/users/index.vue').default },
            { path: '/users/create', component: require('./pages/users/create.vue').default },
            { path: '/users/:id/edit', component: require('./pages/users/edit.vue').default },
            { path: '/users/:id', component: require('./pages/users/show.vue').default },

            { path: '/admin/empresas', component: require('./pages/admin/empresas/index.vue').default },
            { path: '/admin/empresas/create', component: require('./pages/admin/empresas/create.vue').default },
            { path: '/admin/empresas/:id/edit', component: require('./pages/admin/empresas/edit.vue').default },
            { path: '/admin/empresas/:id', component: require('./pages/admin/empresas/show.vue').default },

            { path: '/admin/filiais', component: require('./pages/admin/filiais/index.vue').default },
            { path: '/admin/filiais/create', component: require('./pages/admin/filiais/create.vue').default },
            { path: '/admin/filiais/:id/edit', component: require('./pages/admin/filiais/edit.vue').default },
            { path: '/admin/filiais/:id', component: require('./pages/admin/filiais/show.vue').default },

            { path: '/admin/setores', component: require('./pages/admin/setores/index.vue').default },
            { path: '/admin/setores/create', component: require('./pages/admin/setores/create.vue').default },
            { path: '/admin/setores/:id/edit', component: require('./pages/admin/setores/edit.vue').default },
            { path: '/admin/setores/:id', component: require('./pages/admin/setores/show.vue').default },

            { path: '/admin/cargos', component: require('./pages/admin/cargos/index.vue').default },
            { path: '/admin/cargos/create', component: require('./pages/admin/cargos/create.vue').default },
            { path: '/admin/cargos/:id/edit', component: require('./pages/admin/cargos/edit.vue').default },
            { path: '/admin/cargos/:id', component: require('./pages/admin/cargos/show.vue').default },

            { path: '/admin/calendarios', component: require('./pages/admin/calendarios/index.vue').default },
            { path: '/admin/calendarios/create', component: require('./pages/admin/calendarios/create.vue').default },
            { path: '/admin/calendarios/:id/edit', component: require('./pages/admin/calendarios/edit.vue').default },
            { path: '/admin/calendarios/:id', component: require('./pages/admin/calendarios/show.vue').default },

            { path: '/admin/perfis', component: require('./pages/admin/perfis/index.vue').default },
            { path: '/admin/perfis/create', component: require('./pages/admin/perfis/create.vue').default },
            { path: '/admin/perfis/:id/edit', component: require('./pages/admin/perfis/edit.vue').default },
            { path: '/admin/perfis/:id', component: require('./pages/admin/perfis/show.vue').default },

            { path: '/admin/permissoes', component: require('./pages/admin/permissoes/index.vue').default },
            { path: '/admin/permissoes/create', component: require('./pages/admin/permissoes/create.vue').default },
            { path: '/admin/permissoes/:id/edit', component: require('./pages/admin/permissoes/edit.vue').default },
            { path: '/admin/permissoes/:id', component: require('./pages/admin/permissoes/show.vue').default },
                                        
            { path: '/admin/cercas-virtuais', component: require('./pages/admin/cercas-virtuais/index.vue').default },
            { path: '/admin/cercas-virtuais/create', component: require('./pages/admin/cercas-virtuais/create.vue').default },
            { path: '/admin/cercas-virtuais/:id/edit', component: require('./pages/admin/cercas-virtuais/edit.vue').default },
            { path: '/admin/cercas-virtuais/:id', component: require('./pages/admin/cercas-virtuais/show.vue').default },

            { path: '/admin/ocorrenciasjustificadas', component: require('./pages/admin/ocorrenciasjustificadas/index.vue').default },
            { path: '/admin/ocorrenciasjustificadas/create', component: require('./pages/admin/ocorrenciasjustificadas/create.vue').default },
            { path: '/admin/ocorrenciasjustificadas/:id/edit', component: require('./pages/admin/ocorrenciasjustificadas/edit.vue').default },
            { path: '/admin/ocorrenciasjustificadas/:id', component: require('./pages/admin/ocorrenciasjustificadas/show.vue').default },

            /* sem admin pra funcionar o target blank*/ 
            { path: '/ocorrenciaspendentes', component: require('./pages/admin/ocorrenciaspendentes/index.vue').default },
            { path: '/ocorrenciaspendentes/create', component: require('./pages/admin/ocorrenciaspendentes/create.vue').default },
            { path: '/ocorrenciaspendentes/:id/edit', component: require('./pages/admin/ocorrenciaspendentes/edit.vue').default },
            { path: '/ocorrenciaspendentes/:id', component: require('./pages/admin/ocorrenciaspendentes/show.vue').default },

            { path: '/admin/marcacoespendentes', component: require('./pages/admin/marcacoespendentes/index.vue').default },
            { path: '/admin/marcacoespendentes/create', component: require('./pages/admin/marcacoespendentes/create.vue').default },
            { path: '/admin/marcacoespendentes/:id/edit', component: require('./pages/admin/marcacoespendentes/edit.vue').default },
            { path: '/admin/marcacoespendentes/:id', component: require('./pages/admin/marcacoespendentes/show.vue').default },
            
            { path: '/admin/registro-ponto-auditoria', component: require('./pages/admin/registro-ponto-auditoria/index.vue').default },
            { path: '/admin/registro-ponto-auditoria/create', component: require('./pages/admin/registro-ponto-auditoria/create.vue').default },
            { path: '/admin/registro-ponto-auditoria/:id/edit', component: require('./pages/admin/registro-ponto-auditoria/edit.vue').default },
            { path: '/admin/registro-ponto-auditoria/:id', component: require('./pages/admin/registro-ponto-auditoria/show.vue').default },

            { path: '/admin/historico', component: require('./pages/admin/historico/index.vue').default },
            { path: '/admin/historico/marcacoespendentes', component: require('./pages/admin/historico/marcacoespendentes.vue').default },
            { path: '/admin/historico/ocorrenciaspendentes', component: require('./pages/admin/historico/ocorrenciaspendentes.vue').default },

            { path: '/admin/controledeponto/pontos', component: require('./pages/admin/controledeponto/pontos/index.vue').default },
            { path: '/admin/controledeponto/pontos/create', component: require('./pages/admin/controledeponto/pontos/create.vue').default },
            { path: '/admin/controledeponto/pontos/:id', component: require('./pages/admin/controledeponto/pontos/show.vue').default },
            { path: '/admin/controledeponto/pontos/:id/edit', component: require('./pages/admin/controledeponto/pontos/edit.vue').default },

            { path: '/admin/dispositivos-autorizado', component: require('./pages/admin/dispositivos-autorizado/index.vue').default },
            { path: '/admin/dispositivos-autorizado/create', component: require('./pages/admin/dispositivos-autorizado/create.vue').default },
            { path: '/admin/dispositivos-autorizado/:id/edit', component: require('./pages/admin/dispositivos-autorizado/edit.vue').default },
            { path: '/admin/dispositivos-autorizado/:id', component: require('./pages/admin/dispositivos-autorizado/show.vue').default },

            /* Controle de horas */
            { path: '/admin/controledeponto/bancodehoras', component: require('./pages/admin/controledeponto/bancodehoras/index.vue').default },
            { path: '/admin/controledeponto/bancodehoras/create', component: require('./pages/admin/controledeponto/bancodehoras/create.vue').default },
            { path: '/admin/controledeponto/bancodehoras/:id', component: require('./pages/admin/controledeponto/bancodehoras/show.vue').default },
            { path: '/admin/controledeponto/bancodehoras/:id/edit', component: require('./pages/admin/controledeponto/bancodehoras/edit.vue').default },

            { path: '/admin/importar-afd-afdt', component: require('./pages/admin/controledeponto/afdt-afd/importar.vue').default },
            { path: '/admin/exportar-afd-afdt', component: require('./pages/admin/controledeponto/afdt-afd/exportar.vue').default },

            { path: '/admin/relatorios', component: require('./pages/admin/relatorios/index.vue').default },

            { path: '/admin/relatorios', component: require('./pages/admin/relatorios/index.vue').default },

            { path: '/colaboradores', component: require('./pages/comum/colaboradores/index.vue').default },
            { path: '/colaboradores/create', component: require('./pages/comum/colaboradores/create.vue').default },
            { path: '/colaboradores/:id/edit', component: require('./pages/comum/colaboradores/edit.vue').default },
            { path: '/colaboradores/:id', component: require('./pages/comum/colaboradores/show.vue').default },

            { path: '/escalas', component: require('./pages/comum/escalas/index.vue').default },
            { path: '/escalas/create', component: require('./pages/comum/escalas/create.vue').default },
            { path: '/escalas/:id/edit', component: require('./pages/comum/escalas/edit.vue').default },
            { path: '/escalas/:id', component: require('./pages/comum/escalas/show.vue').default },

            { path: '/logs', component: require('./pages/admin/logs/index.vue').default },
            { path: '/logs/create', component: require('./pages/admin/logs/create.vue').default },
            { path: '/logs/:id/edit', component: require('./pages/admin/logs/edit.vue').default },
            { path: '/logs/:id', component: require('./pages/admin/logs/show.vue').default },

            { path: '/chat', component: require('./pages/admin/chat/index.vue').default },
            { path: '/chat/create', component: require('./pages/admin/chat/create.vue').default },
            { path: '/chat/:id/edit', component: require('./pages/admin/chat/edit.vue').default },
            { path: '/chat/:id', component: require('./pages/admin/chat/show.vue').default },

            /* colaborador */
            { path: '/registro-ponto', component: require('./pages/comum/registro-ponto/index.vue').default },
            { path: '/registro-ponto/create', component: require('./pages/comum/registro-ponto/create.vue').default },
            { path: '/registro-ponto/:id/edit', component: require('./pages/comum/registro-ponto/edit.vue').default },
            { path: '/registro-ponto/:id', component: require('./pages/comum/registro-ponto/show.vue').default },

            { path: '/historicos/pontos', component: require('./pages/comum/historicos/pontos/index.vue').default },
            { path: '/historicos/pontos/create', component: require('./pages/comum/historicos/pontos/create.vue').default },
            { path: '/historicos/pontos/:id/edit', component: require('./pages/comum/historicos/pontos/edit.vue').default },
            { path: '/historicos/pontos/:id', component: require('./pages/comum/historicos/pontos/show.vue').default },
            
            // { path: '/historicos/bancodehoras', component: require('./pages/comum/historicos/bancodehoras/index.vue').default },
            // { path: '/historicos/bancodehoras/create', component: require('./pages/comum/historicos/bancodehoras/create.vue').default },
            { path: '/historicos/bancodehoras/:id/edit', component: require('./pages/comum/historicos/bancodehoras/edit.vue').default },
            // { path: '/historicos/bancodehoras/:id', component: require('./pages/comum/historicos/bancodehoras/show.vue').default },

            // { path: '/colaboradorchat', component: require('./pages/comum/colaboradorchat/index.vue').default },
            { path: '/colaboradorchat', component: require('./pages/comum/colaboradorchat/create.vue').default },
            { path: '/colaboradorchat/:id/edit', component: require('./pages/comum/colaboradorchat/edit.vue').default },
            { path: '/colaboradorchat/:id', component: require('./pages/comum/colaboradorchat/show.vue').default },
            
            { path: '/cadastrar-dispositivo', component: require('./pages/comum/cadastrar-dispositivo/index.vue').default },
        ]
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

/* funcoes uteis */
Vue.filter('upText', function(text){
    return text.toUpperCase()
})

Vue.filter('onlyDate', function(created){
    return moment(created).format('DD/MM/YYYY')
})

Vue.filter('onlyDateBD', function(created){
    return moment(created).format('YYYY-MM-DD')
})

Vue.filter('myDate', function(created){
    return moment(created).format('DD/MM/YYYY HH:mm')
})

Vue.filter('dataddmmyy', function(created){
    if(created != null)
        return moment(created).format('DD/MM/YYYY')
})
/* fim funcoes uteis*/

window.Fire = new Vue();

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('passport-clients', require('./components/passport/Clients.vue').default);

Vue.component('passport-authorized-clients', require('./components/passport/AuthorizedClients.vue').default);

Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue').default);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('not-found', require('./components/NotFound.vue').default);

Vue.component('pagination', require('laravel-vue-pagination'));

Vue.component('preloader-component', require('./components/PreloaderComponent.vue').default)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    store,
    computed: {
        verificaRotaCSS() {
            let rotaAdmin = this.$route.path.search('admin')
            if(rotaAdmin == 1) {
                return typeof user === "undefined" ? this.$router.go(-1) : 1 
            }
        }
    },
});
