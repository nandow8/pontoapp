<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCalendariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendarios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nome_calendario')->nullable();
            $table->date('ano')->nullable();
            $table->text('descricao_feriado_ou_recesso')->nullable();
            $table->date('data_feriado')->nullable();
            $table->boolean('hora_ou_periodo')->nullable();
            $table->boolean('dia_inteiro_apuracao')->nullable();
            $table->string('apuracao')->nullable();
            $table->time('horario_apuracao')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('calendarios');
    }
}
