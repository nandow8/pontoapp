<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCalendariosDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendarios_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('empresas_id')->nullable();
            $table->integer('calendarios_id')->nullable();
            $table->text('descricao_feriado_ou_recesso')->nullable();
            $table->date('data_feriado')->nullable();
            $table->timestamp('data_periodo_inicio')->nullable();
            $table->timestamp('data_periodo_fim')->nullable();
            $table->boolean('hora_ou_periodo')->nullable();
            $table->boolean('dia_inteiro_apuracao')->nullable();
            $table->string('apuracao')->nullable();
            $table->time('horario_apuracao')->nullable();
            $table->boolean('status')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('calendarios_datas');
    }
}
