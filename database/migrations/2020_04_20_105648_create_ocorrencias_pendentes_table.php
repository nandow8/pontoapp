<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOcorrenciasPendentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocorrencias_pendentes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('empresas_id')->nullable();
            $table->integer('users_id')->nullable();
            $table->text('descricao')->nullable();
            $table->boolean('dia_ou_datahora')->nullable();
            $table->timestamp('periodo_inicio')->nullable();
            $table->timestamp('periodo_fim')->nullable();
            $table->boolean('aprovar_ou_reprovar')->nullable();
            $table->boolean('status')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ocorrencias_pendentes');
    }
}
