<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateColaboradoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colaboradores', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('empresas_id')->nullable();
            $table->integer('users_id')->nullable();
            $table->integer('filiais_id')->nullable();
            $table->integer('cargos_id')->nullable();
            $table->integer('setores_id')->nullable();
            $table->string('matricula')->nullable();
            $table->dateTime('data_admissao')->nullable();
            $table->dateTime('data_demissao')->nullable();
            $table->integer('auth_user_id')->nullable();
            $table->boolean('enviar_instrucoes_email')->nullable();
            $table->boolean('enviar_comprovantes_email')->nullable();
            $table->boolean('status')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('colaboradores');
    }
}
