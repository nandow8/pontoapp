<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEscalasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('escalas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('empresas_id')->nullable();
            $table->string('nome_escala')->nullable();
            $table->boolean('tipo')->nullable();
            $table->integer('intervalos_id')->nullable();
            $table->integer('dia_jornada')->nullable();
            $table->boolean('tolerancia')->nullable();
            $table->integer('tolerancia_tempo')->nullable();
            $table->time('inicio_expediente')->nullable();
            $table->time('fim_expediente')->nullable();
            $table->boolean('jornada_hora_dia')->nullable();
            $table->integer('dias_trabalhar')->nullable();
            $table->integer('dias_dsr_folga')->nullable();
            $table->boolean('status')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('escalas');
    }
}
