<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePermissoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissoes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('empresas_id')->nullable();
            $table->string('titulo')->nullable();
            $table->string('quem_pertence')->nullable();
            $table->string('chave_ordem')->nullable();
            $table->integer('ordem_exibicao')->nullable();
            $table->string('avo')->nullable();
            $table->boolean('permissao_direta')->nullable();
            $table->string('descricao')->nullable();
            $table->integer('pai')->nullable();
            $table->boolean('status')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permissoes');
    }
}
